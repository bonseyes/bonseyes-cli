certifi==2018.4.16
chardet==3.0.4
idna==2.7
jsonschema==2.6.0
PyYAML==3.12
requests==2.19.1
urllib3==1.22
ruamel.yaml==0.15.100
jsonpointer==2.0
tqdm==4.39.0
cryptography==2.8