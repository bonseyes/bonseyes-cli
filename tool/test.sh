#!/bin/bash

set -e

BASE_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

for TEST in ${BASE_PATH}/tests/*/test.sh; do
    echo "Running tests in ${TEST}"
    ${TEST}
done
