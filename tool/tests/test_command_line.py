import json
import os
import pathlib
import sys
from contextlib import contextmanager
from filecmp import dircmp

from io import StringIO
import pytest
import shutil

import subprocess

import requests

from bonseyes.tool import main


def run(*args):

    os.environ['DEBUG'] = '1'

    sys.argv = ['bonseyes'] + list(args)

    with pytest.raises(SystemExit) as e:
        main()

    assert e.value.code == 0


def save_dir(path1, path2):
    os.makedirs(os.path.dirname(path2), exist_ok=True)
    shutil.copytree(path1, path2)


def assert_dir_equal(path1, path2):

    def print_diff(dcmp):
        has_differences = False

        for name in dcmp.left_only:
            print('%s only in %s' % (name, dcmp.left))
            has_differences = True

        for name in dcmp.right_only:
            print('%s only in %s' % (name, dcmp.right))
            has_differences = True

        for name in dcmp.diff_files:
            print("%s differs in %s and %s" % (name, dcmp.left, dcmp.right))
            has_differences = True

        for sub_dcmp in dcmp.subdirs.values():
            has_differences = has_differences or print_diff(sub_dcmp)

        return has_differences

    dcmp = dircmp(path1, path2)

    assert not print_diff(dcmp)


@pytest.fixture(scope='module')
def docker(request):

    docker_storage = request.config.cache.makedir('bonseyes-docker-storage')

    docker_cid = subprocess.check_output(['docker', 'run', '--rm', '--privileged', '-p', '2375', '-v',
                                          os.path.abspath(str(docker_storage)) + ':/var/lib/docker',
                                          '-d', 'docker:19.03.4-dind']).decode().strip()

    ext_port = subprocess.check_output(['docker', 'inspect', '--type=container',
                                        '-f', '{{(index (index .NetworkSettings.Ports "2375/tcp") 0).HostPort}}',
                                        docker_cid]).decode().strip()

    ip = subprocess.check_output(['docker', 'inspect', '--type=container',
                                  '-f', '{{(index (index .NetworkSettings.Ports "2375/tcp") 0).HostIp}}',
                                  docker_cid]).decode().strip()

    prev_docker_host = os.environ.get('DOCKER_HOST')

    os.environ["DOCKER_HOST"] = ip + ':' + ext_port

    yield os.environ["DOCKER_HOST"]

    if prev_docker_host is None:
        del os.environ["DOCKER_HOST"]
    else:
        os.environ["DOCKER_HOST"] = prev_docker_host

    subprocess.check_call(['docker', 'logs', docker_cid])

    subprocess.check_output(['docker', 'stop', docker_cid])


@pytest.fixture(scope='module')
def examples():

    # root directory of the repository
    root_dir = pathlib.Path(__file__).parents[2]

    return root_dir / 'examples'


@pytest.fixture
def algorithm_config(examples):
    return examples / 'algorithm_config'


@pytest.fixture
def deployment_config(examples):
    return examples / 'deployment_config'


@pytest.fixture
def challenge(examples):
    return examples / 'challenge'


@pytest.fixture
def deployment_tool(examples):
    return examples / 'deployment_tool'


@pytest.fixture(scope='module')
def platform_sources(examples, request):

    # pull the base dockers if not done before
    if not request.config.cache.get("bonseyes/platform_sources_dockers", False):
        subprocess.check_call([str(examples / 'platform_sources' / 'dockers' / 'pull.sh')])
        request.config.cache.set("bonseyes/platform_sources_dockers", True)

    # always rebuild as it contains the code under test
    subprocess.check_call([str(examples / 'platform_sources' / 'dockers' / 'build.sh')])

    return examples / 'platform_sources'


def create_evaluation_tool(tool_name, request, examples):
    config_name = "bonseyes/" + tool_name + "_dockers"

    # pull the base docker if not done before
    if not request.config.cache.get(config_name, False):
        subprocess.check_call([str(examples / tool_name / 'pull.sh')])
        request.config.cache.set(config_name, True)

    # always rebuild as it contains the code under test
    subprocess.check_call([str(examples / tool_name / 'build.sh')])

    return examples / tool_name


@pytest.fixture(scope='module')
def evaluation_tool(examples, request):
    return create_evaluation_tool('evaluation_tool', request, examples)


@pytest.fixture(scope='module')
def evaluation_tool2(examples, request):
    return create_evaluation_tool('evaluation_tool2', request, examples)


def create_data_tool(tool_name, request, examples):

    # pull the base docker if not done before
    if not request.config.cache.get("bonseyes/"+ tool_name + "_dockers", False):
        subprocess.check_call([str(examples / tool_name / 'pull.sh')])
        request.config.cache.set("bonseyes/"+ tool_name + "_dockers", True)

    # always rebuild as it contains the code under test
    subprocess.check_call([str(examples / tool_name / 'build.sh')])

    return examples / tool_name


@pytest.fixture(scope='module')
def data_tool(examples, request):
    return create_data_tool('data_tool', request, examples)


@pytest.fixture(scope='module')
def data_tool2(examples, request):
    return create_data_tool('data_tool2', request, examples)


@pytest.fixture
def credentials(tmp_path):
    credentials_file = tmp_path / 'credentials.txt'
    with credentials_file.open('w') as fp:
        fp.write('secret')

    return credentials_file


@pytest.fixture
def dataset(challenge, data_tool, tmp_path):
    output_dir = tmp_path / 'dataset'

    parameters_file = tmp_path / 'parameters'

    with parameters_file.open('w') as fp:
        fp.write('{"example_parameter": "example value"}')

    run('data-tool', 'run',
        '--data-tool', str(data_tool),
        '--input-dir', str(challenge / 'example_data'),
        '--parameters', str(parameters_file),
        '--output-dir', str(output_dir))

    return output_dir


@pytest.fixture
def dataset2(challenge, data_tool2, tmp_path, credentials):
    output_dir = tmp_path / 'dataset2'

    run('data-tool', 'run',
        '--data-tool', str(data_tool2),
        '--credentials', str(credentials),
        '--output-dir', str(output_dir))

    return output_dir


@pytest.fixture
def runtime(examples):
    return examples / 'runtime'


@pytest.fixture
def platform_build(request, platform_sources):
    platform_build_path = request.config.cache.makedir('bonseyes-platform-build')

    if not (platform_build_path / 'build.yml').exists():
        run('platform', 'build', '--force', '--platform-sources', str(platform_sources), str(platform_build_path))

    return platform_build_path


@pytest.fixture
def target_config(request, platform_sources, platform_build):

    target_config_path = request.config.cache.makedir('bonseyes-target-config')

    if not (target_config_path / 'target.yml').exists():
        run('--package', str(platform_sources),
            'target', 'setup',
            '--platform-build', str(platform_build),
            '--target-config', str(target_config_path))

    return target_config_path


@pytest.fixture
def ai_app(request, platform_sources, challenge, platform_build, algorithm_config, deployment_tool, deployment_config):

    ai_app_path = request.config.cache.makedir('bonseyes-ai-app')

    if not (ai_app_path / 'ai_app.yml').exists():
        run('--package', str(platform_sources),
            '--package', str(challenge),
            '--package', str(platform_build),
            'ai-app', 'generate',
            '--algorithm-config', str(algorithm_config),
            '--deployment-tool', str(deployment_tool),
            '--deployment-config', str(deployment_config),
            str(ai_app_path))

    return ai_app_path


@contextmanager
def capture_stdout():

    orig_stdout = sys.stdout
    sys.stdout = StringIO()

    try:

        yield sys.stdout

    finally:
        sys.stdout = orig_stdout


@pytest.fixture
def target_server(platform_sources, platform_build, target_config):

    run('--package', str(platform_sources),
        '--package', str(platform_build),
        'target', 'start-server',
        '--target-config', str(target_config))

    with capture_stdout() as stream:
        run('--package', str(platform_sources),
            '--package', str(platform_build),
            'target', 'server-address',
            '--target-config', str(target_config))

        server_address = stream.getvalue().strip()

    yield server_address

    run('--package', str(platform_sources),
        '--package', str(platform_build),
        'target', 'stop-server',
        '--target-config', str(target_config))


@pytest.fixture
def ai_app_worker(platform_sources, platform_build, challenge, ai_app, target_config, runtime):
    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        'ai-app', 'start-worker',
        '--reinstall',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))

    with capture_stdout() as stream:
        run('--package', str(platform_sources),
            '--package', str(platform_build),
            '--package', str(challenge),
            '--package', str(runtime),
            'ai-app', 'worker-address',
            '--ai-app', str(ai_app),
            '--target-config', str(target_config))

        server_address = stream.getvalue().strip()

    yield server_address

    run('--package', str(runtime),
        'ai-app', 'worker-logs',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))

    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        'ai-app', 'stop-worker',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))


def test_help():
    run('--help')


def test_packages_list():
    run('package', 'list')


def test_packages_artifacts():
    run('package', 'artifacts')


def test_packages_create(tmp_path):
    package = (tmp_path / 'mypackage').as_posix()
    run('package', 'create', '--name', 'com_example/my_package', package)
    assert_dir_equal(package, os.path.join(os.path.dirname(__file__), 'assets', 'test_packages_create'))


def test_platform_info(platform_sources):
    run('platform', 'info', '--platform-sources', str(platform_sources / 'platform.yml'))
    run('platform', 'info', '--platform-sources', str(platform_sources))


def test_platform_build(platform_sources, tmp_path):
    built_platform = tmp_path / 'build_platform'

    run('platform', 'build', '--force', '--platform-sources', str(platform_sources), str(built_platform))
    run('platform', 'build', '--platform-sources', str(platform_sources), str(built_platform))


def test_target_setup(platform_sources, platform_build, tmp_path):
    target_config = tmp_path / 'target'

    run('--package', str(platform_sources),
        'target', 'setup',
        '--platform-build', str(platform_build),
        '--target-config', str(target_config))


def test_target_server(target_server, target_config, platform_sources, platform_build):

    with requests.get(target_server) as ret:
        assert ret.status_code == 200

    run('--package', str(platform_sources),
        '--package', str(platform_build),
        'target', 'server-logs',
        '--target-config', str(target_config))


def test_ai_app_generate(platform_sources, platform_build, challenge, deployment_tool,
                         deployment_config, algorithm_config, tmp_path):
    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        'ai-app', 'generate',
        '--algorithm-config', str(algorithm_config),
        '--deployment-tool', str(deployment_tool),
        '--deployment-config', str(deployment_config),
        str(tmp_path / 'ai_app'))


def test_ai_app_benchmark_server(target_server, challenge, ai_app, runtime, data_tool,
                                 data_tool2, evaluation_tool, evaluation_tool2, tmp_path, credentials):

    run('--package', str(challenge),
        '--package', str(runtime),
        '--package', str(data_tool2),
        '--package', str(data_tool),
        '--package', str(evaluation_tool),
        '--package', str(evaluation_tool2),
        'ai-app', 'benchmark',
        '--ai-app', str(ai_app),
        '--credentials', str(credentials),
        '--manager-url', target_server,
        str(tmp_path / 'benchmark_server'))

    assert_dir_equal(tmp_path / 'benchmark_server', os.path.join(os.path.dirname(__file__), 'assets', 'benchmarks'))


def test_ai_app_benchmark(platform_sources, platform_build, challenge, ai_app, target_config, runtime, data_tool,
                          data_tool2, evaluation_tool, evaluation_tool2, tmp_path, credentials):
    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        '--package', str(data_tool),
        '--package', str(data_tool2),
        '--package', str(evaluation_tool),
        '--package', str(evaluation_tool2),
        'ai-app', 'benchmark',
        '--ai-app', str(ai_app),
        '--credentials', str(credentials),
        '--target-config', str(target_config),
        str(tmp_path / 'benchmark'))

    assert_dir_equal(tmp_path / 'benchmark', os.path.join(os.path.dirname(__file__), 'assets', 'benchmarks'))


def test_ai_app_benchmark_evaldir(platform_sources, platform_build, challenge, ai_app, target_config, runtime,
                                  data_tool2, data_tool, evaluation_tool, evaluation_tool2, tmp_path, credentials):
    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        '--package', str(data_tool2),
        '--package', str(data_tool),
        '--package', str(evaluation_tool),
        '--package', str(evaluation_tool2),
        'ai-app', 'benchmark',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config),
        '--credentials', str(credentials),
        '--evaluation-data', str(tmp_path / 'eval_data'),
        str(tmp_path / 'benchmark_evaldir'))

    assert_dir_equal(tmp_path / 'benchmark_evaldir', os.path.join(os.path.dirname(__file__), 'assets', 'benchmarks'))


def test_ai_app_worker(ai_app_worker, ai_app, target_config, runtime):

    with requests.get(ai_app_worker) as ret:
        assert ret.status_code == 200

    run('--package', str(runtime),
        'ai-app', 'worker-logs',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))


def test_ai_app_demo(platform_sources, platform_build, challenge, ai_app, target_config, runtime):
    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        'ai-app', 'start-demo',
        '--reinstall',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))

    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        'ai-app', 'demo-logs',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))

    run('--package', str(platform_sources),
        '--package', str(platform_build),
        '--package', str(challenge),
        '--package', str(runtime),
        'ai-app', 'stop-demo',
        '--ai-app', str(ai_app),
        '--target-config', str(target_config))


def test_data_tool2(dataset2):
    assert (dataset2 / 'dataset.json').exists()


def test_data_tool_run(dataset):
    assert (dataset / 'dataset.json').exists()


def test_evaluation_tool_run(dataset, ai_app_worker, evaluation_tool, tmp_path):
    run('evaluation-tool', 'run',
        '--evaluation-tool', str(evaluation_tool),
        '--worker-url', str(ai_app_worker),
        '--output-dir', str(tmp_path / 'output'),
        '--dataset-dir', str(dataset))


def test_evaluation_tool2_run(dataset, ai_app_worker, evaluation_tool2, tmp_path):
    run('evaluation-tool', 'run',
        '--evaluation-tool', str(evaluation_tool2),
        '--worker-url', str(ai_app_worker),
        '--output-dir', str(tmp_path / 'output'),
        '--named-dataset-dir', 'images', str(dataset),
        '--named-dataset-dir', 'bounding_boxes', str(dataset))


def test_challenge_get_data(challenge, data_tool, data_tool2, tmp_path, credentials):
    run('--package', str(data_tool),
        '--package', str(data_tool2),
        'challenge', 'get-data',
        '--type', 'evaluation',
        '--challenge', str(challenge),
        '--credentials', str(credentials),
        '--output-dir', str(tmp_path / 'eval'))

    run('--package', str(data_tool),
        '--package', str(data_tool2),
        'challenge', 'get-data',
        '--type', 'sample',
        '--challenge', str(challenge),
        '--credentials', str(credentials),
        '--output-dir', str(tmp_path / 'sample'))

    run('--package', str(data_tool),
        '--package', str(data_tool2),
        'challenge', 'get-data',
        '--type', 'training',
        '--credentials', str(credentials),
        '--challenge', str(challenge),
        '--output-dir', str(tmp_path / 'train'))


def test_licenses(tmp_path):
    license_path = tmp_path / 'license.lic'
    author_keys = tmp_path / 'author_keys'

    run('license', 'create-redistribution-license',
        '--output-file', str(license_path),
        '--target', 'test#tost',
        '--assigner', 'demo@bonseyes.com',
        '--amount', '100 CHF',
        '--recipients', 'test@dd.com,tost@dd.com',
        '--regions', 'https://www.wikidata.org/wiki/Q39,https://www.wikidata.org/wiki/Q40')

    run('license', 'create-author-keys',
        '--output-dir', str(author_keys))

    run('license', 'sign-redistribution-license',
        '--license', str(license_path),
        '--author-keys', str(author_keys))

    run('license', 'verify-redistribution-license',
        '--license', str(license_path),
        '--author-keys', str(author_keys))


@pytest.fixture
def bmp_credentials(tmp_path):

    bmp_credentials_path = tmp_path / 'bmp_credentials.json'

    with bmp_credentials_path.open('w') as fp:
        json.dump({'user': {'email': 'fake'}}, fp)

    prev = os.environ.get('BMP_CREDENTIALS')

    os.environ["BMP_CREDENTIALS"] = str(bmp_credentials_path)

    yield os.environ["BMP_CREDENTIALS"]

    if prev is None:
        del os.environ["BMP_CREDENTIALS"]
    else:
        os.environ["BMP_CREDENTIALS"] = prev


def test_package_artifacts(tmp_path, ai_app, challenge,
                           platform_sources, evaluation_tool, data_tool, data_tool2,
                           runtime, bmp_credentials, evaluation_tool2):

    author_keys = tmp_path / 'author_keys'

    run('license', 'create-author-keys',
        '--output-dir', str(author_keys))

    run('--package', str(challenge),
        '--package', str(evaluation_tool),
        '--package', str(evaluation_tool2),
        '--package', str(data_tool),
        '--package', str(data_tool2),
        '--package', str(runtime),
        'marketplace', 'package-ai-app',
        '--ai-app', str(ai_app),
        '--output-dir', str(tmp_path / 'packaged_ai_app'),
        '--author-keys', str(author_keys))

    run('marketplace', 'package-platform-sources',
        '--platform-sources', str(platform_sources),
        '--output-dir', str(tmp_path / 'packaged_platform'),
        '--author-keys', str(author_keys))

    run('--package', str(evaluation_tool),
        '--package', str(evaluation_tool2),
        '--package', str(data_tool),
        '--package', str(data_tool2),
        'marketplace', 'package-challenge',
        '--challenge', str(challenge),
        '--output-dir', str(tmp_path / 'packaged_challenge'),
        '--author-keys', str(author_keys))


def test_platform_snapshot(tmp_path, platform_sources, platform_build):

    platform_snapshot = tmp_path / 'platform_snapshot'
    platform_restore_dir = tmp_path / 'platform_restored'

    run('--package', str(platform_sources),
        'platform', 'save-build-snapshot',
        '--platform-build', str(platform_build),
        '--snapshot-dir', str(platform_snapshot))

    run('--package', str(platform_sources),
        'platform', 'load-build-snapshot',
        '--platform-build-dir', str(platform_restore_dir ),
        '--snapshot-dir', str(platform_snapshot))


def test_target_manage(tmp_path, challenge, ai_app, runtime, target_server):

    # ai-app

    run('target', 'manage',
        '--manager-url', target_server,
        'ai-app-upload', '--name', 'test_app', '--ai-app', str(ai_app))

    run('target', 'manage',
        '--manager-url', target_server,
        'ai-app-list')

    run('target', 'manage',
        '--manager-url', target_server,
        'ai-app-manifest', '--name', 'test_app')

    # runtime

    run('target', 'manage',
        '--manager-url', target_server,
        'runtime-upload', '--name', 'test_runtime', '--runtime', str(runtime))

    run('target', 'manage',
        '--manager-url', target_server,
        'runtime-list')

    run('target', 'manage',
        '--manager-url', target_server,
        'runtime-manifest', '--name', 'test_runtime')

    # system

    run('target', 'manage',
        '--manager-url', target_server,
        'system-address')

    run('target', 'manage',
        '--manager-url', target_server,
        'system-info')

    # solution and solution config

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-upload', '--name', 'test_solution',
        '--solution', str(runtime) + '/solutions/http_worker')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-list')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-manifest', '--name', 'test_solution')

    sample_config = tmp_path / 'config.conf'

    sample_config.write_text('AI_APP=test_ai_app\nRUNTIME=test_runtime\nPORT=8080')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-upload',
        '--solution', 'test_solution',
        '--name', 'test_config',
        '--solution-config', str(sample_config))

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-list',
        '--solution', 'test_solution')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-is-running',
        '--solution', 'test_solution',
        '--name', 'test_config')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-start',
        '--solution', 'test_solution',
        '--name', 'test_config')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-log',
        '--solution', 'test_solution',
        '--name', 'test_config')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-stop',
        '--solution', 'test_solution',
        '--name', 'test_config')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-show',
        '--solution', 'test_solution',
        '--name', 'test_config')

    # delete all the artifacts

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-config-delete',
        '--solution', 'test_solution',
        '--name', 'test_config')

    run('target', 'manage',
        '--manager-url', target_server,
        'solution-delete', '--name', 'test_solution')

    run('target', 'manage',
        '--manager-url', target_server,
        'ai-app-delete', '--name', 'test_app')

    run('target', 'manage',
        '--manager-url', target_server,
        'runtime-delete', '--name', 'test_runtime')

    # monitor

    with capture_stdout() as stream:
        run('target', 'manage',
            '--manager-url', target_server,
            'monitor-start')

        monitor_name = stream.getvalue().strip()

    run('target', 'manage',
        '--manager-url', target_server,
        'monitor-list')

    run('target', 'manage',
        '--manager-url', target_server,
        'monitor-stop', '--name', monitor_name)

    run('target', 'manage',
        '--manager-url', target_server,
        'monitor-info', '--name', monitor_name)

    run('target', 'manage',
        '--manager-url', target_server,
        'monitor-clear', '--name', monitor_name)

    # configuration

    run('target', 'manage',
        '--manager-url', target_server,
        'config-list')

    run('target', 'manage',
        '--manager-url', target_server,
        'config-get', '--name', 'benchmark_mode')

    run('target', 'manage',
        '--manager-url', target_server,
        'config-set', '--name', 'benchmark_mode', '--value', 'on')
