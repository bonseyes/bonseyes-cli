import pytest

from bonseyes.marketplace import Marketplace


@pytest.fixture
def marketplace():
    return Marketplace.from_env()


def test_development_platform_types(marketplace):
    types = marketplace.development_platform_types
    assert len(types) > 0
