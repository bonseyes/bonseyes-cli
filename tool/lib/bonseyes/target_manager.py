import json
from typing import Iterable, Optional, List, Callable

import requests

from bonseyes.utils import generate_tar

MonitorId = str
CommandId = str


class TargetManager:

    def __init__(self, url: str):
        self.url = url.rstrip('/')

    def _get_iterable(self, resource):
        with requests.get(self.url + resource) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

            yield req.content

    def _get_data(self, resource):
        with requests.get(self.url + resource) as req:

            if req.status_code != 200:
                raise Exception(req.reason)

            return req.content

    def _get_json(self, resource):
        with requests.get(self.url + resource) as req:

            if req.status_code != 200:
                raise Exception(req.reason)

            return req.json()

    def _delete(self, resource):
        with requests.delete(self.url + resource) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

    def _upload_artifact(self, resource, local_dir, name):

        if name is None:
            method = requests.post
        else:
            method = requests.put

        headers = {'content-type': 'application/x-tar'}

        with method(self.url + resource + (name or ''), headers=headers, data=generate_tar(local_dir)) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

            if name is None:
                name = req.json()['name']

        return name

    def _put_data(self, resource, data):
        with requests.put(self.url + resource, data=data) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

            return req.json()

    def _post_data(self, resource, data):
        with requests.post(self.url + resource, data=data) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

            return req.json()

    def _put(self, resource, data):
        with requests.put(self.url + resource, json=data) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

            return req.json()

    def _post(self, resource, data):
        with requests.post(self.url + resource, json=data) as req:
            if req.status_code != 200:
                raise Exception(req.reason)

            return req.json()

    # MCU

    def get_mcu_benchmark(self) -> str:
       return json.dumps(self._get_json('/platform/mcu-benchmark'))

    # Platform

    def get_platform_info(self) -> str:
        return json.dumps(self._get_json('/platform/info'))

    # System

    def get_system_info(self) -> str:
        return json.dumps(self._get_json('/system/info'))

    def get_system_address(self) -> str:
        return self._get_json('/system/address')['address']

    # Monitors

    def list_monitors(self) -> List[MonitorId]:
        return self._get_json('/monitors/')

    def start_monitor(self) -> MonitorId:
        return self._post('/monitors/', {})['name']

    def stop_monitor(self, monitor_id: MonitorId) -> None:
        return self._post('/monitors/' + monitor_id, {'action': 'stop'})

    def get_monitor_info(self, monitor_id: MonitorId) -> Iterable[bytes]:
        data = self._get_json('/monitors/' + monitor_id + '/data')

        yield (','.join(data['columns']) + '\n').encode()

        for value in data['values']:
            yield (','.join(value) + '\n').encode()

    def clear_monitor(self, monitor_id: MonitorId) -> None:
        self._delete('/monitors/' + monitor_id)

    # Configs

    def set_config(self, name: str, value: str) -> None:
        self._put('/config/' + name, {'value': value})

    def get_config(self, name: str) -> str:
        return self._get_json('/config/' + name)['value']

    def list_configs(self) -> List[str]:
        return self._get_json('/config/')

    # Ai apps

    def list_ai_apps(self) -> List[str]:
        return self._get_json('/ai-apps/')

    def upload_ai_app(self, local_path: str, name: Optional[str]=None) -> str:
        return self._upload_artifact('/ai-apps/', local_path, name)

    def delete_ai_app(self, name: str) -> None:
        self._delete('/ai-apps/' + name)

    def get_ai_app_manifest(self, name: str) -> str:
        return self._get_json('/ai-apps/' + name)['manifest']

    # Runtimes

    def list_runtimes(self) -> List[str]:
        return self._get_json('/runtimes/')

    def upload_runtime(self, local_path: str, name: Optional[str]=None) -> str:
        return self._upload_artifact('/runtimes/', local_path, name)

    def delete_runtime(self, name: str) -> None:
        self._delete('/runtimes/' + name)

    def get_runtime_manifest(self, name: str) -> str:
        return self._get_json('/runtimes/' + name)['manifest']

    # Solutions

    def list_solutions(self) -> List[str]:
        return self._get_json('/solutions/')

    def upload_solution(self, local_path: str, name: Optional[str]=None) -> str:
        return self._upload_artifact('/solutions/', local_path, name)

    def delete_solution(self, name: str) -> None:
        self._delete('/solutions/' + name)

    def get_solution_manifest(self, name: str) -> str:
        return self._get_json('/solutions/' + name)['manifest']

    # Solution configs

    def list_solution_configs(self, solution_name: str) -> List[str]:
        return self._get_json('/solutions/' + solution_name + '/configs/')

    def upload_solution_config(self, config: str, solution_name: str, config_name: Optional[str]=None) -> str:

        if config_name is None:
            return self._post_data('/solutions/' + solution_name + '/configs/', config)['name']
        else:
            self._put_data('/solutions/' + solution_name + '/configs/' + config_name, config)

            return config_name

    def delete_solution_config(self, solution_name: str, config_name: str) -> None:
        self._delete('/solutions/' + solution_name + '/configs/' + config_name)

    def get_solution_config(self, solution_name: str, config_name: str) -> bytes:
        return self._get_data('/solutions/' + solution_name + '/configs/' + config_name)

    def start_solution_config(self, solution_name: str, config_name: str, affinity: Optional[int]=None) -> None:
        message = {'command': 'start'}

        if affinity is not None:
            message['affinity'] = affinity

        self._post('/solutions/' + solution_name + '/configs/' + config_name, message)

    def stop_solution_config(self, solution_name: str, config_name: str) -> None:
        self._post('/solutions/' + solution_name + '/configs/' + config_name, {'command': 'stop'})

    def is_solution_config_running(self, solution_name: str, config_name: str) -> bool:
        return self._get_json('/solutions/' + solution_name + '/configs/' + config_name + '/is-running')['state']

    def get_solution_config_log(self, solution_name: str, config_name: str) -> Iterable[bytes]:
        return self._get_iterable('/solutions/' + solution_name + '/configs/' + config_name + '/log')

    def escape_artifact_name(self, artifact_name: str):
        return artifact_name.replace('/', '__').replace('#', '__').replace('.', '__')
