import importlib
import logging
import os
from argparse import ArgumentParser

import sys

from bonseyes.context import Context
from bonseyes.utils import get_data_dir

logger = logging.getLogger('tool')


def add_subparsers(parser, commands_dir, package_path, action_name='action'):

    subparsers = parser.add_subparsers(dest=action_name)
    subparsers.required = True

    categories = [
        x for x in os.listdir(commands_dir) if os.path.isdir(
            os.path.join(
                commands_dir, x))]

    # add all subparsers for categories
    for category in categories:
        if category == '__pycache__':
            continue

        subparser = subparsers.add_parser(category.replace('_', '-'))
        add_subparsers(
            subparser,
            os.path.join(
                commands_dir,
                category),
            package_path +
            '.' +
            category,
            'sub' + action_name)

    # add all commands
    commands = [x[:-3] for x in os.listdir(commands_dir) if x.endswith('.py')]

    for command in commands:
        parser = subparsers.add_parser(command.replace('_', '-'))
        importlib.import_module(
            package_path + '.' + command,
            package=__package__).setup_parser(parser)


def main():
    parser = ArgumentParser(description="Bonseyes CLI Tool.")

    parser.add_argument('--quiet', action='store_true')

    parser.add_argument('--package', default=[], action='append', help="Add package to the search path")
    parser.add_argument('--packages-dir', default=[], action='append',
                        help="Add directory with packages to the search path")

    add_subparsers(parser, os.path.join(os.path.dirname(__file__), 'commands'), '.commands')

    try:
        import argcomplete
        argcomplete.autocomplete(parser)
    except ImportError:
        pass

    args = parser.parse_args()

    if not args.quiet and "pytest" not in sys.modules:
        logger.handlers.clear()
        logger.setLevel(logging.DEBUG if 'DEBUG' in os.environ else logging.INFO)
        logger.addHandler(logging.StreamHandler(sys.stdout))

    try:

        package_paths = []

        context = Context(package_paths)

        # add the default packages for interfaces and algorithms that are distributed with the tool
        context.add_package(os.path.join(get_data_dir(), 'interfaces'))
        context.add_package(os.path.join(get_data_dir(), 'algorithms'))
        context.add_package(os.path.join(get_data_dir(), 'network_formats'))

        # add the packages from the command line
        for package in args.package:
            context.add_package(package)

        # scan the packages dirs from the environment
        packages_dirs = args.packages_dir + [os.getcwd()]

        if 'BONSEYES_PACKAGES_DIRS' in os.environ:
            packages_dirs += os.environ['BONSEYES_PACKAGES_DIRS'].split(':')

        for package_dir in packages_dirs:
            for package in os.listdir(package_dir):
                package_path = os.path.join(package_dir, package, 'package.yml')
                if os.path.isfile(package_path):
                    try:
                        package = context.add_package(os.path.join(package_dir, package))
                        if 'DEBUG' in os.environ:
                            sys.stderr.write("Found package %s in %s\n" % (package.name, package_path))
                    except Exception:
                        sys.stderr.write("Invalid package in %s\n" % package_path)

        # add the packages from the environment
        if 'BONSEYES_PACKAGES' in os.environ:
            for package_path in os.environ['BONSEYES_PACKAGES'].split(':'):
                try:
                    context.add_package(package_path)
                except Exception:
                    sys.stderr.write("Could not add package %s specified in BONSEYES_PACKAGES\n" % package_path)

        args.func(context, args)

        sys.exit(0)

    except Exception as e:
        if 'DEBUG' in os.environ:
            raise e
        else:
            print(e)
            sys.exit(1)
