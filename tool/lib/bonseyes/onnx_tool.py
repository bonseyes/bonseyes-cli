class OnnxTool:
    @staticmethod
    def run_checker(model: str):
        try:
            from lpdnn_onnx import check_lpdnn_conversion
        except ModuleNotFoundError:
            print("To use onnx check you need to have the lpdnn_python_lib installed. "
                  "If the registry is setup correctly you should be able to install it using 'pip install lpdnn_python_lib'")
            return

        check_lpdnn_conversion.check_model(model)
