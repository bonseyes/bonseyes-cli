from typing import Dict, Union, MutableMapping, MutableSequence

from bonseyes.algorithm import Algorithm
from bonseyes.challenge import Challenge
from bonseyes.interface import Interface
from bonseyes.data_tool import DataTool
from bonseyes.evaluation_tool import EvaluationTool
from bonseyes.network_format import NetworkFormat
from bonseyes.object_editor import JsonEditor
from bonseyes.schemas import SCHEMAS
from bonseyes.ui import ask_boolean, update_option, input_with_context


class MetadataEditor(JsonEditor):

    SCHEMA_PATH = SCHEMAS['metadata']

    def edit_generic(self, schema: Dict, parent: Union[MutableMapping, MutableSequence],
                     attr_name: Union[int, str]) -> None:

        # special case to edit extra metadata
        if self.current_path_matches(['extra', None]):
            self.edit_external_schema(SCHEMAS['metadata_marketplace'], parent, attr_name)

        else:
            super().edit_generic(schema, parent, attr_name)


class DataEditor(JsonEditor):

    SCHEMA_PATH = SCHEMAS['data_reference']

    def display_entry(self, instance):
        if self.current_path == ['sources']:
            return (instance.get('title', 'No title') + ' (' +
                    instance.get('url', 'No URL') + ')\n' +
                    instance.get('description', 'No description'))
        else:
            return super().display_entry(instance)

    def edit_generic(self, schema: Dict,
                     parent: Union[MutableMapping, MutableSequence],
                     attr_name: Union[int, str]) -> None:

        # special case to edit data tool ids
        if self.current_path_matches(['tool', 'id']):
            self.edit_artifact_name(schema, parent, attr_name, DataTool)

        # special case for data tool parameters
        elif self.current_path_matches(['tool', 'parameters']):
            tool = DataTool.load(parent['id'], self.context)

            if tool.parameters_schema_path is None:
                return

            self.edit_external_schema(tool.parameters_schema_path, parent, attr_name)

        else:
            super().edit_generic(schema, parent, attr_name)


class ChallengeEditor(JsonEditor):

    SCHEMA_PATH = SCHEMAS['challenge']

    def display_entry(self, instance):
        if self.current_path == ['evaluation_protocol']:
            return 'tool: ' + instance.get('tool', {}).get('id')
        else:
            return super().display_entry(instance)

    def edit_generic(self, schema: Dict,
                     parent: Union[MutableMapping, MutableSequence],
                     attr_name: Union[int, str]) -> None:

        # special case to edit metadata
        if self.current_path_matches(['metadata']):
            self.edit_custom_editor(MetadataEditor, parent, attr_name)

        # special case to edit interface parameters
        elif self.current_path_matches(['specification', 'interface', 'parameters']):
            interface = Interface.load(self.instance['specification']['interface']['id'],
                                       self.context)

            if interface.parameters_schema_path is None:
                return

            self.edit_external_schema(interface.parameters_schema_path, parent, attr_name)

        # special case to edit interface id
        elif self.current_path_matches(['specification', 'interface', 'id']):
            self.edit_artifact_name(schema, parent, attr_name, Interface)

        # special case to edit evaluation tools ids
        elif self.current_path_matches(['evaluation_protocol', None, 'tool', 'id']):
            self.edit_artifact_name(schema, parent, attr_name, EvaluationTool)

        # special case to edit data tool ids
        elif self.current_path_matches(['specification', 'training_data']) or \
                self.current_path_matches(['specification', 'sample_data']) or \
                self.current_path_matches(['specification', 'quantization_data']) or \
                self.current_path_matches(['evaluation_protocol', None, 'evaluation_data']):
            self.edit_custom_editor(DataEditor, parent, attr_name)

        # special case for evaluation tools parameters
        elif self.current_path_matches(['evaluation_protocol', None, 'tool', 'parameters']):
            tool = EvaluationTool.load(parent['id'], self.context)

            if tool.parameters_schema_path is None:
                return

            self.edit_external_schema(tool.parameters_schema_path, parent, attr_name)

        else:
            super().edit_generic(schema, parent, attr_name)


class AlgorithmConfigEditor(JsonEditor):

    SCHEMA_PATH = SCHEMAS['algorithm_config']

    def edit_algorithm_name(self, parent, attr_name, interface_name):

        if attr_name in parent and parent[attr_name] is not None:

            print("The current value is " + str(parent[attr_name]))

            if not ask_boolean("Do you want to change it?", True,
                               context=self.current_context_string):
                return

        if ask_boolean("Do you want to pick an algorithm available in the context?", True,
                       context=self.current_context_string):

            options = {}

            for artifact_entry in self.context.artifacts:

                if artifact_entry.artifact_type != Algorithm.ARTIFACT_CLASS_NAME:
                    continue

                artifact = Algorithm.load(artifact_entry.artifact_name,
                                          self.context)   # type: Algorithm

                if artifact.interface_name != interface_name:
                    continue

                artifact_metadata = artifact.metadata or {}

                options[artifact.artifact_name] = artifact_metadata.get('title')

            if len(options) != 0:
                selected_artifact = update_option(None, options,
                                                  context=self.current_context_string)
                parent[attr_name] = selected_artifact
                return

            print()
            print("No algorithm with interface %s found in the context." % interface_name)
            print()
            print("You need to enter the algorithm name manually.")
            print()

        while True:

            print()
            print("Select the package from the packages available in the context:")
            print()

            package_name = update_option(None, {package.name: package.base_path
                                                for package in self.context.packages},
                                         context=self.current_context_string)

            print()

            artifact_path = input_with_context("Enter the algorithm name (default . )",
                                               context=self.current_context_string)

            if artifact_path == '':
                artifact_path = '.'

            artifact_name = package_name + '#' + artifact_path

            try:
                algorithm = Algorithm.load(artifact_name, self.context)

                parent[attr_name] = artifact_name

                if algorithm.interface_name == interface_name:
                    break
                else:
                    print("The algorithm doesn't implement interface %s" % interface_name)

            except Exception as e:
                print("Failed to load algorithm: " + str(e))

    def edit_subcomponents(self, parent_algorithm: Algorithm,
                     parent: Union[MutableMapping, MutableSequence], attr_name: Union[int, str]):

        for name, interface_name in parent_algorithm.subcomponents.items():

            print("Editing algorithm subcomponent " + name +
                  " of algorithm " + parent_algorithm.artifact_name)

            subcomponent_data = parent.setdefault(attr_name, {}).setdefault(name, {})

            self.edit_algorithm_name(subcomponent_data, 'algorithm', interface_name)

            algorithm = Algorithm.load(subcomponent_data['algorithm'], self.context)

            if algorithm.parameters_schema_path is None:
                return

            subcomponent_data['parameters'] = {}

            self.edit_external_schema(algorithm.parameters_schema_path, subcomponent_data,
                                      'parameters')

            subcomponent_data['subcomponents'] = {}

            self.edit_subcomponents(algorithm, subcomponent_data, 'subcomponents')

    def edit_generic(self, schema: Dict,
                     parent: Union[MutableMapping, MutableSequence],
                     attr_name: Union[int, str]) -> None:

        # make sure we alway prompt for subcomponent and parameters
        if not self.full_current_path:
            schema['required'].extend(['subcomponents', 'parameters'])
            super().edit_generic(schema, parent, attr_name)

        # special case to edit metadata
        elif self.current_path_matches(['metadata']):
            self.edit_custom_editor(MetadataEditor, parent, attr_name)

        # special case to edit the algorithm
        elif self.current_path_matches(['algorithm']):

            challenge = Challenge.load(parent['challenge'], self.context)

            interface_name = challenge.interface_name

            if interface_name is None:
                raise Exception("The challenge doesn't have an interface specified")

            self.edit_algorithm_name(parent, attr_name, interface_name)

        # special case to edit the challenge
        elif self.current_path_matches(['challenge']):
            self.edit_artifact_name(schema, parent, attr_name, Challenge)

        # special case to edit interface parameters
        elif self.current_path_matches(['parameters']):

            if parent.get(attr_name) is None:
                parent[attr_name] = {}

            algorithm = Algorithm.load(self.instance['algorithm'], self.context)

            if algorithm.parameters_schema_path is None:
                return

            self.edit_external_schema(algorithm.parameters_schema_path, parent, attr_name)

        elif self.current_path_matches(['subcomponents']):

            if parent.get(attr_name) is None:
                parent[attr_name] = {}

            algorithm = Algorithm.load(self.instance['algorithm'], self.context)

            self.edit_subcomponents(algorithm, parent, attr_name)

        else:
            super().edit_generic(schema, parent, attr_name)


class ModelEditor(JsonEditor):
    SCHEMA_PATH = SCHEMAS['model']

    def edit_network_format(self, parent, attr_name):

        if attr_name in parent and parent[attr_name] is not None:

            print("The current value is " + str(parent[attr_name]))

            if not ask_boolean("Do you want to change it?", True,
                               context=self.current_context_string):
                return

        if ask_boolean("Do you want to pick an network format available in the context?", True,
                       context=self.current_context_string):

            options = {}

            for artifact_entry in self.context.artifacts:

                if artifact_entry.artifact_type != NetworkFormat.ARTIFACT_CLASS_NAME:
                    continue

                artifact = NetworkFormat.load(artifact_entry.artifact_name,
                                              self.context)   # type: NetworkFormat

                artifact_metadata = artifact.metadata or {}

                options[artifact.artifact_name] = artifact_metadata.get('title')

            if len(options) != 0:
                selected_artifact = update_option(None, options,
                                                  context=self.current_context_string)
                parent[attr_name] = selected_artifact
                return

            print()
            print("No network format found in the context.")
            print()
            print("You need to enter the network format name manually.")
            print()

        while True:

            print()
            print("Select the package from the packages available in the context:")
            print()

            package_name = update_option(None, {package.name: package.base_path
                                                for package in self.context.packages},
                                         context=self.current_context_string)

            print()

            artifact_path = input_with_context("Enter the network format name (default . )",
                                               context=self.current_context_string)

            if artifact_path == '':
                artifact_path = '.'

            artifact_name = package_name + '#' + artifact_path

            try:
                NetworkFormat.load(artifact_name, self.context)

                parent[attr_name] = artifact_name

                break

            except Exception as e:
                print("Failed to load network format: " + str(e))

    def edit_generic(self, schema: Dict,
                     parent: Union[MutableMapping, MutableSequence],
                     attr_name: Union[int, str]) -> None:

        if self.current_path_matches(['type']):
            self.edit_network_format(parent, attr_name)

        elif self.current_path_matches(['parameters']):

            if parent.get(attr_name) is None:
                parent[attr_name] = {}

            network_format = NetworkFormat.load(self.instance['type'], self.context)

            if network_format.parameters_schema_path is None:
                return
            print("----------->" + network_format.parameters_schema_path)
            self.edit_external_schema(network_format.parameters_schema_path, parent, attr_name)

        else:
            super().edit_generic(schema, parent, attr_name)
