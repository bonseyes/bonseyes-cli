import os

import subprocess
from typing import Optional, List, Union, Dict

from bonseyes.algorithm import AlgorithmConfig
from bonseyes.ai_app import AiApp
from bonseyes.ai_artifact import AiArtifact
from bonseyes.challenge import Challenge
from bonseyes.dataset import Dataset
from bonseyes.platform import PlatformBuild
from bonseyes.schemas import SCHEMAS
from bonseyes.utils import docker_run_extra_params, find_subcomponent


class DeploymentConfig(AiArtifact):
    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#deployment_config'
    DATA_SCHEMA = SCHEMAS['deployment_config']
    DEFAULT_ARTIFACT_FILE_NAME = 'deployment_config.yml'

    def get_parameters(self, role: Optional[Union[str, List[str]]] = None) -> Dict:
        return find_subcomponent(self.data, role, {}).get('parameters')

    def get_subcomponent(self, role: Optional[Union[str, List[str]]] = None) -> Dict:
        return find_subcomponent(self.data, role, {})


class DeploymentTool(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#deployment_tool'
    DATA_SCHEMA = SCHEMAS['deployment_tool']
    DEFAULT_ARTIFACT_FILE_NAME = 'deployment_tool.yml'

    def generate_ai_app(self, output_dir: str,
                        algorithm_config: AlgorithmConfig,
                        deployment_config: Optional[DeploymentConfig] = None,
                        challenge: Optional[Challenge] = None,
                        manager_url: Optional[str] = None,
                        dataset: Optional[Dataset] = None,
                        dset_gt : str = None,
                        quant_params: str = None,
                        debug: bool = False
                        ) -> AiApp:

        if self.data.get('target_required', False) and manager_url is None:
            raise Exception("Missing target manager URL")
        if self.data.get('datase_required', False) and dataset is None:
            raise Exception("Missing dataset")

        # find the challenge in the algorithm_config if the challenge was not passed in the command line
        if challenge is None:
            challenge = Challenge.load(algorithm_config.challenge_name, self.package.context)

        os.makedirs(output_dir, exist_ok=True)

        extra_params = docker_run_extra_params()

        platform_build = PlatformBuild.load(self.data['platform_build'], self.package.context)

        extra_params += ['-v', os.path.abspath(self.package.base_path) + ':/tool']
        extra_params += ['-v', os.path.abspath(platform_build.platform_sources.package.base_path) + ':/platform']
        extra_params += ['-v', os.path.abspath(algorithm_config.package.base_path) + ':/model']
        extra_params += ['-v', os.path.abspath(output_dir) + ':/out']
        extra_params += ['-v', os.path.abspath(challenge.package.base_path) + ':/challenge']

        build_params = [platform_build.platform_sources.builder_image,
                        os.path.join('/tool', self.rel_base_path, self.data.get('entry_point', 'bin/deploy.sh')),
                        '--platform-path', '/platform',
                        '--model-file', '/model/' + algorithm_config.rel_path,
                        '--output-dir', '/out']

        if debug:
            build_params += ['--debug']

        if deployment_config is not None:
            extra_params += ['-v', os.path.abspath(deployment_config.package.base_path) + ':/deployment_config']
            build_params += ['--deployment-config-file', '/deployment_config/' + deployment_config.rel_path]

        if challenge is not None:
            extra_params += ['-v', os.path.abspath(challenge.package.base_path) + ':/challenge']
            build_params += ['--challenge-file', '/challenge/' + challenge.rel_path]

        if dataset is not None:
            extra_params += ['-v', os.path.abspath(dataset.package.base_path) + ':/dataset']
            build_params += ['--dataset-file', '/dataset/' + dataset.rel_path]

        if dset_gt is not None:
            extra_params += ['-v', os.path.abspath(os.path.dirname(dset_gt)) + ':/dset_gt']
            build_params += ['--dset-gt', '/dset_gt/'  + dset_gt.split('/')[-1]]

        if quant_params is not None:
            extra_params += ['-v', os.path.abspath(os.path.dirname(quant_params)) + ':/quant_params']
            build_params += ['--quant-params', '/quant_params/'  + quant_params.split('/')[-1]]

        if manager_url is not None:
            extra_params += ['--network', 'host']
            build_params += ['--manager-url', manager_url]

        subprocess.check_call(['docker', 'run'] + extra_params + build_params)

        output_file = os.path.join(output_dir, 'ai_app.yml')

        self.package.context.add_package(output_dir)
        output_name = self.package.context.find_artifact_name(output_file)

        return AiApp.load(output_name, self.package.context)

    def benchmark_analyze(self, release: str,
                        ai_app: str,
                        manager_url: str = None,
                        dataset: Dataset = None,
                        number: int = None,
                        rgb: Optional[str] = None,
                        dset_gt : Optional[str] = None,
                        filename: Optional[str] = None,
                        force_cp: Optional[bool] = False
                        ):

        if self.data.get('target_required', False) and manager_url is None:
            raise Exception("Missing target manager URL")
        if self.data.get('datase_required', False) and dataset is None:
            raise Exception("Missing dataset")

        extra_params = docker_run_extra_params()
        out = os.path.join(os.path.abspath(release), '..', 'out')
        ai_app_name = ai_app.rstrip('/').split('/')[-1]

        platform_build = PlatformBuild.load(self.data['platform_build'], self.package.context)

        extra_params += ['-v', os.path.abspath(self.package.base_path) + ':/tool']
        extra_params += ['-v', os.path.abspath(release) + ':/release']
        extra_params += ['-v', os.path.abspath(out) + ':/out']
        extra_params += ['-v', os.path.abspath(ai_app) + ':/ai_app']
        extra_params += ['-v', os.path.abspath(dataset.package.base_path) + ':/dataset']
        extra_params += ['-w', os.path.join('/tool', os.path.join('tools', 'benchmark-analyzer'))]

        build_params = [platform_build.platform_sources.builder_image,
                        os.path.join('/tool', os.path.join('tools', 'benchmark-analyzer','main.py')),
                        '--runtime', os.path.join('/release', 'runtime'),
                        '--ai-app', '/ai_app',
                        '--ai-app-name', ai_app_name,
                        '--dset', '/dataset/' + dataset.rel_path,
                        '--number', str(number),
                        '--platform']

        if dset_gt is not None:
            extra_params += ['-v', os.path.abspath(os.path.dirname(dset_gt)) + ':/dset_gt']
            build_params += ['--dset-gt', '/dset_gt/' + dset_gt.split('/')[-1]]

        if filename is not None:
            extra_params += ['-v', os.path.abspath(os.path.dirname(filename)) + ':/filename']
            build_params += ['--filename', '/filename/' + filename.split('/')[-1]]

        if manager_url is not None:
            extra_params += ['--network', 'host']
            build_params += ['--manager-url', manager_url]

        if rgb is not None:
            build_params += ['--rgb', rgb]

        if force_cp:
            build_params += ['--force-cp']

        subprocess.check_call(['docker', 'run'] + extra_params + build_params)
