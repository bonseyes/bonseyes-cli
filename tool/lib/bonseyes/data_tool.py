import os

import subprocess

import sys
from typing import Optional

from bonseyes.ai_artifact import AiArtifact
from bonseyes.schemas import SCHEMAS


class DataTool(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#data_tool'
    DATA_SCHEMA = SCHEMAS['data_tool']
    DEFAULT_ARTIFACT_FILE_NAME = 'data_tool.yml'

    @property
    def image(self):
        return self.data['image']

    @image.setter
    def image(self, new_image: str):
        self.data['image'] = new_image

    @property
    def parameters_schema_path(self):
        if 'parameters' in self.data:
            return os.path.join(self.abs_base_path, self.data['parameters'])
        else:
            return None

    def create_dataset(self, output_dir: str, parameters: str = None, input_dir: str = None,
                       credentials: Optional[str]=None):
        docker_extra_params = []
        data_tool_params = []

        # check if we are running in a pseudoterminal and if that is the case attach it to docker
        if sys.stdout.isatty():
            docker_extra_params += ['-ti']

        if self.data.get('input_data_required', False) and input_dir is None:
            raise Exception("The data tool %s requires an input dir but none was specified" % self.artifact_name)

        # check that the input dir actually exists
        if input_dir is not None:
            if not os.path.isdir(input_dir):
                raise Exception("The input_dir '%s' is not a valid directory" % str(input_dir))

            # mount the input dir in the docker
            input_dir = os.path.abspath(input_dir)

            docker_extra_params.extend(['-v', input_dir + ':' + input_dir + ':ro'])
            data_tool_params.extend(['--input-dir', input_dir])

        # check that the parameters files actually exists if specified
        if parameters is not None:
            if not os.path.isfile(parameters):
                raise Exception("Parameters '%s' given are not a valid file" % str(parameters))

            # mount the parameter files in the docker
            parameters = os.path.abspath(parameters)

            docker_extra_params.extend(['-v', parameters + ':' + parameters + ':ro'])
            data_tool_params.extend(['--parameters', parameters])

        # check if credentials are required and provided
        credentials_required = self.data.get('credentials', 'none')

        if credentials_required == 'required' and credentials is None:
            raise Exception('Data tool requires credentials and none was provided')

        # add credentials to tool invocation if provided
        if credentials is not None and credentials_required != 'none':
            if not os.path.exists(credentials):
                raise Exception("Credentials " + credentials + " do not exist")

            docker_extra_params.extend(['-v', os.path.abspath(credentials) + ':/credentials:ro'])
            data_tool_params.extend(['--credentials', '/credentials'])

        # check that the output dir doesn't exists yet
        if os.path.exists(output_dir):
            raise Exception("Output directory '%s' already exists" % output_dir)

        # create the output dir
        os.makedirs(output_dir)

        # find the absolute path of output dir and mount it in the docker
        output_dir = os.path.abspath(output_dir)
        docker_extra_params.extend(['-v', output_dir + ':' + output_dir])
        data_tool_params.extend(['--output-dir', output_dir])

        # set uid and gid in the docker to match the ones used by the current user
        docker_extra_params.extend(['-u', str(os.getuid()) + ':' + str(os.getgid())])

        # execute the evaluation tool
        subprocess.check_call(['docker', 'run'] + docker_extra_params +
                              [self.image] + data_tool_params)
