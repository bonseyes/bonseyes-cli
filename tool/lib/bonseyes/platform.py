import json
import os
from builtins import property
from subprocess import CalledProcessError
from time import sleep
from typing import Dict, List

import logging

import subprocess

import shutil
from typing import Optional

import requests

from bonseyes.ai_artifact import AiArtifact
from bonseyes.context import Context, Package, get_artifact_name_parts, get_abs_artifact_name
from bonseyes.docker import Volume, Container, VolumeEntry
from bonseyes.utils import docker_run_extra_params

logger = logging.getLogger('tool')


class PlatformSources(AiArtifact):

    ARTIFACT_CLASS_NAME = "com_bonseyes/types#platform_sources"

    DEFAULT_ARTIFACT_FILE_NAME = 'platform.yml'

    @property
    def platform_sources_dir(self) -> str:
        return self.abs_base_path

    @property
    def support_image(self) -> str:
        return self._load_image_name('support-image')

    @property
    def builder_image(self) -> str:
        return self._load_image_name('builder-image')

    @property
    def manager_image(self) -> str:
        return self._load_image_name('manager-image')

    def _load_image_name(self, image_type: str) -> str:
        file_name = os.path.join(self.platform_sources_dir, 'platform', 'conf', image_type)

        return self._read_without_comments(file_name)[0]

    def _read_without_comments(self, file_path: str) -> List[str]:
        lines = []

        with open(file_path) as fp:

            for line in fp.readlines():
                if '#' in line:
                    line = line[0:line.index('#')]
                line = line.strip()

                if line != '':
                    lines.append(line)

        return lines

    @property
    def docker_images(self) -> List[str]:
        images_list_file = os.path.join(self.platform_sources_dir, 'platform', 'conf', 'docker-images')

        return self._read_without_comments(images_list_file)

    @property
    def platform_build_name(self) -> str:
        return get_abs_artifact_name(self.data['platform_build'], self.artifact_name)

    def build(self, output_path: str, force: bool = False) -> 'PlatformBuild':
        os.makedirs(output_path, exist_ok=True)

        target_platform_name = self.platform_build_name

        target_package_name, target_artifact_name = get_artifact_name_parts(target_platform_name)

        Package.create(self.package.context, output_path, target_package_name)

        self.package.context.add_package(output_path)

        pb = PlatformBuild.create(target_platform_name, self.package.context, self.artifact_name)
        pb.build(force)

        return pb


class PlatformBuild(AiArtifact):

    ARTIFACT_CLASS_NAME = "com_bonseyes/types#platform_build"
    DEFAULT_ARTIFACT_FILE_NAME = 'build.yml'

    def __init__(self, package: Package, artifact_name: str, data: Dict):
        super().__init__(package, artifact_name, data)
        self._platform = None  # type: Optional[PlatformSources]

    @property
    def build_dir(self):
        return self.abs_base_path

    @classmethod
    def create(cls, artifact_name: str, context: Context, sources_name: str):

        package_path, rel_artifact_path = get_artifact_name_parts(artifact_name)
        package = context.get_artifact_package(artifact_name)
        artifact_path = os.path.join(package.base_path, rel_artifact_path)

        if os.path.isdir(artifact_path):
            artifact_path = os.path.join(artifact_path, cls.DEFAULT_ARTIFACT_FILE_NAME)

        build_dir = os.path.dirname(artifact_path)

        os.makedirs(build_dir, exist_ok=True)

        with open(artifact_path, 'w') as fp:
            json.dump({'sources': sources_name}, fp)

        return cls.load(artifact_name, context)

    @property
    def platform_sources(self) -> PlatformSources:
        if self._platform is None:
            self._platform = PlatformSources.load(self.platform_sources_name, self.package.context)

        return self._platform

    @property
    def platform_sources_name(self) -> str:
        return self.data['sources']

    def _image_exists(self, image_name: str) -> bool:
        with open(os.devnull, 'w') as fp:
            try:
                subprocess.check_call(['docker', 'inspect', '--type=image', image_name], stderr=fp, stdout=fp)
                return True
            except CalledProcessError:
                return False

    def _delete_image(self, image_name: str):
        subprocess.check_call(['docker', 'rmi', '-f', image_name])

    def _load_or_build_image(self, image_name: str, image_source_path: str, force: bool):

        # check if we need to build an image from sources or not
        if not os.path.exists(os.path.join(image_source_path, 'Dockerfile')):
            if not self._image_exists(image_name):
                raise Exception("Cannot find pre-built image " + image_name + " locally")

            return

        # delete existing image if forcing rebuild
        if force:
            if self._image_exists(image_name):
                logger.info("Deleting existing image " + image_name)
                self._delete_image(image_name)

        # rebuild image only if it is missing
        if not self._image_exists(image_name):
            logger.info("Building docker image " + image_name + " from sources in " + image_source_path)

            # build the image
            subprocess.check_call(['docker', 'build',
                                   '--network', 'none',
                                   '-t', image_name,
                                   os.path.abspath(image_source_path)])

        else:
            logger.info("Using existing image " + image_name)

    @property
    def _deps_dir(self):
        return os.path.join(self.build_dir, 'deps')

    @property
    def _os_dir(self):
        return os.path.join(self.build_dir, 'os')

    @property
    def _builder_sources_dir(self):
        return os.path.join(self.build_dir, 'builder')

    @property
    def _manager_sources_dir(self):
        return os.path.join(self.build_dir, 'manager')

    def _download_dependencies(self, force: bool):

        if not os.path.isdir(self.platform_sources.platform_sources_dir):
            raise Exception("The platform directory is invalid")

        os.makedirs(self.build_dir, exist_ok=True)

        # check that the support image is available locally
        if not self._image_exists(self.platform_sources.support_image):
            raise Exception('Cannot find support image ' + self.platform_sources.support_image + ' locally')

        # run the dependency download wizard
        logger.info("Downloading build dependencies")
        logger.info("deps path = " + self._deps_dir)

        deps_complete = os.path.join(self.build_dir, 'deps.complete')

        # remove previous results if force
        if force and os.path.exists(deps_complete):
            shutil.rmtree(self._deps_dir)
            os.unlink(deps_complete)

        # skip download if it has already been done
        if os.path.exists(deps_complete):
            logger.info("Dependencies already downloaded")
            return

        os.makedirs(self._deps_dir, exist_ok=True)
        logger.info("Starting deps.sh in support docker")
        subprocess.check_call(['docker', 'run'] + docker_run_extra_params() +
                              ['-v', os.path.abspath(self.platform_sources.platform_sources_dir) + ':/platform:ro',
                               '-v', os.path.abspath(self.build_dir) + ':/build:rw',
                               '--network', 'host',
                               '-e', 'PLATFORM_DIR=/platform',
                               '-e', 'DEPS_DIR=/build/deps',
                               '-e', 'HOST_PLATFORM_DIR=' + os.path.abspath(self.platform_sources.platform_sources_dir),
                               '-e', 'HOST_DEPS_DIR=' + os.path.abspath(self._deps_dir),
                               self.platform_sources.support_image, 'deps.sh'])

        # touch the deps.complete file
        with open(deps_complete, 'w') as _:
            pass

    def _build(self, force: bool):

        # build the platform
        logger.info("Building the platform")
        logger.info("os files: " + self._os_dir)
        logger.info("builder sources: " + self._builder_sources_dir)
        logger.info("manager sources: " + self._manager_sources_dir)

        # delete previous results if build is force
        build_complete = os.path.join(self.build_dir, 'build.complete')

        # remove previous results if force
        if force and os.path.exists(build_complete):
            shutil.rmtree(self._os_dir)
            shutil.rmtree(self._builder_sources_dir)
            shutil.rmtree(self._manager_sources_dir)
            os.unlink(build_complete)

        if os.path.exists(build_complete):
            logger.info("Using previously built platform")
            return

        os.makedirs(self._os_dir, exist_ok=True)
        os.makedirs(self._builder_sources_dir, exist_ok=True)
        os.makedirs(self._manager_sources_dir, exist_ok=True)

        logger.info("Starting build.sh in support docker")
        subprocess.check_call(['docker', 'run'] + docker_run_extra_params() +
                              ['-v', os.path.abspath(self.platform_sources.platform_sources_dir) + ':/platform:ro',
                               '-v', os.path.abspath(self.build_dir) + ':/build:rw',
                               '--network', 'none',
                               '-e', 'OS_DIR=/build/os',
                               '-e', 'PLATFORM_DIR=/platform',
                               '-e', 'DEPS_DIR=/build/deps',
                               '-e', 'BUILDER_SOURCES_DIR=/build/builder',
                               '-e', 'MANAGER_SOURCES_DIR=/build/manager',
                               self.platform_sources.support_image, 'build.sh'])
        logger.info("Execution of build.sh in support docker completed")

        with open(build_complete, 'w') as _:
            pass

    def _download_docker_images(self, force: bool):
        logger.info("Pulling all required docker images")

        for image in self.platform_sources.docker_images:

            if force and self._image_exists(image):
                logger.info("Deleting existing image " + image)
                self._delete_image(image)

            if not self._image_exists(image):
                logger.info("Pulling image " + image)
                subprocess.check_call(['docker', 'pull', image])
            else:
                logger.info("Using existing local image " + image)

    @property
    def _completed_file(self):
        return os.path.join(self.build_dir, 'complete')

    @property
    def is_built(self):
        return os.path.exists(self._completed_file)

    def build(self, force: bool):

        # check if we need to rebuild
        if self.is_built:
            if force:
                os.unlink(self._completed_file)
            else:
                logger.info("Build already performed")
                return

        # download all the docker images
        self._download_docker_images(force)

        # download all the dependencies of the build
        self._download_dependencies(force)

        # build the OS and docker sources
        self._build(force)

        # load the builder docker image
        self._load_or_build_image(self.platform_sources.builder_image, self._builder_sources_dir, force)

        # load the manager docker image
        self._load_or_build_image(self.platform_sources.manager_image, self._manager_sources_dir, force)

        # mark the build complete
        with open(self._completed_file, 'w') as _:
            pass

    def create_target_config(self, target_config_dir: str):
        obj = TargetConfig.create(target_config_dir, self.package.context, self.artifact_name)
        return obj

    @staticmethod
    def _add_mount(mount_list: List[str], var_name: str, host_path: Optional[str], docker_path: str, mode: str):
        if host_path is None:
            return
        mount_list += ['-v', host_path + ':' + docker_path + ':' + mode]
        mount_list += ['-e', var_name + '=' + docker_path]


    def run_in_builder(self, cmd_args: List[str], sources_dir: str, cache_dir: Optional[str] = None, output_dir: Optional[str] = None, internal_paths: bool = False):
        if not os.path.isdir(sources_dir):
            raise Exception("Invalid source directory: " + sources_dir)
        sources_dir = os.path.abspath(sources_dir)

        if cache_dir is not None:
            os.makedirs(cache_dir, exist_ok=True)
            cache_dir = os.path.abspath(cache_dir)

        if output_dir is not None:
            os.makedirs(output_dir, exist_ok=True)
            output_dir = os.path.abspath(output_dir)

        platform_dir = os.path.abspath(self.platform_sources.platform_sources_dir)

        if internal_paths:
            platform_docker_path = '/platform'
            source_docker_path = '/sources'
            cache_docker_path = '/out'
            output_docker_path = '/install'
        else:
            platform_docker_path = platform_dir
            source_docker_path = sources_dir
            cache_docker_path = cache_dir
            output_docker_path = output_dir

        mount_list = []
        self._add_mount(mount_list, 'PLATFORM_PATH', platform_dir, platform_docker_path, 'ro')
        self._add_mount(mount_list, 'SOURCES_PATH', sources_dir, source_docker_path, 'ro')
        self._add_mount(mount_list, 'OUTPUT_PATH', cache_dir, cache_docker_path, 'rw')
        self._add_mount(mount_list, 'INSTALL_PATH', output_dir, output_docker_path, 'rw')

        subprocess.check_call(['docker', 'run'] + docker_run_extra_params() +
                              ['--network', 'none', '-w', source_docker_path] + mount_list +
                              [self.platform_sources.builder_image] + cmd_args)


class TargetConfig(AiArtifact):

    ARTIFACT_CLASS_NAME = "com_bonseyes/types#target_config"
    DEFAULT_ARTIFACT_FILE_NAME = 'target.yml'

    def __init__(self, package: Package, artifact_name: str, data: Dict):
        super().__init__(package, artifact_name, data)
        self._platform_build = None  # type: Optional[PlatformBuild]

    @classmethod
    def create(cls, target_dir: str, context: Context, build_name: str) -> 'TargetConfig':
        os.makedirs(target_dir, exist_ok=True)

        output_file = os.path.join(target_dir, 'target.yml')

        artifact_name = context.find_artifact_name(output_file)

        os.makedirs(target_dir, exist_ok=True)

        with open(output_file, 'w') as fp:
            json.dump({'platform_build': build_name}, fp)

        return cls.load(artifact_name, context)

    @property
    def target_dir(self):
        return self.abs_base_path

    @property
    def platform_build_name(self) -> str:
        return self.data['platform_build']

    @property
    def platform_build(self) -> PlatformBuild:
        if self._platform_build is None:
            self._platform_build = PlatformBuild.load(self.platform_build_name, self.package.context)

        return self._platform_build

    @property
    def _setup_complete_file(self) -> str:
        return os.path.join(self.target_dir, 'setup.complete')

    def is_setup(self) -> bool:
        return os.path.exists(self._setup_complete_file)

    @property
    def _manager_server_ref(self) -> str:
        return os.path.join(self.target_dir, 'manager_container')

    @property
    def manager_server_container(self):
        return Container.load_from_ref(self._manager_server_ref)

    @property
    def manager_server_address(self) -> str:
        if not self.is_manager_server_running():
            raise Exception("Manager server not running")

        return 'http://%s:%d' % self.manager_server_container.get_address(5000)

    def start_manager_server(self):
        target_volume = Volume.load_from_path(self.target_dir)

        manager_image = self.platform_build.platform_sources.manager_image
        dev_volume = Volume.load_from_path("/dev")

        container = Container.create(image_name=manager_image,
                                     command=['server', '--host', '0.0.0.0', '--port', '5000'],
                                     volumes=[VolumeEntry(volume=target_volume, mount_point='/target', mode='ro'), VolumeEntry(volume=dev_volume, mount_point="/dev", mode="rw")],
                                     environment={'TARGET_CONFIG_DIR': '/target'}, expose_ports=[5000])

        container.save_to_ref(self._manager_server_ref)

        try:
            for _ in range(10):

                if not container.is_running():

                    break

                try:
                    with requests.get(self.manager_server_address) as _:
                        break
                except requests.exceptions.ConnectionError:
                    sleep(1)
            else:
                raise Exception("Timeout while waiting target server on %s to become available" %
                                self.manager_server_address)

        except Exception:
            print("------ Manager server logs ------")
            container.print_logs()
            print("---------------------------------")
            container.remove()
            raise

    def stop_manager_server(self):
        container = self.manager_server_container
        container.stop()
        container.remove()

    def is_manager_server_running(self):
        if os.path.exists(self._manager_server_ref):
            return self.manager_server_container.is_running()
        else:
            return False

    def setup_target(self, force: bool):
        if not self.platform_build.is_built:
            raise Exception("Platform build is not ready")

        if self.is_setup():
            if force:
                os.unlink(self._setup_complete_file)
            else:
                logger.info("Setup already completed")

        os.makedirs(self.target_dir, exist_ok=True)

        subprocess.check_call(['docker', 'run'] + docker_run_extra_params() +
                              ['--network', 'host',
                               '--privileged',
                               '-v', os.path.abspath(
                                  self.platform_build.platform_sources.platform_sources_dir) + ':/platform:rw',
                               '-v', os.path.abspath(self.platform_build.build_dir) + ':/build:rw',
                               '-v', os.path.abspath(self.target_dir) + ':/target:rw',
                               '-v', '/dev:/dev:rw',
                               '-e', 'PLATFORM_DIR=/platform',
                               '-e', 'DEPS_DIR=/build/deps',
                               '-e', 'OS_DIR=/build/os',
                               '-e', 'TARGET_CONFIG_DIR=/target',
                               '-e', 'HOST_TARGET_CONFIG_DIR=' + os.path.abspath(self.target_dir),
                               '-e', 'HOST_PLATFORM_DIR=' + os.path.abspath(
                                  self.platform_build.platform_sources.platform_sources_dir),
                               '-e', 'HOST_DEPS_DIR=' + os.path.abspath(self.platform_build.build_dir) + '/deps',
                               '-e', 'HOST_OS_DIR=' + os.path.abspath(self.platform_build.build_dir) + '/os',
                               self.platform_build.platform_sources.support_image, 'setup.sh'])

        with open(self._setup_complete_file, 'w') as _:
            pass

