from typing import Dict


class Metadata:

    def __init__(self, data: Dict):
        self.data = data

    @property
    def title(self):
        return self.data.get('title')

    @property
    def description(self):
        return self.data.get('description')
