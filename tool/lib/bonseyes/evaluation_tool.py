import os

import subprocess

import sys

from typing import Dict

from bonseyes.ai_artifact import AiArtifact
from bonseyes.schemas import SCHEMAS


class EvaluationTool(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#evaluation_tool'
    DATA_SCHEMA = SCHEMAS['evaluation_tool']
    DEFAULT_ARTIFACT_FILE_NAME = 'evaluation_tool.yml'

    @property
    def image(self):
        return self.data['image']

    @image.setter
    def image(self, new_image: str):
        self.data['image'] = new_image

    @property
    def input_datasets(self):
        if 'data' in self.data and self.is_generic_tool:  # old schema version doesn't have 'data' property
            raise Exception("Generic evaluation tool doesn't have input datasets.")

        return self.data.get('input_datasets', {'main': None}).keys()

    @property
    def parameters_schema_path(self):
        if 'parameters' not in self.data:
            return None

        return os.path.join(self.abs_base_path, self.data['parameters'])

    @property
    def is_generic_tool(self):
        return self.data.get('is_generic_tool', False)

    def create_evaluation_report(self, target_url: str, dataset_dirs: Dict[str, str],
                                 output_dir: str, parameters: str = None, cache_dir: str = None):

        # check that all the required datasets have been provided
        if not self.is_generic_tool and set(dataset_dirs.keys()) != set(self.input_datasets):
            raise Exception("Expected datasets %s but found %s" % (str(list(self.input_datasets)),
                                                                   str(list(dataset_dirs.keys()))))

        docker_extra_params = []
        eval_tool_params = []

        # check if we are running in a pseudoterminal and if that is the case attach it to docker
        if sys.stdout.isatty():
            docker_extra_params += ['-ti']

        # check that the input dirs or files actually exists
        for dataset_dir in dataset_dirs.values():
            if not os.path.exists(dataset_dir):
                raise Exception("Data dir '%s' doesn't exist" % str(dataset_dir))

        # check that the parameters files actually exists if specified
        if parameters is not None:
            if not os.path.isfile(parameters):
                raise Exception("Parameters file '%s' is not a valid file" % str(parameters))

            parameters = os.path.abspath(parameters)

            docker_extra_params.extend(['-v', parameters + ':' + parameters + ':ro'])
            eval_tool_params.extend(['--parameters', parameters])

        # check that the output dir doesn't exists yet
        if os.path.exists(output_dir):
            raise Exception("Output directory '%s' already exists" % str(output_dir))

        # create the output dir
        os.makedirs(output_dir)

        # find the absolute path of output dir and mount it in the docker
        output_dir = os.path.abspath(output_dir)
        docker_extra_params.extend(['-v', output_dir + ':' + output_dir])
        eval_tool_params.extend(['--output-dir', output_dir])

        # setup mounts and arguments for each dataset dir
        for dataset_name, dataset_dir in dataset_dirs.items():
            dataset_dir = os.path.abspath(dataset_dir)
            docker_extra_params.extend(['-v', dataset_dir + ':' + dataset_dir])

            # depending on the number of dataset dirs pass the name or not to the tool
            if len(dataset_dirs) == 1:
                eval_tool_params.extend(['--dataset-dir', dataset_dir])
            else:
                eval_tool_params.extend(['--dataset-dir', dataset_name, dataset_dir])

        # set uid and gid in the docker to match the ones used by the current user
        docker_extra_params.extend(['-u', str(os.getuid()) + ':' + str(os.getgid())])

        # add the target url to the parameters of the evaluation tool
        eval_tool_params.extend(['--target-url', target_url])

        # add cache directory to the parameters of evaluation tool (if passed)
        if cache_dir:
            cache_dir = os.path.abspath(cache_dir)
            os.makedirs(cache_dir, exist_ok=True)
            docker_extra_params.extend(['-v', cache_dir + ':/cache'])
            eval_tool_params.extend(['--cache-dir', '/cache'])

        # execute the evaluation tool
        subprocess.check_call(['docker', 'run'] + docker_extra_params +
                              [self.image] + eval_tool_params)
