import os


from bonseyes.ai_artifact import AiArtifact
from bonseyes.schemas import SCHEMAS
from bonseyes.utils import validate_schema


class NetworkFormat(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#network_format'
    DATA_SCHEMA = SCHEMAS['network_format']

    DEFAULT_ARTIFACT_FILE_NAME = 'network_format.yml'

    @classmethod
    def validate(cls, artifact_path: str, context, data):
        super(NetworkFormat, cls).validate(artifact_path, context, data)

        if 'parameters' in data:
            validate_schema(os.path.join(os.path.dirname(artifact_path), data['parameters']))

    @property
    def parameters_schema_path(self):
        if 'parameters' not in self.data:
            return None

        return os.path.join(self.abs_base_path, self.data['parameters'])
