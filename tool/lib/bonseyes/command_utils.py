from contextlib import contextmanager
from typing import Optional

from bonseyes.context import Context
from bonseyes.platform import TargetConfig
from bonseyes.target_manager import TargetManager


def check_manager_args(manager_url, target_config):
    if target_config is None and manager_url is None:
        raise Exception("Either target-config or manager-url must be specified")

    if target_config is not None and manager_url is not None:
        raise Exception("Either target-config or manager-url can be specified")


@contextmanager
def connect_to_target(context: Context, manager_url: Optional[str]=None, target_config: Optional[str]=None) -> TargetManager:

    check_manager_args(manager_url, target_config)

    start_manager = False
    target_obj = None

    # start the manager server if necessary
    if manager_url is None:

        target_config_name = context.register_package_for_artifact(target_config)

        target_obj = TargetConfig.load(target_config_name, context)

        start_manager = not target_obj.is_manager_server_running()

        if start_manager:
            target_obj.start_manager_server()

        target_manager_address = target_obj.manager_server_address
    else:
        target_manager_address = manager_url

    try:

        # return the manager
        yield TargetManager(target_manager_address)

    finally:

        # stop the manager if it was started by us
        if start_manager:
            target_obj.stop_manager_server()

