import base64
import json
import os
from collections import namedtuple
from typing import Optional, Tuple, List

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding


class AuthorKeys:

    def __init__(self, path: str):
        self.path = path

    @staticmethod
    def create(output_dir: str):
        os.makedirs(output_dir, exist_ok=True)

        key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())

        with open(os.path.join(output_dir, 'private.pem'), "wb") as f:
            f.write(key.private_bytes(encoding=serialization.Encoding.PEM,
                                      format=serialization.PrivateFormat.TraditionalOpenSSL,
                                      encryption_algorithm=serialization.NoEncryption()))

        with open(os.path.join(output_dir, 'public.pem'), "wb") as f:
            f.write(key.public_key().public_bytes(encoding=serialization.Encoding.PEM,
                                                  format=serialization.PublicFormat.SubjectPublicKeyInfo))

        return AuthorKeys(output_dir)

    def create_redistribution_license_signature(self,
                                                redistribution_license: 'RedistributionLicense') -> 'RedistributionLicenseSignature':

        with open(os.path.join(self.path, 'private.pem'), 'rb') as fp:
            author_key = serialization.load_pem_private_key(fp.read(), password=None, backend=default_backend())

        with open(redistribution_license.path, 'rb') as fp:
            license_data = fp.read()

        signature = author_key.sign(license_data, padding.PKCS1v15(), hashes.SHA256())

        with open(os.path.join(redistribution_license.signature_path), "wb") as fp:
            fp.write(base64.encodebytes(signature))

        return redistribution_license.signature

    def verify_redistribution_license_signature(self, redistribution_license: 'RedistributionLicense'):

        with open(os.path.join(self.path, 'public.pem'), 'rb') as fp:
            author_key = serialization.load_pem_public_key(fp.read(), backend=default_backend())

        with open(redistribution_license.path, 'rb') as fp:
            license_data = fp.read()

        with open(redistribution_license.signature.path, 'rb') as fp:
            signature = base64.decodebytes(fp.read())

        author_key.verify(signature, license_data, padding.PKCS1v15(), hashes.SHA256())

    @staticmethod
    def load(output_dir: str):
        return AuthorKeys(output_dir)


class RedistributionLicense:
    def __init__(self, path: str):
        self.path = path

    @property
    def signature_path(self):
        return self.path + '.sha256'

    @property
    def signature(self):
        if not os.path.exists(self.signature_path):
            raise FileNotFoundError("Cannot find signature for license %s" % self.signature_path)

        return RedistributionLicenseSignature.load(self.signature_path)

    @staticmethod
    def create(path: str,
               target: str,
               assigner: str,
               pay_amount: Optional[Tuple[int, str]] = None,
               recipients: Optional[List[str]] = None,
               regions: Optional[List[str]] = None):

        sell_constraints = []

        if pay_amount is not None:
            sell_constraints.append({"leftOperand": "payAmount", "operator": "eq", "rightOperand": pay_amount[0],
                                     "unit": pay_amount[1]})

        if recipients is not None:
            for recipient in recipients:
                sell_constraints.append({"leftOperand": "recipient", "operator": "eq", "rightOperand": recipient})

        if regions is not None:
            for region in regions:
                sell_constraints.append({"leftOperand": "spatial", "operator": "eq", "rightOperand": region})

        license_data = {
            "@context": ["http://www.w3.org/ns/odrl.jsonld"],
            "@type": "Set",
            "uid": "http://bonseyes.com/policy:1010",
            "target": target,
            "assigner": assigner,
            "assignee": "marketplace@bonseyes.ai",
            "permission": [{"action": "show"},
                           {"action": "see"},
                           {"action": "sell", "constraint": sell_constraints}],
            "prohibition": [{"action": "store"},
                            {"action": "reproduce"},
                            {"action": "UsePayload"},
                            {"action": "AccessPayload"},
                            {"action": "StorePayload"}]
        }

        with open(path, 'w') as fp:
            json.dump(license_data, fp, indent=True)

        return RedistributionLicense(path)

    @staticmethod
    def load(path: str):
        return RedistributionLicense(path)


class RedistributionLicenseSignature:
    def __init__(self, path: str):
        self.path = path

    @staticmethod
    def load(path: str):
        return RedistributionLicenseSignature(path)


LicenseArgs = namedtuple('LicenseArgs', ['amount', 'regions', 'recipients', 'author_keys'])


def add_license_args(parser, author_keys=True):

    if author_keys:
        parser.add_argument('--author-keys', required=True, help="Directory with author keys")

    parser.add_argument('--amount', help="Cost of the end user license (format: '999 USD'")
    parser.add_argument('--recipients',
                        help="End-users that can obtain a license (format: email,email,email)")
    parser.add_argument('--regions', help="Region of end-users that can obtain a license "
                                          "(format: https://www.wikidata.org/wiki/QXX,https://www.wikidata.org/wiki/QYY)")


def parse_license_args(args):

    amount = tuple(args.amount.split(' ')) if args.amount is not None else None
    regions = tuple(args.regions.split(',')) if args.regions is not None else None
    recipients = tuple(args.recipients.split(',')) if args.recipients is not None else None
    author_keys = AuthorKeys.load(args.author_keys) if 'author_keys' in args else None

    return LicenseArgs(amount=amount, regions=regions,
                       author_keys=author_keys, recipients=recipients)
