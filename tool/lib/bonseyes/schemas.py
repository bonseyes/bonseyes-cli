import os

from bonseyes.utils import get_data_dir

TOP_DIR = os.path.join(get_data_dir(), 'schemas')

SCHEMAS = {
    'algorithm': os.path.join(TOP_DIR, 'core', 'algorithm.yml'),
    'algorithm_config': os.path.join(TOP_DIR, 'core', 'algorithm_config.yml'),
    'ai_app': os.path.join(TOP_DIR, 'core', 'ai_app.yml'),
    'solution': os.path.join(TOP_DIR, 'core', 'solution.yml'),
    'runtime': os.path.join(TOP_DIR, 'core', 'runtime.yml'),
    'deployment_config': os.path.join(TOP_DIR, 'core', 'deployment_config.yml'),
    'interface': os.path.join(TOP_DIR, 'core', 'interface.yml'),
    'challenge': os.path.join(TOP_DIR, 'core', 'challenge.yml'),
    'data_reference': os.path.join(TOP_DIR, 'core', 'data.yml'),
    'dataset': os.path.join(TOP_DIR, 'core', 'dataset.yml'),
    'evaluation_tool': os.path.join(TOP_DIR, 'core', 'evaluation_tool.yml'),
    'data_tool': os.path.join(TOP_DIR, 'core', 'data_tool.yml'),
    'deployment_tool': os.path.join(TOP_DIR, 'core', 'deployment_tool.yml'),
    'onnx_tool': os.path.join(TOP_DIR, 'core', 'onnx_tool.yml'),
    'evaluation_report': os.path.join(TOP_DIR, 'core', 'evaluation_report.yml'),
    'model': os.path.join(TOP_DIR, 'core', 'model.yml'),
    'package': os.path.join(TOP_DIR, 'core', 'package.yml'),
    'platform': os.path.join(TOP_DIR, 'core', 'platform.yml'),
    'network_format': os.path.join(TOP_DIR, 'core', 'network_format.yml'),
    'metadata': os.path.join(TOP_DIR, 'core', 'metadata.yml'),
    'metadata_marketplace': os.path.join(TOP_DIR, 'metadata', 'marketplace.yml')
}
