import ruamel
import os

from bonseyes.editors import ModelEditor


def setup_parser(parser):
    parser.add_argument('output_directory', help="Where to save the model")
    parser.set_defaults(func=create_model)


def create_model(context, args):

    os.makedirs(args.output_directory, exist_ok=True)

    file_name = os.path.join(args.output_directory, 'model.yml')

    model_holder = [None]

    # edit the challenge
    editor = ModelEditor.create(instance_parent=model_holder, parent_attr=0, context=context)
    editor.edit_instance()

    yaml = ruamel.yaml.YAML()

    with open(file_name, 'w') as fp:
        yaml.dump(editor.instance, fp)
