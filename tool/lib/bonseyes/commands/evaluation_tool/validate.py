from bonseyes.context import Context
from bonseyes.evaluation_tool import EvaluationTool


def setup_parser(parser):
    parser.add_argument('--evaluation-tool', required=True,
                        help="Name of the data tool")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    tool_name = context.register_package_for_artifact(args.evaluation_tool)

    EvaluationTool.load(tool_name, context)

