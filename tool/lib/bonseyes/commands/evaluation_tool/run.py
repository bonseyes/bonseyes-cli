import os

from bonseyes.context import Context
from bonseyes.evaluation_tool import EvaluationTool


def setup_parser(parser):
    parser.add_argument('--evaluation-tool', required=True,
                        help="Name of the evaluation tool")
    parser.add_argument('--image',
                        help="Override the image in the manifest")
    parser.add_argument('--parameters',
                        help="Parameters to use when running the tool")
    parser.add_argument('--named-dataset-dir', nargs=2, action="append",
                        help="Name and corresponding directory containing used in the evaluation procedure")
    parser.add_argument('--dataset-dir',
                        help="Directory containing a single dataset used in the evaluation procedure")
    parser.add_argument('--output-dir', required=True,
                        help="Directory where the evaluation result will be stored")
    parser.add_argument('--worker-url', required=True,
                        help="URL of the AI App that must be evaluated")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    evaluation_tool_name = context.register_package_for_artifact(args.evaluation_tool)

    tool = EvaluationTool.load(evaluation_tool_name, context)

    if args.image is not None:
        tool.image = args.image

    dataset_dirs = {}

    if args.named_dataset_dir is not None:
        for name, dataset_dir in args.named_dataset_dir:
            dataset_dirs[name] = dataset_dir

    if args.dataset_dir is not None:
        dataset_dirs['main'] = args.dataset_dir

    tool.create_evaluation_report(
        args.worker_url.strip('/') + '/inference',
        dataset_dirs,
        args.output_dir,
        args.parameters)
