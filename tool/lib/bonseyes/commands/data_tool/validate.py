from bonseyes.context import Context
from bonseyes.data_tool import DataTool


def setup_parser(parser):
    parser.add_argument('--data-tool', required=True,
                        help="Name of the data tool")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    data_tool_name = context.register_package_for_artifact(args.data_tool)

    DataTool.load(data_tool_name, context)

