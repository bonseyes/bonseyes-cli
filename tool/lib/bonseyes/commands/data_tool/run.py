from bonseyes.context import Context
from bonseyes.data_tool import DataTool


def setup_parser(parser):
    parser.add_argument('--data-tool', required=True,
                        help="Name of the data tool")
    parser.add_argument('--credentials', help="Credentials used to access the data")
    parser.add_argument('--image',
                        help="Override the image in the manifest")
    parser.add_argument('--parameters',
                        help="Parameters to use when running the tool")
    parser.add_argument('--input-dir',
                        help="Directory containing the input data for the tool")
    parser.add_argument('--output-dir', required=True,
                        help="Directory where the dataset will be stored")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    data_tool_name = context.register_package_for_artifact(args.data_tool)

    tool = DataTool.load(data_tool_name, context)  # type: DataTool

    if args.image is not None:
        tool.image = args.image

    tool.create_dataset(args.output_dir, args.parameters, args.input_dir, args.credentials)
