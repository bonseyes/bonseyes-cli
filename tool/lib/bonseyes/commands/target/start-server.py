from bonseyes.context import Context
from bonseyes.platform import TargetConfig


def setup_parser(parser):
    parser.add_argument('--target-config', help="Path or name of the target config to use")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    target_config_name = context.register_package_for_artifact(args.target_config)

    target = TargetConfig.load(target_config_name, context)  # type: TargetConfig

    print("Starting server")
    target.start_manager_server()

    print("Target manager container %s is available at %s" %
          (target.manager_server_container.cid, target.manager_server_address))
