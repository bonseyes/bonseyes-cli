from bonseyes.context import Context
from bonseyes.platform import PlatformBuild
from bonseyes.platform import TargetConfig


def setup_parser(parser):
    parser.add_argument('--target-config',
                        help="Path to the directory where the target config will be stored")
    parser.add_argument('--platform-build', required=True, help="Path or name of the platform build to use")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    try:
        context.register_package_for_artifact(args.target_config)
    except FileNotFoundError:
        # create a new package if the directory target config is not in a package
        context.create_new_package(args.target_config)

    platform_build_name = context.register_package_for_artifact(args.platform_build, False)

    platform_build = PlatformBuild.load(platform_build_name, context)

    target = platform_build.create_target_config(args.target_config)  # type: TargetConfig

    target.setup_target(force=False)

    print("Target config %s created in %s" % (target.artifact_name, target.abs_base_path))
