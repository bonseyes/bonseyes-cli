import argparse

import sys

from bonseyes.ai_app import AiApp, Solution
from bonseyes.ai_app import Runtime
from bonseyes.command_utils import connect_to_target
from bonseyes.context import Context
from bonseyes.target_manager import TargetManager


def setup_parser(parser: argparse.ArgumentParser):
    parser.add_argument('--target-config', help="Target config name or path (if manager-url "
                                                "is not specified")
    parser.add_argument('--manager-url', help="URL of the manger (if target-config is "
                                              "not specified)")

    sub_commands = parser.add_subparsers(dest='sub_command')

    sub_parser = sub_commands.add_parser('ai-app-delete')
    sub_parser.add_argument('--name', required=True)

    sub_commands.add_parser('ai-app-list')

    sub_parser = sub_commands.add_parser('ai-app-manifest')
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('ai-app-upload')
    sub_parser.add_argument('--name')
    sub_parser.add_argument('--ai-app', required=True)

    sub_parser = sub_commands.add_parser('config-list')

    sub_parser = sub_commands.add_parser('config-get')
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('config-set')
    sub_parser.add_argument('--name', required=True)
    sub_parser.add_argument('--value', required=True)

    sub_parser = sub_commands.add_parser('monitor-clear')
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('monitor-info')
    sub_parser.add_argument('--name', required=True)

    sub_commands.add_parser('monitor-list')

    sub_commands.add_parser('monitor-start')

    sub_parser = sub_commands.add_parser('monitor-stop')
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('runtime-delete')
    sub_parser.add_argument('--name', required=True)

    sub_commands.add_parser('runtime-list')

    sub_parser = sub_commands.add_parser('runtime-manifest')
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('runtime-upload')
    sub_parser.add_argument('--runtime', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-delete')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-list')
    sub_parser.add_argument('--solution', required=True)

    sub_parser = sub_commands.add_parser('solution-config-is-running')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-log')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-show')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-start')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-stop')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-config-upload')
    sub_parser.add_argument('--solution', required=True)
    sub_parser.add_argument('--name', required=True)
    sub_parser.add_argument('--solution-config', required=True)

    sub_parser = sub_commands.add_parser('solution-delete')
    sub_parser.add_argument('--name', required=True)

    sub_commands.add_parser('solution-list')

    sub_parser = sub_commands.add_parser('solution-manifest')
    sub_parser.add_argument('--name', required=True)

    sub_parser = sub_commands.add_parser('solution-upload')
    sub_parser.add_argument('--name', required=True)
    sub_parser.add_argument('--solution', required=True)

    sub_commands.add_parser('system-address')
    sub_commands.add_parser('system-info')
    sub_commands.add_parser('get-mcu-benchmark')
    
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    with connect_to_target(context, args.manager_url, args.target_config) as manager:  # type: TargetManager
        if args.sub_command == 'ai-app-delete':
            manager.delete_ai_app(args.name)
        elif args.sub_command == 'ai-app-list':
            print('\n'.join(manager.list_ai_apps()) + '\n')
        elif args.sub_command == 'ai-app-manifest':
            print(manager.get_ai_app_manifest(args.name))
        elif args.sub_command == 'ai-app-upload':
            context.register_package_for_artifact(args.ai_app)
            ai_app_name = context.find_artifact_name(args.ai_app)
            ai_app = AiApp.load(ai_app_name, context)  # type: AiApp
            print(manager.upload_ai_app(ai_app.abs_base_path, args.name))
        elif args.sub_command == 'config-list':
            print('\n'.join(manager.list_configs()) + '\n')
        elif args.sub_command == 'config-get':
            print(manager.get_config(args.name))
        elif args.sub_command == 'config-set':
            manager.set_config(args.name, args.value)
        elif args.sub_command == 'monitor-clear':
            manager.clear_monitor(args.name)
        elif args.sub_command == 'monitor-info':
            print(manager.get_monitor_info(args.name))
        elif args.sub_command == 'monitor-list':
            print('\n'.join(manager.list_monitors()) + '\n')
        elif args.sub_command == 'monitor-start':
            print(manager.start_monitor())
        elif args.sub_command == 'monitor-stop':
            manager.stop_monitor(args.name)
        elif args.sub_command == 'runtime-delete':
            manager.delete_runtime(args.name)
        elif args.sub_command == 'runtime-list':
            print('\n'.join(manager.list_runtimes()) + '\n')
        elif args.sub_command == 'runtime-manifest':
            print(manager.get_runtime_manifest(args.name))
        elif args.sub_command == 'runtime-upload':
            context.register_package_for_artifact(args.runtime)
            runtime_name = context.find_artifact_name(args.runtime)
            runtime = Runtime.load(runtime_name, context)  # type: Runtime
            print(manager.upload_runtime(runtime.abs_base_path, args.name))
        elif args.sub_command == 'solution-config-delete':
            manager.delete_solution_config(args.solution, args.name)
        elif args.sub_command == 'solution-config-list':
            print('\n'.join(manager.list_solution_configs(args.solution)) + '\n')
        elif args.sub_command == 'solution-config-show':
            print(manager.get_solution_config(args.solution, args.name))
        elif args.sub_command == 'solution-config-upload':
            with open(args.solution_config) as fp:
                data = fp.read()
            print(manager.upload_solution_config(data, args.solution, args.name))
        elif args.sub_command == 'solution-config-is-running':
            print(manager.is_solution_config_running(args.solution, args.name))
        elif args.sub_command == 'solution-config-start':
            print(manager.start_solution_config(args.solution, args.name))
        elif args.sub_command == 'solution-config-stop':
            print(manager.stop_solution_config(args.solution, args.name))
        elif args.sub_command == 'solution-config-log':
            for data in manager.get_solution_config_log(args.solution, args.name):
                sys.stdout.buffer.write(data)
        elif args.sub_command == 'solution-delete':
            manager.delete_solution(args.name)
        elif args.sub_command == 'solution-list':
            print('\n'.join(manager.list_solutions()) + '\n')
        elif args.sub_command == 'solution-manifest':
            print(manager.get_solution_manifest(args.name))
        elif args.sub_command == 'solution-upload':
            context.register_package_for_artifact(args.solution)
            solution_name = context.find_artifact_name(args.solution)
            solution = Solution.load(solution_name, context)  # type: Solution
            print(manager.upload_solution(solution.abs_base_path, args.name))
        elif args.sub_command == 'system-address':
            print(manager.get_system_address())
        elif args.sub_command == 'system-info':
            print(manager.get_system_info())
        elif args.sub_command == 'get-mcu-benchmark':
            print(manager.get_mcu_benchmark())
        else:
            raise Exception("Invalid command")
