import ruamel
import os

from bonseyes.editors import ChallengeEditor


def setup_parser(parser):
    parser.add_argument('challenge_directory', help="Directory containing the challenge")
    parser.set_defaults(func=create_challenge)


def create_challenge(context, args):

    if os.path.exists(args.challenge_directory):
        raise Exception("Challenge already exists")

    os.makedirs(args.challenge_directory)

    file_name = os.path.join(args.challenge_directory, 'challenge.yml')

    challenge_holder = [None]

    # edit the challenge
    editor = ChallengeEditor.create(instance_parent=challenge_holder, parent_attr=0, context=context)
    editor.edit_instance()

    yaml = ruamel.yaml.YAML()

    with open(file_name, 'w') as fp:
        yaml.dump(editor.instance, fp)

