import ruamel
import os

from bonseyes.editors import ChallengeEditor


def setup_parser(parser):
    parser.add_argument('challenge_path', help="Location of the challenge directory")
    parser.add_argument('--target-path', help="Edit a specific part of the challenge")
    parser.set_defaults(func=edit_challenge)


def edit_challenge(context, args):
    yaml = ruamel.yaml.YAML()

    file_name = os.path.join(args.challenge_path, 'challenge.yml')

    # load the challenge if it already exists
    if not os.path.exists(file_name):
        raise Exception("Challenge doesn't exist")

    challenge_holder = []

    # edit the challenge
    with open(file_name) as fp:
        challenge_holder.append(yaml.load(fp))

    # edit the challenge
    if args.target_path is not None:
        target_path = args.target_path.split('.')
    else:
        target_path = None

    editor = ChallengeEditor.create(instance_parent=challenge_holder, parent_attr=0, context=context)

    editor.edit_instance(target_path)

    with open(file_name, 'w') as fp:
        yaml.dump(editor.instance, fp)
