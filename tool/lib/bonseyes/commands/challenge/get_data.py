import os
import shutil

from bonseyes.challenge import Challenge
from bonseyes.context import Context


def setup_parser(parser):
    parser.add_argument('--challenge', required=True, help="Name of the challenge")
    parser.add_argument('--type', choices=['training', 'evaluation', 'sample'],
                        required=True, help="Type of data")
    parser.add_argument('--credentials', help="Directory with credentials used by data tools")
    parser.add_argument('--output-dir', required=True, help="Directory where to store the data")

    parser.add_argument(
        '--force',
        action="store_true",
        help="Force overwrite existing datasets",
        default=False)

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    # remove existing datasets if --force has been specified
    if args.force:
        if os.path.isdir(args.output_dir):
            shutil.rmtree(args.output_dir)

    challenge_name = context.register_package_for_artifact(args.challenge)

    # create the datasets
    challenge = Challenge.load(challenge_name, context)
    challenge.context = context

    if args.type == 'evaluation':
        challenge.create_evaluation_datasets(args.output_dir, args.credentials)

    elif args.type == 'training':
        if challenge.training_data is None:
            raise Exception("No training data in the challenge")

        challenge.training_data.create_dataset(args.output_dir, args.credentials)
    elif args.type == 'sample':

        if challenge.sample_data is None:
            raise Exception("No sample data in the challenge")

        challenge.sample_data.create_dataset(args.output_dir, args.credentials)
