import os

from bonseyes.command_utils import check_manager_args
from bonseyes.context import Context
from bonseyes.dataset import Dataset
from bonseyes.deployment_tool import DeploymentTool
from bonseyes.platform import TargetConfig
from bonseyes.tool import logger


def setup_parser(parser):
    parser.add_argument('--ai-app', required=True, help='AI-App (folder) to be benchmarked')
    parser.add_argument('--deployment-package', required=True, help='LPDNN platform deployment package')
    parser.add_argument('--target-config', required=True, help='Target platform config obtained through a DPE setup')
    parser.add_argument('--dataset', required=True, help='Folder containing images to infer the AI-App on')
    parser.add_argument('--rgb', help='Flag to specify if the input images are RGB. This flag should be followd of the WxH of the image, e.g. --rgb 640,480')
    parser.add_argument('--dset-gt', help='Ground truth file to compute the accuracy on the given dataset')
    parser.add_argument('--number', help='Number of samples to infer from the dataset')
    parser.add_argument('--filename', help='File to store the HW measurement results')
    parser.add_argument('--force-cp', action='store_true', help='Force copy the runtime in the target device to update the libraries')

    parser.set_defaults(func=execute)


def execute(context: Context, args):
    deployment_tool_path = os.path.join(args.deployment_package, 'optimizer_tool.yml')
    deployment_tool_name = context.register_package_for_artifact(deployment_tool_path, False)
    tool = DeploymentTool.load(deployment_tool_name, context)  # type: DeploymentTool

    dataset_name = context.register_package_for_artifact(args.dataset)
    dataset = Dataset.load(dataset_name, context)

    try:
        context.register_package_for_artifact(args.ai_app)
    except FileNotFoundError:
        # create a new package if the ai-app directory is not in a package
        context.create_new_package(args.ai_app)

    try:
        context.register_package_for_artifact(args.deployment_package)
    except FileNotFoundError:
        # create a new package if the release directory is not in a package
        context.create_new_package(args.deployment_package)
    
    filename = None
    if args.filename is not None:
        filename = args.filename

    dset_gt = None
    if args.dset_gt is not None:
        dset_gt = args.dset_gt
        try:
            context.register_package_for_artifact(dset_gt)
        except FileNotFoundError:
            # create a new package if the output directory is not in a package
            context.create_new_package(dset_gt)
    
    number_samples = int(args.number)
    # Min number of samples is 2: 1 warm-up + 1 benchmark
    if number_samples < 2:
        number_samples = 2

    # Create target manager if needed
    manager_url = None
    manager_started = False
    try:
        if tool.data.get('target_required', False):
            check_manager_args(args.target_config, None)

            if manager_url is None:
                target_config_name = context.register_package_for_artifact(args.target_config)
                target = TargetConfig.load(target_config_name, context)

                if not target.is_manager_server_running():
                    target.start_manager_server()
                    manager_started = True

                manager_url = target.manager_server_address
                logger.info("Manager has been started " + target.manager_server_address)

        tool.benchmark_analyze(args.deployment_package, args.ai_app, manager_url,
                                 dataset, number_samples, args.rgb, dset_gt, filename, args.force_cp)

    finally:
        # stop the manager if it was started by us
        if manager_started:
            target.stop_manager_server()

