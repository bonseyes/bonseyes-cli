import os
import shutil
from tempfile import TemporaryDirectory

import sys

from bonseyes.ai_app import AiApp
from bonseyes.command_utils import connect_to_target
from bonseyes.context import Context
from bonseyes.tool import logger


def setup_parser(parser):
    parser.add_argument('--ai-app', required=True, help="Ai App name or path")
    parser.add_argument('--credentials', help="Directory with credentials used by data tools")
    parser.add_argument('--evaluation-data', help="Directory where to store the datasets")
    parser.add_argument('--cache-dir', help="Cache directory used to speed up evaluation")
    parser.add_argument('--target-config', help="Target config name or path (if manager-url is not specified")
    parser.add_argument('--manager-url', help="URL of the manger (if target-config is not specified)")
    parser.add_argument('--reinstall', action="store_true")
    parser.add_argument('output_dir')

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    if os.path.exists(args.output_dir):
        raise Exception("Benchmark already exists")

    http_port = 8080

    ai_app_name = context.register_package_for_artifact(args.ai_app)

    ai_app = AiApp.load(ai_app_name, context)  # type: AiApp

    challenge = ai_app.challenge
    runtime = ai_app.runtime
    worker = ai_app.get_http_worker()

    config_name = None

    with connect_to_target(context, args.manager_url, args.target_config) as manager:

        try:

            logger.info("Installing runtime")
            if runtime.install_to_target(manager, args.reinstall):
                logger.info("Runtime installed successfully")
            else:
                logger.info("Runtime was already installed")

            logger.info("Installing ai-app")
            if ai_app.install_to_target(manager, args.reinstall):
                logger.info("Ai-app installed successfully")
            else:
                logger.info("Ai-app was already installed")

            logger.info("Installing HTTP worker")
            if worker.install_to_target(manager, args.reinstall):
                logger.info("Worker installed successfully")
            else:
                logger.info("Worker was already installed")

            logger.info("Starting HTTP worker")
            config = ai_app.create_http_worker_config(manager, http_port)
            config_name = manager.upload_solution_config(config, worker.get_install_name(manager))
            manager.start_solution_config(worker.get_install_name(manager), config_name)
            logger.info("HTTP Worker has been started")

            worker_address = 'http://' + manager.get_system_address() + ":" + str(http_port) + '/inference'

            if args.evaluation_data is None:

                with TemporaryDirectory() as tmp_dir:
                    eval_data_path = os.path.join(tmp_dir, 'data')

                    logger.info("Preparing evaluation data")
                    challenge.create_evaluation_datasets(eval_data_path, args.credentials)

                    logger.info("Running evaluation procedure")
                    challenge.evaluate_ai_app(worker_address, eval_data_path, args.output_dir, args.cache_dir)

            else:

                if not os.path.exists(args.evaluation_data):
                    try:
                        logger.info("Preparing evaluation data")
                        challenge.create_evaluation_datasets(args.evaluation_data, args.credentials)
                    except:
                        logger.info("Error while preparing evaluation data, deleting partial data...")
                        shutil.rmtree(args.evaluation_data, ignore_errors=True)
                        raise

                logger.info("Running evaluation procedure")
                challenge.evaluate_ai_app(worker_address, args.evaluation_data, args.output_dir, args.cache_dir)

        except:

            if config_name is not None:
                for data in manager.get_solution_config_log(worker.get_install_name(manager), config_name):
                    sys.stdout.buffer.write(data)

            raise

        finally:

            if config_name is not None:
                logger.info("Stopping HTTP worker")
                manager.stop_solution_config(worker.get_install_name(manager), config_name)
