import os

from bonseyes.ai_app import AiApp
from bonseyes.command_utils import connect_to_target, check_manager_args
from bonseyes.context import Context
from bonseyes.platform import TargetConfig
from bonseyes.target_manager import TargetManager
from bonseyes.tool import logger


def setup_parser(parser):
    parser.add_argument('--ai-app', required=True)
    parser.add_argument('--target-config', help="Target config name or path (if manager-url is not specified")
    parser.add_argument('--manager-url', help="URL of the manger (if target-config is not specified)")
    parser.add_argument('--reinstall', action="store_true")

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    ai_app_name = context.register_package_for_artifact(args.ai_app)
    ai_app = AiApp.load(ai_app_name, context)  # type: AiApp

    runtime = ai_app.runtime
    demo = ai_app.get_demo()

    check_manager_args(args.target_config, args.manager_url)

    manager_url = args.manager_url

    if manager_url is None:
        target_config_name = context.register_package_for_artifact(args.target_config)
        target = TargetConfig.load(target_config_name, context)

        if not target.is_manager_server_running():
            target.start_manager_server()

        manager_url = target.manager_server_address
        logger.info("Manager has been started " + target.manager_server_address)

    manager = TargetManager(manager_url)

    logger.info("Installing runtime")
    if runtime.install_to_target(manager, args.reinstall):
        logger.info("Runtime installed successfully")
    else:
        logger.info("Runtime was already installed")

    logger.info("Installing ai-app")
    if ai_app.install_to_target(manager, args.reinstall):
        logger.info("Ai-app installed successfully")
    else:
        logger.info("Ai-app was already installed")

    logger.info("Installing demo")
    if demo.install_to_target(manager, args.reinstall):
        logger.info("Demo installed successfully")
    else:
        logger.info("Demo was already installed")

    logger.info("Starting demo")
    config = ai_app.create_demo_config(manager)
    manager.upload_solution_config(config, demo.get_install_name(manager), 'default')
    manager.start_solution_config(demo.get_install_name(manager), 'default')
    logger.info("Demo has been started")

