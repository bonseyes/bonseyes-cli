from time import sleep

import requests
import sys

from bonseyes.ai_app import AiApp
from bonseyes.command_utils import check_manager_args
from bonseyes.context import Context
from bonseyes.platform import TargetConfig
from bonseyes.target_manager import TargetManager
from bonseyes.tool import logger


def setup_parser(parser):
    parser.add_argument('--ai-app', required=True)
    parser.add_argument('--target-config', help="Target config name or path (if manager-url is not specified")
    parser.add_argument('--manager-url', help="URL of the manger (if target-config is not specified)")
    parser.add_argument('--reinstall', action="store_true")

    parser.set_defaults(func=execute)


def execute(context: Context, args):
    http_port = 8080

    ai_app_name = context.register_package_for_artifact(args.ai_app)

    ai_app = AiApp.load(ai_app_name, context)  # type: AiApp
    ai_app.context = context

    runtime = ai_app.runtime
    worker = ai_app.get_http_worker()

    check_manager_args(args.target_config, args.manager_url)

    manager_url = args.manager_url

    if manager_url is None:
        target_config_name = context.register_package_for_artifact(args.target_config)
        target = TargetConfig.load(target_config_name, context)

        if not target.is_manager_server_running():
            target.start_manager_server()

        manager_url = target.manager_server_address
        logger.info("Manager has been started " + target.manager_server_address)

    manager = TargetManager(manager_url)

    logger.info("Installing runtime")
    if runtime.install_to_target(manager, args.reinstall):
        logger.info("Runtime installed successfully")
    else:
        logger.info("Runtime was already installed")

    logger.info("Installing ai-app")
    if ai_app.install_to_target(manager, args.reinstall):
        logger.info("Ai-app installed successfully")
    else:
        logger.info("Ai-app was already installed")

    logger.info("Installing HTTP worker")
    if worker.install_to_target(manager, args.reinstall):
        logger.info("Worker installed successfully")
    else:
        logger.info("Worker was already installed")

    logger.info("Starting HTTP worker")
    config = ai_app.create_http_worker_config(manager, http_port)

    solution_name = worker.get_install_name(manager)

    if 'default' in manager.list_solution_configs(solution_name):
        manager.delete_solution_config(solution_name, 'default')

    config_name = manager.upload_solution_config(config, solution_name, 'default')
    manager.start_solution_config(solution_name, config_name)

    worker_address = 'http://' + manager.get_system_address() + ":" + str(http_port)

    try:

        for _ in range(10):

            if not manager.is_solution_config_running(solution_name, 'default'):
                raise Exception("Worker has stopped")

            try:
                with requests.get(worker_address) as _:
                    break
            except requests.exceptions.ConnectionError:
                sleep(1)
        else:
            raise Exception("Timeout while waiting for the worker at %s to start" % worker_address)

    except Exception:
        print("------ Worker logs ------")
        for log in manager.get_solution_config_log(solution_name, 'default'):
            sys.stdout.buffer.write(log)
        print("---------------------------------")
        raise

    logger.info("HTTP Worker has been started " + worker_address)

