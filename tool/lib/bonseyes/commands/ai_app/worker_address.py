
from bonseyes.ai_app import AiApp
from bonseyes.command_utils import check_manager_args
from bonseyes.context import Context
from bonseyes.platform import TargetConfig
from bonseyes.target_manager import TargetManager


def setup_parser(parser):
    parser.add_argument('--ai-app', required=True)
    parser.add_argument('--target-config', help="Target config name or path (if manager-url is not specified")
    parser.add_argument('--manager-url', help="URL of the manger (if target-config is not specified)")

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    ai_app_name = context.register_package_for_artifact(args.ai_app)

    ai_app = AiApp.load(ai_app_name, context)  # type: AiApp
    ai_app.context = context

    check_manager_args(args.target_config, args.manager_url)

    worker = ai_app.get_http_worker()

    manager_url = args.manager_url

    if manager_url is None:
        target_config_name = context.register_package_for_artifact(args.target_config)
        target = TargetConfig.load(target_config_name, context)

        manager_url = target.manager_server_address

    manager = TargetManager(manager_url)

    config = manager.get_solution_config(worker.get_install_name(manager), 'default').decode()

    port = {x[0]: x[1] for x in [y.split('=') for y in config.strip().split('\n')]}['PORT']

    print('http://' + manager.get_system_address() + ':' + port)
