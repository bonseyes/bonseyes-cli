from bonseyes.algorithm import AlgorithmConfig
from bonseyes.challenge import Challenge
from bonseyes.command_utils import check_manager_args
from bonseyes.context import Context
from bonseyes.dataset import Dataset
from bonseyes.deployment_tool import DeploymentTool, DeploymentConfig
from bonseyes.platform import TargetConfig
from bonseyes.tool import logger


def setup_parser(parser):
    parser.add_argument('--deployment-tool', required=True)
    parser.add_argument('--algorithm-config', required=True)
    parser.add_argument('--challenge')
    parser.add_argument('--deployment-config')
    parser.add_argument('--target-config', help="Target config name or path (if manager-url is not specified")
    parser.add_argument('--manager-url', help="URL of the manger (if target-config is not specified)")
    parser.add_argument('--dataset', help="Dataset")
    parser.add_argument('output_dir')

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    deployment_tool_name = context.register_package_for_artifact(args.deployment_tool, False)
    tool = DeploymentTool.load(deployment_tool_name, context)    # type: DeploymentTool

    algorithm_config_name = context.register_package_for_artifact(args.algorithm_config)
    algorithm_config = AlgorithmConfig.load(algorithm_config_name, context)

    deployment_config = None
    quant_params = None
    dset_gt = None
    debug = False
    if args.deployment_config is not None:
        deployment_config_name = context.register_package_for_artifact(args.deployment_config)
        deployment_config = DeploymentConfig.load(deployment_config_name, context)
        deployment_params = deployment_config.get_parameters()
        if deployment_params is not None:
            quant_params = deployment_params.get('quant-params')
            dset_gt = deployment_params.get('dset-gt')
            if deployment_params.get('options') is not None:
                debug = True if '--debug' in deployment_params.get('options').split(" ") else False

    challenge = None
    if args.challenge is not None:
        challenge_name = context.register_package_for_artifact(args.challenge)
        challenge = Challenge.load(challenge_name, context)

    dataset = None
    if args.dataset is not None:
        dataset_name = context.register_package_for_artifact(args.dataset)
        dataset = Dataset.load(dataset_name, context)

    try:
        context.register_package_for_artifact(args.output_dir)
    except FileNotFoundError:
        # create a new package if the output directory is not in a package
        context.create_new_package(args.output_dir)

    if quant_params is not None:
        try:
            context.register_package_for_artifact(quant_params)
        except FileNotFoundError:
            # create a new package if the quant_params directory is not in a package
            context.create_new_package(quant_params)

    if dset_gt is not None:
        try:
            context.register_package_for_artifact(dset_gt)
        except FileNotFoundError:
            # create a new package if the dset_gt directory is not in a package
            context.create_new_package(dset_gt)

    # Create target manager if needed
    manager_url = None
    manager_started = False
    try:
        if tool.data.get('target_required', False):
            check_manager_args(args.target_config, args.manager_url)
            manager_url = args.manager_url

            if manager_url is None:
                target_config_name = context.register_package_for_artifact(args.target_config)
                target = TargetConfig.load(target_config_name, context)

                if not target.is_manager_server_running():
                    target.start_manager_server()
                    manager_started = True

                manager_url = target.manager_server_address
                logger.info("Manager has been started " + target.manager_server_address)

        ai_app = tool.generate_ai_app(args.output_dir, algorithm_config, deployment_config,
                                      challenge, manager_url, dataset, dset_gt, quant_params, debug)

    finally:
        # stop the manager if it was started by us
        if manager_started:
            target.stop_manager_server()

    print("Generated AI App %s in %s" % (ai_app.artifact_name, ai_app.abs_base_path))
