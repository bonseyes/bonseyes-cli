import os
import shutil

from bonseyes.context import Context
from bonseyes.licenses import RedistributionLicense, parse_license_args, \
    add_license_args
from bonseyes.marketplace import Marketplace, Repo
from bonseyes.platform import PlatformSources


def setup_parser(parser):
    parser.add_argument("--platform-sources", required=True)
    parser.add_argument("--pull-images", action="store_true")
    parser.add_argument("--output-dir", required=True)
    add_license_args(parser)

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    marketplace = Marketplace.from_env()

    artifact_name = context.register_package_for_artifact(args.platform_sources)

    platform_sources = PlatformSources.load(artifact_name, context)  # type: PlatformSources

    # copy the ai-app content
    shutil.copytree(platform_sources.abs_base_path, args.output_dir,
                    ignore=lambda x, y: ['.git'])

    # copy the ai-app dependencies
    repo = Repo(args.output_dir)
    repo.add_platform_sources_deps(platform_sources)
    repo.save_docker_images(args.pull_images)
    repo.save_packages()

    # create a license
    license_args = parse_license_args(args)

    redistribution_license = RedistributionLicense.create(
        path=os.path.join(args.output_dir, 'redistribution_license.json'),
        assigner=marketplace.user_email,
        target=repo.package_name + '#platform.yml',
        pay_amount=license_args.amount,
        recipients=license_args.recipients,
        regions=license_args.regions)

    license_args.author_keys.create_redistribution_license_signature(redistribution_license)

    print("Platform sources packaged for publication in " + args.output_dir)
