from bonseyes.marketplace import Marketplace
from bonseyes.ui import ask_choice, ask_boolean


def setup_parser(parser):
    parser.add_argument("--url", required=True)
    parser.add_argument("--ai-class-id")
    parser.add_argument("--force", action="store_true")
    parser.set_defaults(func=execute)


def execute(context, args):

    marketplace = Marketplace.from_env()

    if not marketplace.is_connected_to_gitlab():
        raise Exception('You need to authorize your GitLab account!')

    challenge_id = marketplace.get_artifact_id(args.url)

    if challenge_id:

        if args.force or \
           ask_boolean('Are you sure that you want to update the challenge on the marketplace?'):
            marketplace.refetch_challenge(challenge_id)
    else:

        if args.ai_class_id is None:

            ai_class = ask_choice("Please choose AI Class:",
                                  marketplace.ai_classes, lambda x: x.name)

        else:
            ai_classes = [x for x in marketplace.ai_classes if x.id == args.ai_class_id]

            if len(ai_classes) == 0:
                raise Exception("AI class not found")

            ai_class = ai_classes[0]

        marketplace.upload_challenge(args.url, ai_class.id)

    print('AI Challenge uploaded')
