import os
import shutil

from bonseyes.challenge import Challenge
from bonseyes.context import Context
from bonseyes.licenses import RedistributionLicense, add_license_args, \
    parse_license_args
from bonseyes.marketplace import Marketplace, Repo


def setup_parser(parser):
    parser.add_argument("--challenge", required=True)
    parser.add_argument("--pull-images", action="store_true")
    parser.add_argument("--output-dir", required=True)
    add_license_args(parser)

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    marketplace = Marketplace.from_env()

    artifact_name = context.register_package_for_artifact(args.challenge)

    challenge = Challenge.load(artifact_name, context)  # type: Challenge

    # copy the ai-app content
    shutil.copytree(challenge.abs_base_path, args.output_dir,
                    ignore=lambda x, y: ['.git'])

    # copy the ai-app dependencies
    repo = Repo(args.output_dir)
    repo.add_challenge_deps(challenge)
    repo.save_docker_images(args.pull_images)
    repo.save_packages()

    # create a license
    license_args = parse_license_args(args)

    redistribution_license = RedistributionLicense.create(
        path=os.path.join(args.output_dir, 'redistribution_license.json'),
        assigner=marketplace.user_email,
        target=repo.package_name + '#challenge.yml',
        pay_amount=license_args.amount,
        recipients=license_args.recipients,
        regions=license_args.regions)

    license_args.author_keys.create_redistribution_license_signature(redistribution_license)

    print("Challenge packaged for publication in " + args.output_dir)
