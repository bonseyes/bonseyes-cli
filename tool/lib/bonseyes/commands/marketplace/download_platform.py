from bonseyes.marketplace import Marketplace
from bonseyes.ui import ask_choice


def setup_parser(parser):
    parser.add_argument("--output-directory", required=True)
    parser.set_defaults(func=execute)


def execute(context, args):
    marketplace = Marketplace.from_env()

    try:
        platform_order = ask_choice("Please choose platform: ",
                                    marketplace.platforms_orders,
                                    lambda x: x.development_platform_environment.manifest.
                                    get('metadata', {}).get('title'))

        if platform_order.archive == '':
            print("The platform order is still being processed, please try again later")
            exit(1)

        marketplace.download_artifact(platform_order.archive, args.output_directory)
        marketplace.download_artifact_licence(platform_order.licence_archive, args.output_directory)
    except KeyError:
        print("You didn't buy any platform yet")
