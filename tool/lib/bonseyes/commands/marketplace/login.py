import getpass

from bonseyes.marketplace import Marketplace


def setup_parser(parser):
    parser.set_defaults(func=execute)
    parser.add_argument('--certificate', help='Please provide certificate path.')


def execute(context, args):

    marketplace = Marketplace.from_env()

    email = input("Email: ")
    password = getpass.getpass("Password: ")

    if args.certificate:
        ssl_passphare = getpass.getpass("SSL Client Cert Passphrase: ")
        marketplace.import_pkcs12_certificate(args.certificate, ssl_passphare)

    print('Trying to login... ', end='', flush=True)

    try:
        marketplace.login(email, password)
    except Exception as e:
        raise Exception('Login error! {}'.format(e))
    else:
        print('Done.')
