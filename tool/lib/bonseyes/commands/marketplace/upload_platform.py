from bonseyes.marketplace import Marketplace
from bonseyes.ui import ask_choice, ask_boolean


def setup_parser(parser):
    parser.add_argument("--url", required=True)
    parser.add_argument("--force", action="store_true")

    parser.set_defaults(func=execute)


def execute(context, args):

    marketplace = Marketplace.from_env()

    if not marketplace.is_connected_to_gitlab():
        raise Exception('You need to authorize your GitLab account!')

    challenge_id = marketplace.get_artifact_id(args.url)

    if challenge_id:

        if args.force or \
           ask_boolean('Are you sure that you want to update the platform on the marketplace?'):
            marketplace.refetch_platform(challenge_id)

    else:

        dp = ask_choice("Please choose the developer platform",
                        marketplace.development_platforms,
                        lambda x: x.name, allow_new=True)

        if dp is None:

            name = input("Enter the name of the hardware platform >")
            hardware_url = input("Enter the URL of the board manufacturer >")
            short_description = input("Enter a short description of the hardware platform >")
            long_description = input("Enter a short description of the hardware platform >")

            dpe_type = ask_choice("Choose the platform type:",
                                  marketplace.development_platform_types,
                                  lambda x: x.name)

            dp_id = marketplace.create_development_platform(name=name,
                                                            developer_platform_type_id=dpe_type.id,
                                                            hardware_url=hardware_url,
                                                            short_description=short_description,
                                                            long_description=long_description)
        else:
            dp_id = dp.id

        name = input("Enter the name of the platform >"),

        marketplace.upload_development_platform_environment(args.url, dp_id)

        print("Platform uploaded")
