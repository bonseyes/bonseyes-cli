import json

from bonseyes.marketplace import Marketplace
from bonseyes.ui import ask_choice, NoValidChoices


def setup_parser(parser):
    parser.add_argument("--output-directory", required=True)
    parser.set_defaults(func=execute)


def execute(context, args):
    marketplace = Marketplace.from_env()

    try:
        ai_app_order = ask_choice("Please choose ai app: ",
                                  marketplace.application_orders,
                                  lambda x: x.ai_app.manifest.get('metadata', {}).get('title'))

        if ai_app_order.archive == '':
            print("The ai app order is still being processed, please try again later")
            exit(1)

        marketplace.download_artifact(ai_app_order.archive, args.output_directory, load_docker=False)
        marketplace.download_artifact_licence(ai_app_order.licence_archive, args.output_directory)

    except NoValidChoices:
        print("You didn't buy any AI app yet")
