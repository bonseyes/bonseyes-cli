from bonseyes.marketplace import Marketplace
from bonseyes.ui import ask_choice, ask_boolean


def setup_parser(parser):
    parser.add_argument("--url", required=True)
    parser.add_argument("--challenge-url")
    parser.add_argument("--platform-url")
    parser.add_argument("--ai-class-id")
    parser.add_argument("--ai-category-id")
    parser.add_argument("--force", action="store_true")
    parser.set_defaults(func=execute)


def execute(context, args):

    marketplace = Marketplace.from_env()

    if not marketplace.is_connected_to_gitlab():
        raise Exception('You need to authorize your GitLab account!')

    app_id = marketplace.get_artifact_id(args.url)

    if app_id:

        if args.force or \
                ask_boolean('Are you sure that you want to update the AI app on the marketplace?'):
            marketplace.refetch_ai_app(app_id)

    else:

        if args.ai_class_id is not None:
            ai_classes = [x for x in marketplace.ai_classes if x.id == args.ai_class_id]

            if len(ai_classes) == 0:
                raise Exception("AI class not found")

            ai_class_id = ai_classes[0].id
        else:
            ai_class_id = ask_choice("Please choose the AI class:",
                                     marketplace.ai_classes,
                                     lambda x: x.name).id

        if args.ai_category_id is not None:
            ai_categories = [x for x in marketplace.ai_categories if x.id == args.ai_category_id]

            if len(ai_categories) == 0:
                raise Exception("AI category not found")

            ai_category_id = ai_categories[0].id
        else:
            ai_category_id = ask_choice("Please choose the AI category:",
                                        marketplace.ai_categories,
                                        lambda x: x.name).id

        if args.platform_url is not None:
            dpe_id = marketplace.get_artifact_id(args.platform_url)
        else:
            dpe_id = ask_choice("Please choose DPE:",
                                marketplace.developer_platform_environments,
                                lambda x: x.manifest.get('metadata', {}).get('title')).id

        if args.challenge_url is not None:
            challenge_id = marketplace.get_artifact_id(args.challenge_url)
        else:
            joined_challenge = ask_choice("Please choose challenge:", marketplace.joined_challenges,
                                          lambda x: x.challenge.manifest.get('metadata', {})
                                          .get('title'))

            challenge_id = joined_challenge.challenge.id

        app_id = marketplace.upload_ai_app(args.url,
                                           ai_class_id,
                                           challenge_id,
                                           dpe_id,
                                           ai_category_id)

        print('AI App uploaded with id %s' % str(app_id))
