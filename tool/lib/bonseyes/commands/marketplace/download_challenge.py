from bonseyes.marketplace import Marketplace
from bonseyes.ui import ask_choice


def setup_parser(parser):
    parser.add_argument("--output-directory", required=True)
    parser.set_defaults(func=execute)


def execute(context, args):
    marketplace = Marketplace.from_env()

    try:
        joined_challenge = ask_choice("Please choose challenge: ",
                                      marketplace.joined_challenges,
                                      lambda x: x.challenge.manifest.get('metadata').get('title'))

        if joined_challenge.archive == '':
            print("The challenge join is still being processed, please try again later")
            exit(1)

        marketplace.download_artifact(joined_challenge.archive, args.output_directory)

    except KeyError:
        print("You didn't join any challenge yet")
