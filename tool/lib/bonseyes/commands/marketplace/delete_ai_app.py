from bonseyes.marketplace import Marketplace


def setup_parser(parser):
    parser.add_argument("--url", required=True)
    parser.set_defaults(func=execute)


def execute(context, args):

    marketplace = Marketplace.from_env()

    if not marketplace.is_connected_to_gitlab():
        raise Exception('You need to authorize your GitLab account!')

    app_id = marketplace.get_artifact_id(args.url)

    if not app_id:
        raise Exception("Cannot find AI App")

    marketplace.delete_ai_app(app_id)

    print("Deleted AI app %s" % str(app_id))
