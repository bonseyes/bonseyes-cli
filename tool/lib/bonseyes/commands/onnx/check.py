from bonseyes.command_utils import check_manager_args
from bonseyes.context import Context
# TODO: Add check that lpdnn_python_lib is installed
from bonseyes.onnx_tool import OnnxTool

def setup_parser(parser):
    parser.epilog = ("To use onnx check you need to have the lpdnn_python_lib installed. "
                     "If the registry is setup correctly you should be able to install it using 'pip install lpdnn_python_lib'")
    parser.add_argument('--model', required=True, help='Path to the ONNX model')
    parser.set_defaults(func=execute)

def execute(context: Context, args):
    OnnxTool.run_checker(args.model)

