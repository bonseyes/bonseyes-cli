import subprocess

import sys

from bonseyes.context import Context


def setup_parser(parser):
    parser.set_defaults(func=execute)


def print_title(text):
    print("\n\033[1m\033[4m%s\033[0m\n" % text)


def print_error(text):
    print("\033[31m%s\033[0m" % text)


def print_warning(text):
    print("\033[33m%s\033[0m" % text)


def check_version(command, found_version, tested_version):
    print_title(command)
    print("  Recommended: " + tested_version)
    print("  Detected: " + found_version)

    if found_version == tested_version:
        print("  Version: \033[32mOK\033[0m")
    else:
        print("  Version: \033[33mWARNING\033[0m")

    return found_version == tested_version


def run(command):
    return subprocess.check_output(command).strip().decode()


def execute(context: Context, args):

    problems_detected = False

    try:
        os_version = run(['lsb_release', '-s', '-d'])
        os_ok = check_version("OS Version", os_version, "Ubuntu 20.04.3 LTS")
    except FileNotFoundError:
        print_error("Cannot detect OS version")
        os_ok = False

    problems_detected = not os_ok or problems_detected

    try:
        docker_client_version = run(['docker', 'version', '--format', '{{.Client.Version}}'])

        docker_client_ok = check_version("Docker Client", docker_client_version, "19.03")

        docker_server_version = run(['docker', 'version', '--format', '{{.Server.Version}}'])

        docker_server_ok = check_version("Docker Server", docker_server_version, "19.03")

        docker_ok = docker_client_ok and docker_server_ok

    except FileNotFoundError:
        print_error("Cannot find docker")
        docker_ok = False

    problems_detected = not docker_ok or problems_detected

    try:
        git_version = run(['git', 'version'])
        git_ok = check_version("Git", git_version, "git version 2.17.1")
    except FileNotFoundError:
        print("Cannot find git")
        git_ok = False

    problems_detected = not git_ok or problems_detected

    try:
        git_lfs_version = run(['git', 'lfs', 'version'])

        git_lfs_ok = check_version("Git LFS", git_lfs_version,
                                   "git-lfs/2.3.4 (GitHub; linux amd64; go 1.8.3)")

        try:
            if run(['git', 'config', 'filter.lfs.clean']) != "git-lfs clean -- %f" or \
               run(['git', 'config', 'filter.lfs.smudge']) != "git-lfs smudge -- %f" or \
               run(['git', 'config', 'filter.lfs.process']) != "git-lfs filter-process":
                git_lfs_ok = False
                print_error("Config: git lfs not configured with standard configuration")
            else:
                print("  Config: \033[32mOK\033[0m")

        except subprocess.CalledProcessError:
            git_lfs_ok = False
            print_error("Config: git lfs not configured (did you run 'git lfs install')")

    except FileNotFoundError:
        print_error("Git LFS not working")
        problems_detected = True

    problems_detected = not git_lfs_ok or problems_detected

    if problems_detected:
        sys.exit(1)
