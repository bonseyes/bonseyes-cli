from bonseyes.context import Context
from bonseyes.platform import PlatformSources


def setup_parser(parser):
    parser.add_argument('--platform-sources', help="Source of the platform")
    parser.add_argument('--item', choices=['name','builder','manager','support'], help="Item to get")
    parser.set_defaults(func=execute)


def execute(context: Context, args):
    platform_sources_name = context.register_package_for_artifact(args.platform_sources, False)

    platform_sources = PlatformSources.load(platform_sources_name, context)  # type: PlatformSources

    if args.item is None:
        print("name: " + platform_sources.artifact_name)
        print("manger docker image: " + platform_sources.manager_image)
        print("builder docker image: " + platform_sources.builder_image)
        print("support docker image: " + platform_sources.support_image)
    elif args.item == 'name':
        print(platform_sources.artifact_name)
    elif args.item == 'manager':
        print(platform_sources.manager_image)
    elif args.item == 'builder':
        print(platform_sources.builder_image)
    elif args.item == 'support':
        print(platform_sources.support_image)
