import gzip
import os
import subprocess
import tarfile

from bonseyes.context import Context
from bonseyes.platform import PlatformBuild


def setup_parser(parser):
    parser.add_argument('--platform-build', required=True,
                        help="Path or name of the platform build to use")
    parser.add_argument('--snapshot-dir', required=True,
                        help="Directory where to save snapshot")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    platform_build_name = context.register_package_for_artifact(args.platform_build, False)

    platform_build = PlatformBuild.load(platform_build_name, context)  # type: PlatformBuild

    os.makedirs(args.snapshot_dir, exist_ok=True)

    print("Saving docker images...")
    with gzip.open(os.path.join(args.snapshot_dir, 'docker-images.tar.gz'), 'wb') as fp:
        subprocess.check_call(['docker', 'save', platform_build.platform_sources.support_image,
                                                 platform_build.platform_sources.manager_image,
                                                 platform_build.platform_sources.builder_image],
                              stdout=fp)

    print("Saving platform build data...")
    with tarfile.open(os.path.join(args.snapshot_dir, 'build.tar.gz'), "w:gz") as tar:
        tar.add(platform_build.abs_base_path, arcname=".")
