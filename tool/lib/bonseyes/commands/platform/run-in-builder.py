import argparse
from bonseyes.context import Context
from bonseyes.platform import PlatformBuild


def setup_parser(parser):
    parser.add_argument('--platform-build', required=True, help="Path or name of the platform build to use")
    parser.add_argument('--sources-dir', required=True, help="Path to the source directory")
    parser.add_argument('--cache-dir', required=False, help="Build cache directory")
    parser.add_argument('--output-dir', required=False, help="Build result directory")
    parser.add_argument('--internal-paths', action='store_true')
    parser.add_argument('cmd_args', nargs=argparse.REMAINDER)
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    platform_build_name = context.register_package_for_artifact(args.platform_build, False)

    platform_build = PlatformBuild.load(platform_build_name, context)

    if len(args.cmd_args) and args.cmd_args[0] == '--':
        del args.cmd_args[0]
    platform_build.run_in_builder(args.cmd_args, args.sources_dir, args.cache_dir, args.output_dir, args.internal_paths)

