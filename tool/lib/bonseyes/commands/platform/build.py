import logging

import sys

from bonseyes.context import Context
from bonseyes.platform import PlatformSources

import bonseyes.platform


def setup_parser(parser):
    parser.add_argument('--platform-sources', help="Source of the platform")
    parser.add_argument('output_package_dir', help="Path where the built platform will be stored")
    parser.add_argument('--force', action="store_true", help="Force rebuild of existing build")
    parser.set_defaults(func=execute)


def execute(context: Context, args):
    platform_sources_name = context.register_package_for_artifact(args.platform_sources, False)

    platform_sources = PlatformSources.load(platform_sources_name, context)
    platform_build = platform_sources.build(args.output_package_dir, force=args.force)

    print("Platform build  %s created in %s" % (platform_build.artifact_name, args.output_package_dir))

