import gzip
import os
import subprocess
import tarfile

from bonseyes.context import Context


def setup_parser(parser):
    parser.add_argument('--platform-build-dir', required=True,
                        help="Path where to save the platfrom build")
    parser.add_argument('--snapshot-dir', required=True,
                        help="Directory containing the snapshot to load")
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    os.makedirs(args.platform_build_dir, exist_ok=True)

    print("Loading docker images...")
    with gzip.open(os.path.join(args.snapshot_dir, 'docker-images.tar.gz'), 'rb') as fp:
        subprocess.check_call(['docker', 'load'], stdin=fp)

    print("Loading platform build data...")
    with tarfile.open(os.path.join(args.snapshot_dir, 'build.tar.gz'), "r:gz") as tar:
        tar.extractall(args.platform_build_dir)
