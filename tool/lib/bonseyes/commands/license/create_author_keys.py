from bonseyes.context import Context
from bonseyes.licenses import AuthorKeys


def setup_parser(parser):
    parser.add_argument('--output-dir', required=True, help="Output directory")
    parser.set_defaults(func=execute)


def execute(context: Context, args):
    AuthorKeys.create(args.output_dir)
