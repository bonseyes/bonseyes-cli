from bonseyes.context import Context
from bonseyes.licenses import RedistributionLicense, add_license_args, parse_license_args


def setup_parser(parser):
    parser.add_argument('--output-file', required=True, help="Output file")
    parser.add_argument('--target', required=True, help="Name of the AI artifact subject of the license (format: email)")
    parser.add_argument('--assigner', required=True, help="Marketplace user of the artifact owner")

    add_license_args(parser, author_keys=False)

    parser.set_defaults(func=execute)


def execute(context: Context, args):

    license_args = parse_license_args(args)

    RedistributionLicense.create(path=args.output_file,
                                 assigner=args.assigner,
                                 target=args.target,
                                 pay_amount=license_args.amount,
                                 recipients=license_args.recipients,
                                 regions=license_args.regions)
