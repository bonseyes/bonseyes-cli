from bonseyes.context import Context
from bonseyes.licenses import AuthorKeys, RedistributionLicense


def setup_parser(parser):
    parser.add_argument('--license', required=True, help="License to sign")
    parser.add_argument('--author-keys', required=True, help="Directory with author keys")
    parser.set_defaults(func=execute)


def execute(context: Context, args):
    author_keys = AuthorKeys.load(args.author_keys)
    redistribution_license = RedistributionLicense.load(args.license)
    author_keys.create_redistribution_license_signature(redistribution_license)
