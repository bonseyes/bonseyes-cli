from bonseyes.context import Context, Package


def setup_parser(parser):
    parser.add_argument('--name', required=True, help="Name of the package")
    parser.add_argument('package_directory', help="Directory where to create the package")
    parser.set_defaults(func=execute)


def execute(context: Context, args):
    Package.create(context, args.package_directory, args.name)
