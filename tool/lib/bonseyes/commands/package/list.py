from bonseyes.context import Context, Package


def setup_parser(parser):
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    print("{: <50} {: <50}".format('NAME', 'PATH'))

    for package in context.packages:
        print("{: <50} {: <50}".format(package.name, package.base_path))
