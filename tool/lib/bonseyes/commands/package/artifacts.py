from bonseyes.context import Context, Package


def setup_parser(parser):
    parser.set_defaults(func=execute)


def execute(context: Context, args):

    print("{: <80} {: <40} {: <50}".format('NAME', 'TYPE', 'PATH'))

    for entry in context.artifacts:

        try:
            artifact_path = context.get_artifact_path(entry.artifact_name)
        except FileNotFoundError:
            artifact_path = '<not found>'

        print("{: <80} {: <40} {: <50}".format(entry.artifact_name, entry.artifact_type, artifact_path))
