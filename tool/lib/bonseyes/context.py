import fnmatch
import os
from collections import namedtuple
import glob
from typing import List, Iterable, Optional
from uuid import uuid4

import yaml

from bonseyes.schemas import SCHEMAS
from bonseyes.utils import validate_instance

ArtifactEntry = namedtuple('ArtifactEntry', ['artifact_name', 'artifact_type'])


class Package:
    def __init__(self, context: 'Context', name: str, base_path: str,
                 artifacts: Optional[List[ArtifactEntry]] = None,
                 subpackages: Optional[List[str]] = None):
        self.context = context
        self.name = name
        self.base_path = base_path

        if subpackages is not None:
            self.artifacts = artifacts
        else:
            self.artifacts = []

        if subpackages is not None:
            self.subpackages = subpackages
        else:
            self.subpackages = []

    @classmethod
    def create(cls, context: 'Context', base_path: str, package_name: str):

        package_file = os.path.join(base_path, 'package.yml')

        os.makedirs(base_path, exist_ok=True)

        with open(package_file, 'w') as fp:
            yaml.dump({'name': package_name}, fp)

        return cls.load(context, base_path)

    @staticmethod
    def load(context: 'Context', base_path: str):
        package_file = os.path.join(base_path, 'package.yml')

        try:
            with open(package_file) as fp:
                data = yaml.safe_load(fp)
        except Exception as e:
            raise Exception("Unable to load package file %s" % package_file) from e

        validate_instance(SCHEMAS['package'], data)

        package_name = data['name']

        artifacts = []

        for artifact_glob in data.get('artifacts', {}):
            for match in glob.glob(base_path + '/' + artifact_glob['pattern']):

                for exclude in artifact_glob.get('exclude', []):

                    # skip the file if it is excluded
                    if fnmatch.fnmatch(match, base_path + '/' + exclude):
                        break
                else:

                    # file is not excluded so we can add it
                    artifact_name = package_name + '#' + os.path.relpath(match, base_path)
                    artifacts.append(ArtifactEntry(artifact_name, artifact_glob['type']))

        subpackages = []

        for package_glob in data.get('subpackages', {}):
            for match in glob.glob(base_path + '/' + package_glob['pattern']):

                for exclude in package_glob.get('exclude', []):

                    # skip the file if it is excluded
                    if fnmatch.fnmatch(match, base_path + '/' + exclude):
                        break
                else:

                    if not os.path.exists(os.path.join(match, 'package.yml')):
                        continue

                    # file is not excluded so we can add it
                    subpackages.append(match)

        return Package(context, package_name, base_path, artifacts, subpackages)

    def get_artifact_path(self, artifact_part: str) -> str:
        artifact_path = os.path.join(self.base_path, artifact_part)

        if not os.path.exists(artifact_path):
            raise FileNotFoundError("Cannot find %s#%s" % (self.name, artifact_part))

        return artifact_path


def get_abs_artifact_name(rel_artifact_name: str, base_artifact: Optional[str] = None) -> str:

    base_package_part, base_artifact_part = get_artifact_name_parts(base_artifact)

    if '#' in rel_artifact_name:
        package_part, artifact_part = get_artifact_name_parts(rel_artifact_name)

        if package_part == '':
            package_part = base_package_part
    else:
        package_part = base_package_part
        artifact_part = os.path.join(os.path.dirname(base_artifact_part), rel_artifact_name)

    return package_part + '#' + artifact_part


def is_abs_artifact_name(artifact_name: str) -> bool:
    return '#' in artifact_name and artifact_name[0] != '#'


ArtifactNameParts = namedtuple('ArtifactNameParts', ['package', 'artifact'])


def get_artifact_name_parts(artifact_name: str) -> ArtifactNameParts:
    return ArtifactNameParts(*artifact_name.split('#'))


def find_containing_package(artifact_path: str):

    if os.path.isdir(artifact_path):
        path = os.path.abspath(artifact_path)
    else:
        path = os.path.dirname(os.path.abspath(artifact_path))

    while True:
        if os.path.isfile(os.path.join(path, 'package.yml')):
            return path

        if path == '/':
            break

        path = os.path.dirname(path)

    raise FileNotFoundError("File %s is not in any package" % artifact_path)


class Context:
    def __init__(self, package_paths: List[str]):
        self.packages = [Package.load(self, package_path) for package_path in package_paths]

    def add_package(self, path: str) -> Package:

        for package in self.packages:
            if os.path.abspath(package.base_path) == os.path.abspath(path):
                return package

        package = Package.load(self, path)
        self.packages.append(package)

        # load the subpackages distributed with this package
        for subpackage in package.subpackages:
            self.add_package(subpackage)

        return package

    def get_package(self, package_name: str):
        for package in self.packages:
            if package_name == package.name:
                return package
        else:
            raise KeyError("Package %s not found" % package_name)

    def is_in_package(self, artifact_path_or_name: str):
        try:
            self.find_artifact_name(artifact_path_or_name)
            return True
        except FileNotFoundError:
            return False

    def register_package_for_artifact(self, artifact_path_or_name: str, auto_create_package: Optional[bool]=True) -> str:
        if is_abs_artifact_name(artifact_path_or_name):
            return artifact_path_or_name
        else:

            if not os.path.exists(artifact_path_or_name):
                raise FileNotFoundError("Cannot find the artifact " + artifact_path_or_name)

            try:
                package_path = find_containing_package(artifact_path_or_name)
                self.add_package(package_path)
                return self.find_artifact_name(artifact_path_or_name)
            except FileNotFoundError:

                if not auto_create_package:
                    raise

                # create a fake package

                if os.path.isdir(artifact_path_or_name):
                    package_path = artifact_path_or_name
                else:
                    package_path = os.path.dirname(artifact_path_or_name)

                package = Package(self, 'uuid_' + str(uuid4()), package_path, [])
                self.packages.append(package)
                return self.find_artifact_name(artifact_path_or_name)

    def create_new_package(self, package_path: str):
        package = Package.create(self, package_path, 'uuid_' + str(uuid4()))
        self.add_package(package.base_path)
        return package

    def find_artifact_name(self, artifact_path_or_name: str):

        # the artifact_path is already a artifact name
        if '#' in artifact_path_or_name:
            return artifact_path_or_name

        # otherwise find the package that contains the artifact_path and build a artifact_name
        abs_file_path = os.path.abspath(artifact_path_or_name)

        # find the longest matching prefix
        longest_prefix = None
        longest_prefix_package = None

        for package in self.packages:
            abs_package_path = os.path.abspath(package.base_path)

            common_prefix = os.path.commonprefix([abs_file_path, abs_package_path])

            if common_prefix != abs_package_path:
                continue

            if longest_prefix is None or len(common_prefix) > len(longest_prefix):
                longest_prefix_package = package
                longest_prefix = common_prefix

        if longest_prefix is None:
            raise FileNotFoundError("The file %s doesn't belong to any package" % artifact_path_or_name)

        return longest_prefix_package.name + '#' + \
            os.path.relpath(abs_file_path, os.path.abspath(longest_prefix_package.base_path))

    def get_artifact_package(self, artifact_name: str, parent_artifact_path: Optional[str]=None):

        if not is_abs_artifact_name(artifact_name):

            if parent_artifact_path is None:
                raise Exception('Cannot load relative artifact name %s without a'
                                'parent artifact path' % artifact_name)

            package_path = os.path.abspath(find_containing_package(parent_artifact_path))

            for package in self.packages:
                if os.path.abspath(package.base_path) == package_path:
                    return package
            else:
                raise Exception('Cannot find package for relative artifact %s (parent: %s)' % (
                    artifact_name, parent_artifact_path))

        package_part, artifact_part = get_artifact_name_parts(artifact_name)

        return self.get_package(package_part)

    def get_artifact_path(self, artifact_name: str, parent_artifact_path: Optional[str]=None):

        if not is_abs_artifact_name(artifact_name):

            if parent_artifact_path is None:
                raise Exception('Cannot load relative artifact name %s without a '
                                'parent artifact path' %
                                artifact_name)

            return os.path.join(parent_artifact_path, artifact_name)

        package_part, artifact_part = get_artifact_name_parts(artifact_name)

        return self.get_package(package_part).get_artifact_path(artifact_part)

    @property
    def artifacts(self) -> Iterable[ArtifactEntry]:
        for package in self.packages:
            for artifact in package.artifacts:
                yield artifact
