from bonseyes.ai_artifact import AiArtifact
from bonseyes.challenge import Challenge
from bonseyes.schemas import SCHEMAS
from bonseyes.target_manager import TargetManager


class AiApp(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#ai_app'
    DATA_SCHEMA = SCHEMAS['ai_app']
    DEFAULT_ARTIFACT_FILE_NAME = 'ai_app.yml'

    @property
    def runtime(self):
        return Runtime.load(self._get_abs_artifact_name(self.data['runtime']), self.package.context)

    @property
    def challenge(self):
        return Challenge.load(self._get_abs_artifact_name(self.data['challenge']), self.package.context)

    def install_to_target(self, manager: TargetManager, force: bool=False):
        if not force and manager.escape_artifact_name(self.artifact_name) in manager.list_ai_apps():
            return False

        manager.upload_ai_app(self.abs_base_path, manager.escape_artifact_name(self.artifact_name))
        return True

    def get_demo(self) -> 'Solution':
        return self.runtime.get_ai_app_demo(self.challenge.interface_name)

    def get_http_worker(self) -> 'Solution':
        return self.runtime.get_ai_app_http_worker()

    def create_demo_config(self, manager: TargetManager):
        return self.runtime.create_demo_config(manager, self)

    def create_http_worker_config(self, manager: TargetManager, http_port: int):
        return self.runtime.create_http_worker_config(manager, self, http_port)


class Runtime(AiArtifact):
    DEFAULT_ARTIFACT_FILE_NAME = 'runtime.yml'
    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#runtime'
    DATA_SCHEMA = SCHEMAS['runtime']

    def install_to_target(self, manager: TargetManager, force: bool=False):
        if not force and manager.escape_artifact_name(self.artifact_name) in manager.list_runtimes():
            return False

        manager.upload_runtime(self.abs_base_path, manager.escape_artifact_name(self.artifact_name))
        return True

    def get_ai_app_demo(self, interface_name: str) -> 'Solution':
        available_demos = [x for x in self.data.get('demos', []) if x['interface'] == interface_name]

        if len(available_demos) == 0:
            raise Exception("Cannot find demo for class ID " + interface_name)

        return Solution.load(self._get_abs_artifact_name(available_demos[0]['solution']), self.package.context)

    def get_ai_app_http_worker(self) -> 'Solution':
        return Solution.load(self._get_abs_artifact_name(self.data['http_worker']), self.package.context)

    def create_demo_config(self, manager: TargetManager, ai_app: AiApp) -> str:
        return ('AI_APP=' + manager.escape_artifact_name(ai_app.artifact_name) + '\n' +
                'RUNTIME=' + manager.escape_artifact_name(self.artifact_name))

    def create_http_worker_config(self, manager: TargetManager, ai_app: AiApp, http_port: int) -> str:
        return ('AI_APP=' + manager.escape_artifact_name(ai_app.artifact_name) + '\n' +
                'RUNTIME=' + manager.escape_artifact_name(self.artifact_name) + '\n' +
                'PORT=' + str(http_port) + '\n')


class Solution(AiArtifact):
    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#solution'
    DATA_SCHEMA = SCHEMAS['solution']
    DEFAULT_ARTIFACT_FILE_NAME = 'solution.yml'

    def get_install_name(self, manager: TargetManager):
        return manager.escape_artifact_name(self.artifact_name)

    def install_to_target(self, manager: TargetManager, force: bool=False):
        if not force and self.get_install_name(manager) in manager.list_solutions():
            return False
        manager.upload_solution(self.abs_base_path, self.get_install_name(manager))
        return True
