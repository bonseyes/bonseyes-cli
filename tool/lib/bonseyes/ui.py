import os
from typing import Dict, List, Callable, Iterable, TypeVar, Optional


def print_title(title):
    print()
    print(title)
    print("-" * len(title))
    print()


def ask_edit(current_value, context=None):
    print("The current value is:\n")
    print(current_value)
    print()

    return ask_boolean("Do you want to change it?", context)


def input_with_context(description, context=None):
    if context is None:
        return input(description + ' > ')
    else:
        print(description)
        return input(context + ' > ')


def ask_boolean(description, default=False, context=None):
    while True:

        if default:
            text = description + " [(y)/n]"
        else:
            text = description + " [y/(n)]"

        ans = input_with_context(text, context)

        if ans not in ['y', 'n', '']:
            print("Invalid answer, please enter y or n")
            continue

        if ans == '':
            return default
        else:
            return ans == 'y'


class NoValidChoices(Exception):
    pass


T = TypeVar('T')


def ask_choice(question: str, choices: Iterable[T], render_fun: Callable[[T], str],
               allow_new: bool=False, context=None) -> Optional[T]:

    choices = list(choices)

    if len(choices) == 0 and not allow_new:
        raise NoValidChoices("No options available")

    print(question)
    print()

    for index, choice in enumerate(choices):
        print("%d ) %s" % (index + 1, render_fun(choice)))

    if len(choices) == 0:
        print("No choice available, creating new.")
        return None

    print()
    while True:
        if not allow_new:
            result = input_with_context("Please select a choice", context)
        else:
            result = input_with_context("Please select a choice or type new to create a new choice",
                                        context)
            if result == 'new':
                return None

        try:
            result = int(result)
            if 0 < result <= len(choices):
                return choices[result - 1]

        except ValueError:
            pass

        print("Invalid choice")


def update_line(default_value, context=None):
    if default_value is not None:
        if not ask_edit(default_value, context):
            return default_value

    print()
    return input_with_context("Type the value and then press enter", context)


def update_option(default_value, options, context=None):
    if default_value is not None and default_value in options.keys():
        if not ask_edit(default_value, context):
            return default_value

    print("The following options are available:")
    print()

    for option_name, options_description in options.items():
        print("  " + option_name + " : " + options_description)

    print()

    while True:
        choice = input_with_context("Please enter the name of the option", context)

        if choice not in options.keys():
            print("Invalid choice")
        else:
            return choice


def input_multiline(context=None):
    print("Type the text and when completed create a line containing only a single dot.")
    print()

    text = ""

    while True:
        line = input_with_context("", context)
        if line == ".":
            return text

        if text != "":
            text += "\n"

        text += line


def update_text(default_value, context=None):

    if default_value is not None:
        if not ask_edit(default_value, context):
            return default_value

    print("Type the text and when completed create a line containing only a single dot.")
    print()

    text = ""

    while True:
        line = input_with_context("", context)
        if line == ".":
            return text

        if text != "":
            text += "\n"

        text += line


def update_list(default_value, context=None):
    if default_value is not None:
        if not ask_edit('  - ' + '\n  - '.join(default_value), context):
            return default_value

    print("Type each entry of the list when completed create a line containing only a single dot.")
    print()

    list_values = []

    while True:
        line = input_with_context("", context)
        if line == ".":
            return list_values

        list_values.append(line)


def update_file_with_list(file_name, base, context=None):
    full_file_name = os.path.join(base, file_name)

    if os.path.exists(full_file_name):
        with open(full_file_name) as fp:
            values_list = [x.strip() for x in fp.read().strip().split('\n')]
    else:
        values_list = None

    values_list = update_list(values_list, context)

    with open(full_file_name, 'w') as fp:
        fp.write("\n".join(values_list))


def update_optional_key(description_add, description_remove, dict_obj, dict_key, context=None):
    update_key = False

    if dict_key not in dict_obj:
        if ask_boolean(description_add, context):
            update_key = True
    else:
        if not ask_boolean(description_remove, context):
            update_key = True
        else:
            del dict_obj[dict_key]

    return update_key
