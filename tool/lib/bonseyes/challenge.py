from tempfile import TemporaryDirectory

import os
import json

from typing import Dict, Optional

from bonseyes.ai_artifact import AiArtifact
from bonseyes.interface import Interface
from bonseyes.data_tool import DataTool
from bonseyes.evaluation_tool import EvaluationTool
from bonseyes.schemas import SCHEMAS
from bonseyes.utils import validate_instance


class Challenge(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#challenge'
    DATA_SCHEMA = SCHEMAS['challenge']
    DEFAULT_ARTIFACT_FILE_NAME = 'challenge.yml'

    @classmethod
    def validate(cls, artifact_path: str, context, data):

        # convert from old challenge format
        if isinstance(data.get('evaluation_protocol', {}), list):
            data['evaluation_protocol'] = {str(x): y for x, y in enumerate(data['evaluation_protocol'])}

        super(Challenge, cls).validate(artifact_path, context, data)
        interface = Interface.load(data['specification']['interface']['id'], context)  # type: Interface

        if interface.parameters_schema_path is not None:
            validate_instance(interface.parameters_schema_path,
                              data['specification']['interface'].get('parameters', {}))

    @property
    def interface_name(self):
        return self.data.get('specification', {}).get('interface', {}).get('id')

    @property
    def interface_parameters(self):
        return self.data['specification']['interface'].get('parameters', {})

    @property
    def evaluation_procedures(self):
        return {x: EvaluationProcedure(self, y) for x, y in self.data.get('evaluation_protocol', {}).items()}

    def create_evaluation_datasets(self, evaluation_datasets_dir: str,
                                   credentials: Optional[str]=None):
        for name, evaluation_procedure in self.evaluation_procedures.items():
            datasets_dir = os.path.join(evaluation_datasets_dir, name)
            evaluation_procedure.create_evaluation_data(datasets_dir, credentials)

    @property
    def sample_data(self):
        sample_data = self.data.get('specification', {}).get('sample_data')
        return Data(self, sample_data) if sample_data is not None else None

    @property
    def training_data(self):
        sample_data = self.data.get('specification', {}).get('training_data')
        return Data(self, sample_data) if sample_data is not None else None

    def evaluate_ai_app(self, app_target_url: str, evaluation_datasets_dir: str,
                        output_dir: str, cache_dir: str = None):
        for name, evaluation_procedure in self.evaluation_procedures.items():
            proc_cache_dir = os.path.join(cache_dir, name) if cache_dir is not None else None

            evaluation_procedure.evaluate_ai_app(app_target_url,
                                                 datasets_dir=os.path.join(evaluation_datasets_dir,
                                                                           name),
                                                 output_dir=os.path.join(output_dir, name),
                                                 cache_dir=proc_cache_dir)


class Data:
    def __init__(self, challenge: Challenge, data: Dict):
        self.data = data
        self.challenge = challenge

    @property
    def data_tool_name(self):
        return self.data.get('tool', {}).get('id', None)

    @property
    def data_tool_parameters(self):
        return self.data['tool'].get('parameters')

    @property
    def data_tool(self):
        if self.data_tool_name is None:
            return None
        else:
            return DataTool.load(self.data_tool_name, self.challenge.package.context,
                                 self.challenge.abs_base_path)

    @property
    def input_data(self):
        if 'input_data' in self.data.get('tool', {}):
            return os.path.join(self.challenge.abs_base_path, self.data['tool']['input_data'])
        else:
            return None

    def validate(self):
        data_tool = self.data_tool

        if data_tool.parameters_schema_path is not None:
            validate_instance(data_tool.parameters_schema_path, self.data_tool_parameters or {})
        elif self.data_tool_parameters is not None:
            raise Exception("Unexpected parameters")

        if self.data.get('input_data_required', False) and self.input_data is None:
            raise Exception("Missing input data")

    @property
    def is_precomputed_dataset(self):
        return 'dataset' in self.data

    @property
    def dataset_name(self):
        return self.data.get('dataset')

    def create_dataset(self, dataset_dir: str, credentials: Optional[str]=None):

        self.validate()

        if 'dataset' in self.data:
            raise Exception("Cannot create dataset when it is pre-created")

        data_tool = self.data_tool

        with TemporaryDirectory() as tmp_dir:

            # save the parameters if required to a temporary file
            if data_tool.parameters_schema_path is not None:
                parameters_path = os.path.join(tmp_dir, 'parameters.json')

                with open(parameters_path, 'w') as fp:
                    json.dump(self.data_tool_parameters or {}, fp)
            else:
                parameters_path = None

            data_tool.create_dataset(output_dir=dataset_dir,
                                     parameters=parameters_path,
                                     input_dir=self.input_data,
                                     credentials=credentials)


class EvaluationProcedure:
    def __init__(self, challenge: Challenge, data: Dict):
        self.data = data
        self.challenge = challenge

    @property
    def evaluation_tool_name(self):
        return self.data.get('tool', {}).get('id', None)

    @property
    def evaluation_tool_parameters(self):
        return self.data['tool'].get('parameters')

    @property
    def evaluation_tool(self) -> EvaluationTool:
        return EvaluationTool.load(self.evaluation_tool_name,
                                   self.challenge.package.context,
                                   self.challenge.abs_base_path)

    def validate(self):
        eval_tool = self.evaluation_tool

        if eval_tool.parameters_schema_path is not None:
            validate_instance(eval_tool.parameters_schema_path, self.evaluation_tool_parameters or {})
        elif self.evaluation_tool_parameters is not None:
            raise Exception("Unexpected parameters")

        if not self.evaluation_tool.is_generic_tool and (set(self.evaluation_data.keys()) != eval_tool.input_datasets):
            raise Exception("Evaluation data input mismatch expected " + str(eval_tool.input_datasets) +
                            " found " + str(self.evaluation_data.keys()))

    def create_evaluation_data(self, evaluation_datasets_dir: str, credentials: Optional[str]=None):
        for dataset_name, data in self.evaluation_data.items():
            dataset_dir = os.path.join(evaluation_datasets_dir, dataset_name)

            if data.is_precomputed_dataset:
                continue

            data.create_dataset(dataset_dir, credentials)

    @property
    def evaluation_data(self) -> Dict[str, Data]:
        if 'evaluation_data' in self.data:
            # the old syntax that supports only one dataset is used
            return {'main': Data(self.challenge, self.data['evaluation_data'])}
        elif 'data' in self.data:
            # the new syntax is used
            return {name: Data(self.challenge, data) for name, data in self.data['data'].items()}
        else:
            # this is an evaluation without data
            return {}

    def evaluate_ai_app(self, app_target_url: str, datasets_dir: str,
                        output_dir: str, cache_dir: str = None):

        self.validate()

        evaluation_tool = self.evaluation_tool

        with TemporaryDirectory() as tmp_dir:

            if evaluation_tool.parameters_schema_path is not None:
                parameters_path = os.path.join(tmp_dir, 'parameters.json')

                with open(parameters_path, 'w') as fp:
                    json.dump(self.evaluation_tool_parameters or {}, fp)
            else:
                parameters_path = None

            datasets = {}
            dataset_names = self.data['data'].keys() if 'data' in self.data else evaluation_tool.input_datasets

            for name in dataset_names:
                if self.evaluation_data[name].is_precomputed_dataset:
                    datasets[name] = self.challenge.package.context.get_artifact_path(
                        self.evaluation_data[name].dataset_name, self.challenge.abs_base_path)
                else:
                    datasets[name] = os.path.join(datasets_dir, name)

            evaluation_tool.create_evaluation_report(target_url=app_target_url,
                                                     dataset_dirs=datasets,
                                                     output_dir=output_dir,
                                                     parameters=parameters_path,
                                                     cache_dir=cache_dir)
