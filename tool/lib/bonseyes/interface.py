import os


from bonseyes.ai_artifact import AiArtifact
from bonseyes.schemas import SCHEMAS
from bonseyes.utils import validate_schema


class Interface(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#interface'
    DATA_SCHEMA = SCHEMAS['interface']

    DEFAULT_ARTIFACT_FILE_NAME = 'interface.yml'

    @classmethod
    def validate(cls, artifact_path: str, context, data):
        super(Interface, cls).validate(artifact_path, context, data)

        if 'results' in data:
            validate_schema(os.path.join(os.path.dirname(artifact_path), data['results']))

        if 'parameters' in data:
            validate_schema(os.path.join(os.path.dirname(artifact_path), data['parameters']))

        if 'ground_truth' in data:
            validate_schema(os.path.join(os.path.dirname(artifact_path), data['ground_truth']))

    @property
    def parameters_schema_path(self):
        if 'parameters' not in self.data:
            return None

        return os.path.join(self.abs_base_path, self.data['parameters'])

    @property
    def results_schema_path(self):
        if 'results' not in self.data:
            return None

        return os.path.join(self.abs_base_path, self.data['results'])

    @property
    def ground_truth_schema_path(self):
        if 'ground_truth' not in self.data:
            return None

        return os.path.join(self.abs_base_path, self.data['ground_truth'])

