import json
import os
from typing import Dict

from bonseyes.ai_artifact import AiArtifact
from bonseyes.interface import Interface
from bonseyes.context import Context
from bonseyes.schemas import SCHEMAS
from bonseyes.utils import validate_instance


class Dataset(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#algorithm'
    DATA_SCHEMA = SCHEMAS['dataset']

    DEFAULT_ARTIFACT_FILE_NAME = 'dataset.json'
    DEFAULT_ARTIFACT_DATA = {'samples': []}

    @classmethod
    def create(cls, path: str, dataset: Dict, context: Context) -> 'Context':
        package = context.create_new_package(path)

        with open(os.path.join(path, 'dataset.json'), 'w') as fp:
            json.dump(dataset, fp)

        return cls.load(package + '#dataset.json', context)

    @classmethod
    def validate(cls, artifact_path: str, context: Context, data):
        super(Dataset, cls).validate(artifact_path, context, data)

        for prop_name, prop in data.get('properties', {}).items():
            if prop['type'] == 'com_bonseyes/types#image':
                pass

            elif prop['type'] == 'com_bonseyes/types#ground_truth':
                if 'interface' not in prop:
                    raise Exception("Missing interface in property " + prop_name)

                if 'id' not in prop['interface']:
                    raise Exception("Missing interface.id in property " + prop_name)

                interface = Interface.load(prop['interface'], context)  # type: Interface

                if interface.parameters_schema_path is None:
                    if 'parameters' in prop['interface']:
                        raise Exception("Interface doesn't support parameters")
                else:
                    validate_instance(interface.parameters_schema_path,
                                      prop['interface'].get('parameters', {}))

            else:
                raise Exception("Invalid property type " + prop['type'])

        for sample in data['samples']:

            sample_name = sample['id']

            for prop_name, prop in sample.items():

                if prop_name == 'id':
                    continue

                prop_desc = data['properties'].get(prop_name)

                if prop_desc is None:
                    raise Exception("Unknown property " + prop_name + " in sample " + sample_name)

                if prop_desc['type'] == 'com_bonseyes/types#image':
                    if not isinstance(prop, str):
                        raise Exception("Invalid value for " + prop_name + " in sample " + sample_name)
                elif prop_desc['type'] == 'com_bonseyes/types#ground_truth':
                    # TODO: this is very inefficient
                    interface = Interface.load(prop['interface'], context)  # type: Interface
                    validate_instance(interface.ground_truth_schema_path, prop)
                else:
                    raise Exception("Unsupported field type " + prop_desc['type'])
