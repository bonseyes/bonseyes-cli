import site
import sys
import os
import tarfile
from io import BytesIO
from tarfile import TarInfo
from typing import Dict, Optional, Union, List
from urllib.request import urlopen

import jsonschema
import yaml


def generate_tar(path: str):
    class OutputStream:
        def __init__(self):
            self.pos = 0
            self.buffer = BytesIO()

        def tell(self):
            return self.pos

        def write(self, data):
            self.pos += len(data)
            self.buffer.write(data)

        def flush(self):
            pass

    output_stream = OutputStream()

    with tarfile.open(fileobj=output_stream, mode='w') as tar:

        dirs = [path]

        while len(dirs) > 0:

            next_dir = dirs.pop(0)

            for entry_name in os.listdir(next_dir):
                full_path = next_dir + '/' + entry_name

                if os.path.isdir(full_path):
                    dirs.append(full_path)
                else:
                    with open(full_path, mode='rb') as fp:
                        tar_info = TarInfo(name=os.path.relpath(os.path.abspath(full_path), start=path).lstrip('/'))
                        tar_info.mode = os.stat(full_path).st_mode
                        fp.seek(0, 2)
                        tar_info.size = fp.tell()
                        fp.seek(0)

                        tar.addfile(tar_info, fp)

                    yield output_stream.buffer.getvalue()

                    output_stream.buffer.seek(0)
                    output_stream.buffer.truncate()


def save_tar(fp, path: str):

    # we need to have seek working even if we only seek forward
    class TarStream:

        def __init__(self, stream):
            self.pos = 0
            self.stream = stream

        def read(self, size=-1):
            data = self.stream.read(size)
            self.pos += len(data)
            return data

        def tell(self):
            return self.pos

        def seek(self, offset, whence=0):

            if whence == 0:
                if offset < self.pos:
                    raise Exception("Cannot seek to %d" % offset)

                if offset != self.pos:
                    self.read(offset - self.pos)

            if whence == 1:
                if offset < 0:
                    raise Exception("Cannot seek to %d from current position" % offset)

                if offset == 0:
                    return

                self.read(offset)

            if whence == 2:
                raise Exception("Cannot seek to %d from end of file" % offset)

    with tarfile.TarFile(fileobj=TarStream(fp)) as f:
        f.extractall(path)


def docker_run_extra_params():
    extra_params = ['--rm']

    # check if we are running in a pseudoterminal and if that is the case attach it to docker
    if sys.stdout.isatty():
        extra_params += ['-ti']

    # set uid and gid in the docker to match the ones used by the current user
    extra_params.extend(['-u', str(os.getuid()) + ':' + str(os.getgid())])

    return extra_params


def get_data_dir():

    data_dirs = [
        os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..')),
        os.path.abspath(os.path.join(site.USER_BASE, 'share', 'bonseyes')),
        os.path.abspath(os.path.join(sys.prefix, 'share', 'bonseyes')),
        # this is to handle pip on debian which doesn't follow sys.prefix (see https://wiki.debian.org/Python)
        '/usr/local/share/bonseyes'
    ]

    for data_dir in data_dirs:
        if os.path.exists(os.path.join(data_dir, 'schemas', 'core', 'metadata.yml')):
            return data_dir

    raise FileNotFoundError('Cannot find data dir')


def validate_instance(schema_path: str, instance):
    if not os.path.isfile(schema_path):
        raise Exception("Invalid pathname for schema file: " + schema_path)

    schema = None

    with open(schema_path) as fp:
        try:
            schema = yaml.safe_load(fp)
        except BaseException as e:
            raise Exception("Error parsing schema file: " + schema_path) from e

    def yaml_handler(uri):
        return yaml.safe_load(urlopen(uri).read().decode("utf-8"))

    resolver = jsonschema.RefResolver('file://' + schema_path, schema, handlers={'file': yaml_handler})

    jsonschema.Draft4Validator(schema, resolver=resolver).validate(instance)


def validate_schema(schema_path: str):

    with open(schema_path) as fp:
        schema = yaml.safe_load(fp)

    jsonschema.Draft4Validator.check_schema(schema)


def find_subcomponent(data: Dict, role: Optional[Union[str, List[str]]], default_value):
    if role is None:
        return data

    if isinstance(role, str):
        return data.get('subcomponents', {}).get(role, default_value)

    for r in role:
        data = data.get('subcomponents', {}).get(r, default_value)

    return data
