import os
from typing import Optional, Union, List, Dict

from bonseyes.ai_artifact import AiArtifact
from bonseyes.context import get_abs_artifact_name
from bonseyes.schemas import SCHEMAS
from bonseyes.utils import validate_schema, find_subcomponent


class Algorithm(AiArtifact):

    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#algorithm'
    DATA_SCHEMA = SCHEMAS['algorithm']

    DEFAULT_ARTIFACT_FILE_NAME = 'algorithm.yml'

    @classmethod
    def validate(cls, artifact_path: str, context, data):
        super(Algorithm, cls).validate(artifact_path, context, data)

        if 'parameters' in data:
            validate_schema(os.path.join(os.path.dirname(artifact_path), data['parameters']))

    @property
    def interface_name(self):
        return self.data['interface']

    @property
    def parameters_schema_path(self):
        if 'parameters' not in self.data:
            return None
        return os.path.join(self.abs_base_path, self.data['parameters'])

    @property
    def subcomponents(self) -> Dict[str, str]:
        return {x: y['interface'] for x, y in self.data.get('subcomponents', {}).items()}


class AlgorithmConfig(AiArtifact):
    ARTIFACT_CLASS_NAME = 'com_bonseyes/types#algorithm_config'
    DATA_SCHEMA = SCHEMAS['algorithm_config']
    DEFAULT_ARTIFACT_FILE_NAME = 'algorithm.yml'

    @property
    def challenge_name(self) -> str:
        return get_abs_artifact_name(self.data['challenge'], self.artifact_name)

    def get_subcomponent(self, role: Optional[Union[str, List[str]]] = None) -> Dict:
        return find_subcomponent(self.data, role, {})
