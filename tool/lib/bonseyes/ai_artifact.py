import os
from copy import deepcopy
from typing import Dict, Type, TypeVar, Optional

import yaml

from bonseyes.context import Package, Context, get_artifact_name_parts, get_abs_artifact_name
from bonseyes.utils import validate_instance

T = TypeVar('T', bound='AiArtifact')


class AiArtifact:

    ARTIFACT_CLASS_NAME = None
    DATA_SCHEMA = None
    DEFAULT_ARTIFACT_FILE_NAME = None
    DEFAULT_ARTIFACT_DATA = None


    def __init__(self, package: Package, artifact_name: str, data: Dict):
        self.data = data
        self.package = package
        self.artifact_name = artifact_name

    def _get_abs_artifact_name(self, rel_artifact_name):
        return get_abs_artifact_name(rel_artifact_name, self.artifact_name)

    @classmethod
    def validate(cls, artifact_path: str, context: Context, data):

        if cls.DATA_SCHEMA is None:
            return

        validate_instance(cls.DATA_SCHEMA, data)

    @property
    def metadata(self):
        return self.data.get('metadata')

    @classmethod
    def load(cls: Type[T], artifact_name: str, context: Context,
             parent_artifact_path: Optional[str]=None) -> T:

        artifact_path = context.get_artifact_path(artifact_name, parent_artifact_path)
        data = None

        # some artifacts have a default file name that can be appended
        if os.path.isdir(artifact_path):
            if cls.DEFAULT_ARTIFACT_FILE_NAME is None:
                raise Exception('The artifact %s points to the directory %s' % (artifact_name, artifact_path))

            if artifact_name.endswith('#.'):
                artifact_name = artifact_name[:-1] + cls.DEFAULT_ARTIFACT_FILE_NAME
            else:
                artifact_name = artifact_name + '/' + cls.DEFAULT_ARTIFACT_FILE_NAME

            try:
                artifact_path = context.get_artifact_path(artifact_name, parent_artifact_path)
            except FileNotFoundError:
                if cls.DEFAULT_ARTIFACT_DATA is None:
                    raise
                # Use default data
                data = deepcopy(cls.DEFAULT_ARTIFACT_DATA)


        package = context.get_artifact_package(artifact_name, parent_artifact_path)

        artifact_name = context.find_artifact_name(artifact_path)

        if data is None:
            with open(artifact_path) as fp:
                data = yaml.safe_load(fp)

        cls.validate(artifact_path, context, data)

        return cls(package, artifact_name, data)

    @property
    def rel_path(self) -> str:
        """
        Returns the path of the artifact relative to the package root
        """
        return get_artifact_name_parts(self.artifact_name).artifact

    @property
    def rel_base_path(self) -> str:
        """
        Returns the path of the directory containing the artifact relative to the package root
        """
        return os.path.dirname(self.rel_path)

    @property
    def abs_base_path(self):
        """
        Returns the absolute path of the directory containing the artifact
        """
        return os.path.abspath(os.path.dirname(self.package.context.get_artifact_path(self.artifact_name)))
