import json
import os
import shutil
import subprocess
from collections import namedtuple
from typing import Optional, Dict
from uuid import uuid4

import requests
from tqdm import tqdm
from ruamel import yaml
from subprocess import check_call

from bonseyes.ai_app import Runtime, AiApp
from bonseyes.challenge import Challenge, Data
from bonseyes.data_tool import DataTool
from bonseyes.evaluation_tool import EvaluationTool

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.serialization import pkcs12

from bonseyes.platform import PlatformSources


DevelopmentPlatform = namedtuple('DevelopmentPlatform', ['id', 'name', 'development_platform_type'])
AiCategory = namedtuple('AiCategory', ['id', 'name', 'ai_class'])
AiClass = namedtuple('AiCategory', ['id', 'name'])
DevelopmentPlatformType = namedtuple('DevelopmentPlatformType', ['id', 'name'])
PublishedChallenge = namedtuple('PublishedChallenge', ['id', 'manifest'])
JoinedChallenge = namedtuple('JoinedChallenge', ['id', 'challenge', 'joined', 'archive'])
DevelopmentPlatformEnvironment = namedtuple('DevelopmentPlatformEnvironment', ['id', 'manifest'])
PublishedAiApp = namedtuple('PublishedAiApp', ['id', 'manifest'])
AiAppTuple = namedtuple('AiApp', ['id', 'url'])
AiAppOrder = namedtuple('AiAppOrder', ['id', 'archive', 'licence_archive', 'ai_app'])
DevelopmentPlatformEnvironmentOrder = namedtuple('DevelopmentPlatformEnvironmentOrder',
                                                 ['id', 'archive', 'licence_archive', 'development_platform_environment'])


class Marketplace:
    def __init__(self, url: str, credentials_path: str,
                 client_certificate_path: Optional[str] = None):
        self.url = url.rstrip('/')
        self.credentials_path = credentials_path
        self.client_certificate_path = client_certificate_path

    @staticmethod
    def from_env():
        url = os.getenv("BMP_API_URL", 'https://beta.bonseyes.com/')
        credentials_file = os.environ.get('BMP_CREDENTIALS', os.path.expanduser('~/.bmp-credentials.json'))
        certificate_file = os.environ.get('BMP_CERTIFICATE', os.path.expanduser('~/.bmp-cert.pem'))

        return Marketplace(url, credentials_file, certificate_file)

    def import_pkcs12_certificate(self, pkcs12_path: str, passphrase: str):
        with open(pkcs12_path, 'rb') as fp:
            pkcs12_cert = fp.read()

        private_key, certificate, additional_certificates = pkcs12.load_key_and_certificates(
            pkcs12_cert, passphrase.encode(),
            default_backend())

        with open(self.client_certificate_path, 'wb') as fp:
            fp.write(private_key.private_bytes(encoding=serialization.Encoding.PEM,
                                               format=serialization.PrivateFormat.TraditionalOpenSSL,
                                               encryption_algorithm=serialization.NoEncryption()))

            fp.write(certificate.public_bytes(encoding=serialization.Encoding.PEM))

            for cert in additional_certificates:
                fp.write(cert.public_bytes(encoding=serialization.Encoding.PEM))

    @property
    def _credentials_data(self):
        try:
            with open(self.credentials_path, 'r') as file:
                return json.load(file)
        except FileNotFoundError:
            raise FileNotFoundError("Not logged in to marketplace")

    @property
    def _request_cert(self):
        if not os.path.exists(self.client_certificate_path):
            return None
        else:
            return self.client_certificate_path

    @property
    def jwt_token(self):
        return 'Bearer {}'.format(self._credentials_data['token'])

    @property
    def user_id(self):
        return self._credentials_data['user']['id']

    @property
    def user_email(self):
        return self._credentials_data['user']['email']

    def _check_results(self, results):
        if results.status_code == 400 and "No required SSL certificate" in results.text:
            raise Exception("Missing SSL client certificate (error 400)")

        if results.status_code != 200:
            raise Exception("Error while processing %s\nServer answered with error %d : %s " % (
                results.url, results.status_code, results.text))

    def _query(self, query: str, variables: Optional[Dict] = None, authenticated: bool = True):

        request_data = {'query': query}

        if variables:
            request_data['variables'] = variables

        headers = {}

        if authenticated:
            headers['Authorization'] = self.jwt_token

        with requests.post(self.url + '/graphql', json=request_data,
                           headers=headers, cert=self._request_cert) as ret:

            self._check_results(ret)

            response = ret.json()

            if response.get('errors', []):
                raise Exception("Error while invoking query:" + json.dumps(response['errors']))

            return response['data']

    def is_connected_to_gitlab(self):

        user_gitlab_query = """
            query {
                userGitlab {
                gitlabId
              }
            }
        """

        gitlab_user_result = self._query(user_gitlab_query)

        return not (gitlab_user_result['userGitlab'] == 'null')

    def login(self, email: str, password: str):
        login_data = {"email": email, "password": password}

        with requests.post(self.url + '/auth/login', json=login_data,
                           cert=self._request_cert) as ret:
            self._check_results(ret)

            with open(self.credentials_path, 'w') as fp:
                json.dump(ret.json(), fp)

    @property
    def development_platforms(self):
        options_query = """
            query GetDeveloperPlatforms {
              developerPlatforms {
                edges {
                  node {
                    id
                    name
                    developerPlatformType {
                      id
                      name
                    }
                    shortDescription
                    longDescription
                  }
                }
              }
            }
        """

        for result in self._query(options_query)['developerPlatforms']['edges']:
            result = result['node']
            yield DevelopmentPlatform(id=result['id'], name=result['name'],
                                      development_platform_type=DevelopmentPlatformType(
                                          id=result['developerPlatformType']['id'],
                                          name=result['developerPlatformType']['name']))

    @property
    def ai_categories(self):
        options_query = """
            query AICategories {
              aiCategories{
                edges {
                  node {
                    id
                    name
                    aiClass {
                      id
                      name
                    }
                  }
                }
              }
            }
        """

        for result in self._query(options_query)['aiCategories']['edges']:
            result = result['node']
            yield AiCategory(id=result['id'], name=result['name'],
                             ai_class=AiClass(id=result['aiClass']['id'],
                                              name=result['aiClass']['name']))

    @property
    def ai_classes(self):
        options_query = """
            query AIClasses {
              aiClasses{
                  edges {
                    node {
                      id
                      name
                    }
                  }
                }
            }
        """

        for result in self._query(options_query)['aiClasses']['edges']:
            result = result['node']
            yield AiClass(id=result['id'], name=result['name'])

    @property
    def development_platform_types(self):
        query = """
                query developerPlatforms {
                  developerPlatformTypes {
                    edges {
                      node {
                        id
                        name
                      }
                    }
                  }
                }
                """
        for result in self._query(query)['developerPlatformTypes']['edges']:
            result = result['node']
            yield DevelopmentPlatformType(id=result['id'], name=result['name'])

    def upload_challenge(self, url: str, ai_class_id: int):
        upload_challenge_query = """
          mutation CreateChallenge(
            $url: String!,
            $aiClass: Int!,
          ) {
            createChallenge(
                url: $url,
                aiClass: $aiClass
            ) {
                challenge {
                    id
                    url        
                }
            }
          }
        """

        result = self._query(upload_challenge_query,
                             variables={'url': url,
                                        'aiClass': ai_class_id})

        return result['createChallenge']['challenge']['id']

    @property
    def joined_challenges(self):
        options_query = """
            query JoinedChallenges {
              joinedChallenges {
                edges {
                  node {
                    id
                    joined
                    challenge {
                      id
                      manifest
                      archive
                    }
                  }
                }
              }              
            }
        """

        for result in self._query(options_query)['joinedChallenges']['edges']:
            result = result['node']
            yield JoinedChallenge(id=result['id'], joined=result['joined'],
                                  archive=result['challenge']['archiveDownload'],
                                  challenge=PublishedChallenge(
                                      id=result['challenge']['id'],
                                      manifest=yaml.safe_load(result['challenge']['manifest'])))

    @property
    def developer_platform_environments(self):
        options_query = """
            query DeveloperPlatformEnvironments {
              developerPlatformEnvironments {
                edges {
                  node {
                    id
                    manifest
                  }
                }
              }
            }
        """

        for result in self._query(options_query)['developerPlatformEnvironments']['edges']:
            result = result['node']
            yield DevelopmentPlatformEnvironment(id=result['id'],
                                                 manifest=yaml.safe_load(result['manifest']))

    def upload_ai_app(self, url: str, ai_class_id: int,
                      challenge_id: int, dpe_id: int, ai_category_id: int):
        upload_aiapp_query = """
          mutation CreateApplication(
            $url: String!,
            $aiClass: Int!,
            $aiCategory: Int!,
            $challenge: Int,
            $dpe: Int    
          ) {
            createApplication(
                url: $url,
                aiClass: $aiClass,
                aiCategory: $aiCategory,
                challenge: $challenge,
                dpe: $dpe    
            ) {
                application {
                    id
                    url        
                }
            }
          }
        """

        result = self._query(upload_aiapp_query, variables={'url': url,
                                                            'aiClass': ai_class_id,
                                                            'aiCategory': ai_category_id,
                                                            'challenge': challenge_id,
                                                            'dpe': dpe_id})

        return result['createApplication']['application']['id']

    def refetch_ai_app(self, ai_app_id: int):
        fetch_aiapp_query = """
          mutation FetchApplication(
            $application: Int!
          ) {
            fetchApplication(
                application: $application
            ) {
                application {
                    id
                    url
                }
            }
          }
        """

        result = self._query(fetch_aiapp_query, variables={'application': ai_app_id})

        return result['fetchApplication']['application']['id']

    def refetch_challenge(self, challenge_id: int):
        query = """
          mutation FetchChallenge(
            $challenge: Int!
          ) {
            fetchChallenge(
                challenge: $challenge
            ) {
                challenge {
                    id
                    url
                }
            }
          }
        """

        result = self._query(query, variables={'challenge': challenge_id})

        return result['fetchChallenge']['challenge']['id']

    def refetch_platform(self, platform_id: int):
        query = """
          mutation FetchDpe(
            $platform: Int!
          ) {
            fetchDpe(
                id: $platform
            ) {
                developerPlatformEnvironment {
                    id
                    url
                }
            }
          }
        """

        result = self._query(query, variables={'platform': platform_id})

        return result['fetchDpe']['developerPlatformEnvironment']['id']

    def delete_ai_app(self, app_id: int):
        query = """
            mutation deleteAiApp($appId: Int!) {
                deleteApplication(application: $appId) {
                    ok
                }
            }
        """

        result = self._query(query, variables={'appId': app_id})

        if not result['deleteApplication']['ok']:
            raise Exception("Unable to delete ai app")

    @property
    def list_applications(self):
        list_applications_query = """
          query Applications {
            applications {
                edges {
                    node {
                        id
                        url
                    }
                }
            }
          }
        """

        for result in self._query(list_applications_query)['applications']['edges']:
            result = result['node']
            yield AiAppTuple(id=result['id'], url=result['url'])

    @property
    def application_orders(self):
        aiapp_orders_query = """
            query ApplicationOrders{
              applicationOrders {
                edges {
                  node {
                    id
                    licenceArchive
                    aiApp {
                      id
                      manifest
                      archive
                    }
                  }
                }
              }
            }
        """

        for result in self._query(aiapp_orders_query)['applicationOrders']['edges']:
            result = result['node']
            yield AiAppOrder(id=result['id'],
                             archive=result['aiApp']['archive'],
                             licence_archive=result['licenceArchive'],
                             ai_app=PublishedAiApp(
                                    id=result['aiApp']['id'],
                                    manifest=yaml.safe_load(result['aiApp']['manifest'])))

    @property
    def platforms_orders(self):
        query = """
            query DPEOrders{
              dpeOrders {
                edges {
                  node {
                    id
                    licenceArchive
                    dpe {
                      id
                      manifest
                      archive
                    }
                  }
                }
              }
            }
        """

        for result in self._query(query)['dpeOrders']['edges']:
            result = result['node']
            yield DevelopmentPlatformEnvironmentOrder(id=result['id'],
                                                      archive=result['dpe']['archive'],
                                                      licence_archive=result['licenceArchive'],
                                                      development_platform_environment=DevelopmentPlatformEnvironment(
                                                          id=result['dpe']['id'],
                                                          manifest=yaml.safe_load(result['dpe']['manifest'])))

    def upload_development_platform_environment(self, url: str, development_platform_id: int):
        upload_dpe_query = """
          mutation CreateDPE (
            $url: String!,
            $developerPlatform: Int!
          ) {
            createDeveloperPlatformEnvironment (
                url: $url,
                developerPlatform: $developerPlatform
            ) {
                developerPlatformEnvironment {
                    id                    
                    url
                }
            }
          }
        """

        ret = self._query(upload_dpe_query, variables={'url': url, 'developerPlatform': development_platform_id})

        return ret['createDeveloperPlatformEnvironment']['developerPlatformEnvironment']['id']

    def create_development_platform(self, name: str,
                                    developer_platform_type_id: int,
                                    hardware_url: str,
                                    short_description: str,
                                    long_description: str, ):

        create_dp_query = """
                        mutation CreateDP (
                            $name: String!, 
                            $hardwareUrl: String!, 
                            $shortDescription: String!,
                            $longDescription: String!,
                            $developerPlatformType: Int!) {
                          createDeveloperPlatform(
                            name: $name
                            hardwareUrl: $hardwareUrl
                            shortDescription: $shortDescription
                            longDescription: $longDescription
                            developerPlatformType: $developerPlatformType
                          ){
                            developerPlatform {
                              id
                              name
                            }
                          }
                        }
                        """

        ret = self._query(create_dp_query, variables={'name': name,
                                                      'hardwareUrl': hardware_url,
                                                      'shortDescription': short_description,
                                                      'longDescription': long_description,
                                                      'developerPlatformType': developer_platform_type_id})

        return ret['createDeveloperPlatform']['developerPlatform']['id']

    @staticmethod
    def extract_artifact(file_name: str, output_dir: str, load_docker: bool = True):
        print('Extracting files... ', flush=True)
        check_call(['tar', '-xvf', os.path.join(output_dir, file_name), '--strip-components=1',
                    '-C', output_dir])
        print('Done.')

        print('Removing archive... ', end='', flush=True)
        os.remove(os.path.join(output_dir, file_name))
        print('Done.')

        if load_docker:
            docker_images = os.path.join(output_dir, 'docker_images/docker_images.tar')
            print('Loading docker images... ', flush=True)
            check_call(['docker', 'load', '-i', f'{docker_images}'])
            print('Done.')

    def download_artifact(self, repo_url, output_dir, load_docker: bool = True):
        try:

            os.makedirs(output_dir, exist_ok=True)

            tar_file = os.path.join(output_dir, 'data.tar.gz')

            with requests.get(self.url + '/media/' + repo_url.lstrip('/'),
                              headers={'Authorization': self.jwt_token},
                              cert=self._request_cert, stream=True) as response:

                self._check_results(response)

                total_size = int(response.headers.get('content-length', 0))
                block_size = 1024

                with tqdm(total=total_size, unit='iB', unit_scale=True) as t:
                    with open(tar_file, "wb") as file:
                        while True:
                            buf = response.raw.read(block_size)
                            if not buf:
                                break
                            file.write(buf)
                            t.update(len(buf))

                print('Download finished')

            self.extract_artifact('data.tar.gz', output_dir, load_docker)
        except Exception as e:
            raise Exception('Error while downloading the ai app: {}'.format(e)) from e

    def download_artifact_licence(self, url, output_dir):
        try:
            tar_file = os.path.join(output_dir, 'licence.tar.gz')

            with requests.get(self.url + '/media/' + url.lstrip('/'),
                              headers={'Authorization': self.jwt_token},
                              cert=self._request_cert, stream=True) as response:

                self._check_results(response)

                total_size = int(response.headers.get('content-length', 0))
                block_size = 1024

                with tqdm(total=total_size, unit='iB', unit_scale=True) as t:
                    with open(tar_file, "wb") as file:
                        while True:
                            buf = response.raw.read(block_size)
                            if not buf:
                                break
                            file.write(buf)
                            t.update(len(buf))

                print('Download finished')

            self.extract_artifact('licence.tar.gz', output_dir, False)
        except Exception as e:
            raise Exception('Error while downloading the ai app user license: {}'.format(e)) from e

    def get_artifact_id(self, url):
        ai_artifact_by_url_query = """
          query GetAIArtifactByURL(
            $url: String!
          ) {
            aiArtifact (
                url: $url
            ) {
                id
            }
          }
        """

        response = self._query(ai_artifact_by_url_query, variables={'url': url})

        return (response.get('aiArtifact') or {}).get('id')


class Repo:

    def __init__(self, base_path: str):
        self.base_path = base_path
        self.packages = {}
        self.docker_images = set()

        if self.package_data is not None:
            self.package_name = self.package_data['name']
        else:
            self.package_name = 'uuid_' + str(uuid4())

    @property
    def package_data(self):
        if not os.path.exists(self.package_file):
            return None

        with open(self.package_file) as fp:
            return yaml.safe_load(fp)

    @property
    def package_file(self):
        return os.path.join(self.base_path, 'package.yml')

    def save_docker_images(self, pull_images: bool = False):

        if pull_images:
            for docker_image in self.docker_images:
                print("Pulling image " + docker_image)
                subprocess.check_call(['docker', 'pull', docker_image])

        if len(self.docker_images) == 0:
            return

        os.makedirs(os.path.join(self.base_path, 'docker_images'), exist_ok=True)

        print("Saving all images")
        with open(os.path.join(self.base_path, 'docker_images', 'docker_images.tar'), 'wb') as fp:
            subprocess.check_call(['docker', 'save'] + list(self.docker_images), stdout=fp)

    def save_packages(self):
        os.makedirs(self.base_path, exist_ok=True)

        fixed_names = []

        for package_name, package_path in self.packages.items():

            if package_name == self.package_name:
                continue

            print("Saving package " + package_name)

            fixed_name = package_name.replace('#', '_').replace('/', '_')
            fixed_names.append(fixed_name)
            shutil.copytree(package_path, os.path.join(self.base_path, fixed_name))

        package_data = self.package_data

        if package_data is None:
            package_data = {'name': self.package_name}

        subpackages = package_data.setdefault('subpackages', [])

        for fixed_name in fixed_names:
            for subpackage in subpackages:
                if subpackage['pattern'] == fixed_name:
                    break
            else:
                subpackages.append({'pattern': fixed_name})

        with open(self.package_file, 'w') as fp:
            json.dump(package_data, fp, indent=True)

    def add_data_tool(self, data_tool: DataTool):
        self.docker_images.add(data_tool.data.get('image'))
        self.packages[data_tool.package.name] = data_tool.package.base_path

    def add_data(self, data: Data):
        self.packages[data.dataset_name] = data.challenge.package.context.get_artifact_path(
            data.dataset_name, data.challenge.abs_base_path)

    def add_evaluation_tool(self, eval_tool: EvaluationTool):
        self.docker_images.add(eval_tool.data.get('image'))
        self.packages[eval_tool.package.name] = eval_tool.package.base_path

    def add_challenge(self, challenge: Challenge):
        self.packages[challenge.package.name] = challenge.package.base_path
        self.add_challenge_deps(challenge)

    def add_platform_sources_deps(self, platform_sources: PlatformSources):
        for image in platform_sources.docker_images:
            self.docker_images.add(image)

    def add_challenge_deps(self, challenge: Challenge):
        for evaluation_procedure in challenge.evaluation_procedures.values():
            if evaluation_procedure.evaluation_tool_name is not None:
                self.add_evaluation_tool(evaluation_procedure.evaluation_tool)

            for data in evaluation_procedure.evaluation_data.values():
                if data.data_tool is None:
                    self.add_data(data)
                else:
                    self.add_data_tool(data.data_tool)

        if challenge.sample_data is not None:
            if challenge.sample_data.data_tool_name is not None:
                self.add_data_tool(challenge.sample_data.data_tool)

        if challenge.training_data is not None:
            if challenge.training_data.data_tool_name is not None:
                self.add_data_tool(challenge.training_data.data_tool)

    def add_runtime(self, runtime: Runtime):
        self.packages[runtime.package.name] = runtime.package.base_path

    def add_ai_app_deps(self, ai_app: AiApp):
        self.add_challenge(ai_app.challenge)
        self.add_runtime(ai_app.runtime)


