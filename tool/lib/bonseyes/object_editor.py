import collections
import json
import os
from json.decoder import JSONDecodeError
from typing import Dict, List, Optional, Union, MutableMapping, MutableSequence, Tuple

import sys

import jsonpointer
import jsonschema
import ruamel.yaml
from jsonschema.exceptions import ValidationError
from ruamel.yaml.comments import CommentedMap

from bonseyes.context import Context
from bonseyes.ui import print_title, ask_boolean, update_option, input_with_context


class JsonEditor:

    SCHEMA_PATH = None

    def __init__(self, instance_parent: Union[MutableSequence, MutableMapping],
                 parent_attr: Union[int, str], schema_file_path: Optional[str]=None,
                 start_path: Optional[List[str]] = None,
                 context: Optional[Context] = None):

        yaml = ruamel.yaml.YAML()

        if schema_file_path is None:
            schema_file_path = self.SCHEMA_PATH

        with open(schema_file_path) as fp:
            self.schema = yaml.load(fp)

        self.instance_parent = instance_parent
        self.parent_attr = parent_attr
        self.schema_file_path = schema_file_path
        self.target_path = None
        self.context = context

        if start_path is not None:
            self.start_path = start_path
        else:
            self.start_path = []

        self.current_path = []

    @property
    def full_current_path(self):
        return self.start_path + self.current_path

    @property
    def instance(self):
        return self.instance_parent[self.parent_attr]

    def _should_ask_edit(self):
        if self.target_path is None:
            return True

        if len(self.full_current_path) <= len(self.target_path):
            return False

        return True

    def _should_skip(self):

        if self.target_path is None:
            return False

        if len(self.full_current_path) < len(self.target_path):

            if self.full_current_path != self.target_path[0:len(self.full_current_path)]:
                return True

        else:

            if self.full_current_path[0:len(self.target_path)] != self.target_path:
                return True

        return False

    def current_path_matches(self, path: List[str]):

        if len(path) != len(self.current_path):
            return False

        for current, pattern in zip(self.current_path, path):
            if pattern is None:
                continue

            if current != pattern:
                return False

        return True

    @property
    def current_context_string(self):
        return ".".join(["[root]"] + self.full_current_path)

    def get_schema(self, path: List[str]) -> Tuple[Dict, str]:

        if len(path) == 0:
            return self.schema, self.schema_file_path

        parent_schema, parent_base_path = self.get_schema(path[:-1])

        if parent_schema['type'] == 'object':

            if parent_schema.get('additionalProperties'):
                schema = parent_schema['additionalProperties']

                if schema is True:
                    schema = {'type': 'any'}
            else:
                schema = parent_schema['properties'][path[-1]]
        elif parent_schema['type'] == 'array':
            schema = parent_schema['items']
        else:
            raise Exception("Unsupported parent schema" + str(parent_schema))

        if schema.get('$ref') is not None:
            yaml = ruamel.yaml.YAML()

            schema_path = os.path.join(os.path.dirname(schema['$ref']), schema['$ref'])

            with open(schema_path) as fp:
                schema = yaml.load(fp)

            return schema, schema_path

        return schema, parent_base_path

    def edit_instance(self, target_path: List[str] = None) -> None:
        self.target_path = target_path

        print_title(self.schema.get('title', os.path.basename(self.schema_file_path)))

        if 'description' in self.schema:
            print(self.schema.get('description', ''))
            print()

        self.edit_generic(self.schema, self.instance_parent, self.parent_attr)

    def edit_generic(self, schema: Dict,
                     parent: Union[MutableMapping, MutableSequence], attr_name: Union[int, str]) -> None:

        if '$ref' in schema:
            self.edit_ref(schema, parent, attr_name)
        elif 'type' not in schema and 'anyOf' in schema:
            self.edit_anyof(schema, parent, attr_name)
        elif schema['type'] == 'object':
            self.edit_object(schema, parent, attr_name)
        elif schema['type'] == 'array':
            self.edit_array(schema, parent, attr_name)
        elif schema['type'] == 'number':
            self.edit_number(schema, parent, attr_name)
        elif schema['type'] == 'string':
            self.edit_string(schema, parent, attr_name)
        elif schema['type'] == 'boolean':
            self.edit_boolean(schema, parent, attr_name)
        else:
            raise Exception("Unsupported schema: " + str(schema))

    def edit_anyof(self, schema: Dict,
                   parent: Union[MutableMapping, MutableSequence], attr_name: Union[int, str]) -> None:

        current_schema = None

        for idx, entry in enumerate(schema['anyOf']):
            try:
                jsonschema.validate(parent[attr_name], entry)
                current_schema = entry
            except ValidationError:
                continue

        if current_schema is None:
            print("Multiple options are available:")
            print()
            for idx, entry in enumerate(schema['anyOf']):
                print(str(idx) + ": " + entry.get('description'))
            print()

            entry_name = input_with_context("Please select", context=self.current_context_string)

            try:
                entry_name = int(entry_name)
                current_schema = schema['anyOf'][entry_name]
            except ValueError:
                print("Invalid choice")
            except IndexError:
                print("Invalid choice")

        self.edit_generic(current_schema, parent, attr_name)

    def edit_object(self, schema: Dict,
                    parent: Union[MutableMapping, MutableSequence], attr_name: Union[int, str]) -> None:

        if schema['type'] != 'object':
            raise Exception("Unsupported schema")

        if schema.get('additionalProperties', True):
            return self.edit_map_object(schema, parent, attr_name)
        else:
            return self.edit_fixed_object(schema, parent, attr_name)

    def has_optional_properties(self, schema, parent, attr_name):
        for property_name, prop in schema.get('properties', {}).items():
            optional_property = property_name not in schema.get('required', [])
            property_present = property_name in parent[attr_name]

            if not optional_property or property_present:
                continue

            return True

        return False

    def edit_object_property(self, property_name, prop, schema, parent, attr_name,
                             only_non_required):
        try:

            self._push_path(property_name)

            optional_property = property_name not in schema.get('required', [])
            property_present = property_name in parent[attr_name]

            if only_non_required:
                if not optional_property or property_present:
                    return
            else:
                if optional_property and not property_present:
                    return

            if self._should_skip():
                return

            print_title(
                prop.get('title', property_name) + ' (' + ".".join(self.full_current_path) + ')')

            if 'description' in prop:
                print(prop.get('description', ''))
                print()

            if optional_property:

                default = property_present

                if self._should_ask_edit():

                    add_property = ask_boolean("This is optional, do you want to specify it?",
                                               default, context=self.current_context_string)

                    if not add_property:
                        if property_name in parent[attr_name]:
                            del parent[attr_name][property_name]
                        return

            if property_name not in parent[attr_name]:
                parent[attr_name][property_name] = None

            self.edit_generic(prop, parent[attr_name], property_name)

        finally:

            self._pop_path()

    def edit_fixed_object(self, schema: Dict,
                          parent: Union[MutableMapping, MutableSequence],
                          attr_name: Union[int, str]) -> None:

        if schema['type'] != 'object':
            raise Exception("Unsupported schema")

        if not isinstance(parent[attr_name], collections.Mapping) or parent[attr_name] is None:
            parent[attr_name] = CommentedMap()

        if schema.get('additionalProperties', True):
            raise Exception('Unsupported type')

        # edit all required properties
        for property_name, prop in schema.get('properties', {}).items():
            self.edit_object_property(property_name, prop, schema, parent, attr_name, False)

        if not self.has_optional_properties(schema, parent, attr_name):
            return

        print()
        if not ask_boolean("There are some optional properties. Do you want to edit them?",
                           default=False, context=self.current_context_string):
            return

        # edit all non-required properties
        for property_name, prop in schema.get('properties', {}).items():
            self.edit_object_property(property_name, prop, schema, parent, attr_name, True)

    def _push_path(self, path_component):
        self.current_path.append(path_component)

    def _pop_path(self):
        self.current_path.pop()

    def edit_map_object(self, schema: Dict,
                        parent: Union[MutableMapping, MutableSequence],
                        attr_name: Union[int, str]) -> None:

        if schema['type'] != 'object':
            raise Exception("Unsupported schema")

        if not isinstance(parent[attr_name], collections.Mapping) or parent[attr_name] is None:
            parent[attr_name] = CommentedMap()

        if schema.get('additionalProperties', True) is True:

            if parent[attr_name] is not None:
                print("Current value is:")
                print()
                print(json.dumps(parent[attr_name], indent=True))
                print()

                if not ask_boolean("Do you want to change it?", context=self.current_context_string):
                    return

            while True:
                try:
                    data = input_with_context("Please enter the data in json format",
                                              context=self.current_context_string)

                    parent[attr_name] = json.loads(data)

                    return
                except JSONDecodeError:
                    print("Invalid json")
        else:
            self.edit_map_object_with_items(schema, parent, attr_name)

    def edit_map_object_with_items(self, schema: Dict,
                        parent: Union[MutableMapping, MutableSequence],
                        attr_name: Union[int, str]) -> None:

        if not isinstance(schema['additionalProperties'], collections.Mapping):
            raise Exception("Unsupported schema")

        if not isinstance(parent[attr_name], collections.Mapping) or parent[attr_name] is None:
            parent[attr_name] = {}

        if self.target_path is not None:
            if len(self.target_path) > len(self.full_current_path):
                try:
                    entry_name = int(self.target_path[len(self.full_current_path)])

                    print("Editing entry %s" % entry_name)

                    self._push_path(entry_name)

                    self.edit_generic(schema['additionalProperties'], parent[attr_name], entry_name)

                    self._pop_path()

                    return

                except KeyError:
                    raise Exception("Cannot find " + ".".join(
                        self.target_path[0:len(self.full_current_path) + 1]))

        while True:

            if len(parent[attr_name]) > 0:
                print("Current contents:")
                print()
                for name, entry in parent[attr_name].items():
                    print("name: " + name)
                    print(self.display_entry(entry))
                    print()

            ans = input_with_context("What you want to do " +
                                     "([e]dit entry/[r]emove entry/" +
                                     "[a]dd entry/[c]ontinue)",
                                     context=self.current_context_string)

            if ans not in ['e', 'r', 'a', 'c']:
                print("Invalid command")
                continue

            if ans == 'e':

                while True:

                    if len(parent[attr_name]) == 0:
                        print("No entries available")
                        break

                    try:
                        entry_name = input_with_context("Enter the name of the entry to edit",
                                                        context=self.current_context_string)

                        self._push_path(entry_name)

                        self.edit_generic(schema['additionalProperties'],
                                          parent[attr_name],
                                          entry_name)

                        self._pop_path()

                        break

                    except ValueError:
                        print("Invalid entry id")
                    except KeyError:
                        print("Entry doesn't exist")

            elif ans == 'r':

                while True:

                    if len(parent[attr_name]) == 0:
                        print("No entries available")
                        break

                    try:
                        entry_name = input_with_context("Enter the name of the entry to delete",
                                                        context=self.current_context_string)

                        del parent[attr_name][entry_name]

                        break

                    except ValueError:
                        print("Invalid entry id")
                    except KeyError:
                        print("Entry doesn't exist")

            elif ans == 'a':

                self._push_path(str(len(parent[attr_name])))

                entry_name = input_with_context("Enter the name of the entry to add",
                                                context=self.current_context_string)

                parent[attr_name][entry_name] = None
                self.edit_generic(schema['additionalProperties'], parent[attr_name], entry_name)
                self._pop_path()

            elif ans == 'c':
                break

            print()

    def edit_ref(self, schema: Dict, parent: Union[MutableMapping, MutableSequence],
                 attr_name: Union[int, str]) -> None:
        ref = schema['$ref']

        if ref.startswith('#'):
            fragment = jsonpointer.resolve_pointer(self.schema, ref[1:])

            self.edit_generic(fragment, parent, attr_name)
        else:
            editor = JsonEditor.create(parent, attr_name, self.full_current_path,
                                       os.path.join(os.path.dirname(self.schema_file_path), ref),
                                       self.context)

            editor.edit_instance(self.target_path)

    def display_entry(self, instance):
        return json.dumps(instance)

    def edit_array(self, schema: Dict, parent: Union[MutableMapping, MutableSequence],
                   attr_name: Union[int, str]) -> None:

        if schema['type'] != 'array':
            raise Exception("Unsupported schema")

        if 'items' not in schema:
            raise Exception("Array without items unsupported")

        if not isinstance(parent[attr_name], collections.Sequence) or parent[attr_name] is None:
            parent[attr_name] = []

        if self.target_path is not None:
            if len(self.target_path) > len(self.full_current_path):
                try:
                    entry_name = int(self.target_path[len(self.full_current_path)])

                    print("Editing entry %d" % entry_name)

                    self._push_path(str(entry_name))

                    self.edit_generic(schema['items'], parent[attr_name], entry_name)

                    self._pop_path()

                    return

                except IndexError:
                    raise Exception("Cannot find " + ".".join(
                        self.target_path[0:len(self.full_current_path) + 1]))

        while True:

            if len(parent[attr_name]) > 0:
                print("Current contents:")
                print()
                for idx, entry in enumerate(parent[attr_name]):
                    print("idx: " + str(idx))
                    print(self.display_entry(entry))
                    print()

            ans = input_with_context("What you want to do " +
                                     "([e]dit entry/[r]emove entry/" +
                                     "[a]dd entry/[m]ove entry/[c]ontinue)",
                                     context=self.current_context_string)

            if ans not in ['e', 'r', 'a', 'c', 'm']:
                print("Invalid command")
                continue

            if ans == 'e':

                while True:

                    if len(parent[attr_name]) == 0:
                        print("No entries available")
                        break

                    try:
                        entry_name = input_with_context("Enter the ID of the entry to edit",
                                                        context=self.current_context_string)

                        self._push_path(str(entry_name))

                        self.edit_generic(schema['items'], parent[attr_name], int(entry_name))

                        self._pop_path()

                        break

                    except ValueError:
                        print("Invalid entry id")
                    except IndexError:
                        print("Entry doesn't exist")

            elif ans == 'r':

                while True:

                    if len(parent[attr_name]) == 0:
                        print("No entries available")
                        break

                    try:
                        entry_name = input_with_context("Enter the ID of the entry to delete",
                                                        context=self.current_context_string)

                        del parent[attr_name][int(entry_name)]

                        break

                    except ValueError:
                        print("Invalid entry id")
                    except IndexError:
                        print("Entry doesn't exist")

            elif ans == 'a':

                self._push_path(str(len(parent[attr_name])))
                parent[attr_name].append(None)
                self.edit_generic(schema['items'], parent[attr_name], len(parent[attr_name]) - 1)
                self._pop_path()

            elif ans == 'm':

                while True:

                    if len(parent[attr_name]) == 0:
                        print("No entries available")
                        break

                    try:
                        src_entry_name = input_with_context("Enter the ID of the entry to move",
                                                            context=self.current_context_string)

                        src_entry_name = int(src_entry_name)

                        if src_entry_name < 0 or src_entry_name >= len(parent[attr_name]):
                            print("Invalid entry index")

                    except ValueError:
                        print("Invalid entry id")
                        continue

                    try:
                        dst_entry_name = input_with_context(
                            "Enter position where to move the entry",
                            context=self.current_context_string)

                        dst_entry_name = int(dst_entry_name)

                        if dst_entry_name < 0 or dst_entry_name >= len(parent[attr_name]):
                            print("Invalid entry position")

                    except ValueError:
                        print("Invalid entry id")
                        continue

                    entry = parent[attr_name][src_entry_name]
                    del parent[attr_name][src_entry_name]
                    parent[attr_name].insert(dst_entry_name, entry)

                    break

            elif ans == 'c':
                break

            print()

    def edit_number(self, schema: Dict, parent: Union[MutableMapping, MutableSequence],
                    attr_name: Union[int, str]) -> None:

        if parent[attr_name] is not None:
            print()
            print("Current value:" + str(parent[attr_name]))
            print()

            if not ask_boolean("Do you want to change it?", context=self.current_context_string):
                return

        print("Enter the value and press enter to confirm:")

        while True:
            val = input_with_context("", context=self.current_context_string)

            try:
                parent[attr_name] = float(val)
            except ValueError:
                print("Invalid number")

    def get_enums(self, schema) -> Optional[Dict[str, str]]:
        if 'enum' in schema:
            return collections.OrderedDict({x: x for x in schema['enum']})
        elif 'oneOf' in schema:
            options = collections.OrderedDict()
            for entry in schema['oneOf']:
                if 'enum' in entry:
                    for enum_val in entry['enum']:
                        options[enum_val] = entry.get('title', enum_val)

            return options
        else:
            return None

    def edit_string(self, schema: Dict, parent: Union[MutableMapping, MutableSequence],
                    attr_name: Union[int, str]) -> None:

        enum_options = self.get_enums(schema)

        if enum_options is not None:
            print("Available options:")
            print()

            for option_val, option_desc in enum_options.items():
                print(" - " + option_val + " (" + option_desc + ")")

            print()

            if parent[attr_name] in enum_options.keys():
                default_value = parent[attr_name]
            else:
                default_value = list(enum_options.keys())[0]

            while True:
                option = input_with_context("Please select an option (" + default_value + ")",
                                            context=self.current_context_string)

                if option == '':
                    option = default_value

                if option not in enum_options.keys():
                    print("Invalid option")
                    continue

                parent[attr_name] = option

                return

        else:

            if parent[attr_name] is not None:
                print()
                print("Current text:")
                print()
                print(parent[attr_name])
                print()

                if not ask_boolean("Do you want to change it?", context=self.current_context_string):
                    return

            print("Enter the text and press enter to confirm:")

            parent[attr_name] = input_with_context("", context=self.current_context_string)

    def edit_boolean(self, schema: Dict, parent: Union[MutableMapping, MutableSequence],
                     attr_name: Union[int, str]) -> None:

        if parent[attr_name] is not None:
            print()
            print("Current value:" + str(parent[attr_name]))
            print()

            if not ask_boolean("Do you want to change it?", context=self.current_context_string):
                return

        parent[attr_name] = ask_boolean("Do want to enable this?", parent[attr_name],
                                        context=self.current_context_string)

    def edit_artifact_name(self, schema, parent, attr_name, artifact_type):

        if attr_name in parent and parent[attr_name] is not None:

            print("The current value is " + str(parent[attr_name]))

            if not ask_boolean("Do you want to change it?", True,
                               context=self.current_context_string):
                return

        if ask_boolean("Do you want to pick an artifact available in the context?", True,
                       context=self.current_context_string):

            options = {}

            for artifact_entry in self.context.artifacts:

                if artifact_entry.artifact_type != artifact_type.ARTIFACT_CLASS_NAME:
                    continue

                artifact = artifact_type.load(artifact_entry.artifact_name, self.context)

                artifact_metadata = artifact.metadata or {}

                options[artifact.artifact_name] = artifact_metadata.get('title')

            if len(options) != 0:
                selected_artifact = update_option(None, options,
                                                  context=self.current_context_string)
                parent[attr_name] = selected_artifact
                return

            print()
            print("No artifact of type %s found in the context." % artifact_type.ARTIFACT_CLASS_NAME)
            print()
            print("You need to enter the artifact name manually.")
            print()

        while True:

            print()
            print("Select the package from the packages available in the context:")
            print()

            package_name = update_option(None, {package.name: package.base_path
                                                for package in self.context.packages},
                                         context=self.current_context_string)

            print()

            artifact_path = input_with_context("Enter the artifact name (default . )",
                                               context=self.current_context_string)

            if artifact_path == '':
                artifact_path = '.'

            artifact_name = package_name + '#' + artifact_path

            try:
                artifact_type.load(artifact_name, self.context)

                parent[attr_name] = artifact_name

                break
            except Exception as e:
                print("Failed to load artifact: " + str(e))

    def edit_external_schema(self, schema_path, parent, attr_name):
        editor = self.create(schema_path=os.path.abspath(schema_path),
                             instance_parent=parent,
                             parent_attr=attr_name,
                             start_path=self.full_current_path)

        editor.edit_instance(self.target_path)

    def edit_custom_editor(self, custom_editor, parent, attr_name):
        editor = custom_editor.create(instance_parent=parent, parent_attr=attr_name,
                                      context=self.context, start_path=self.full_current_path)

        editor.edit_instance(self.target_path)

    @classmethod
    def create(cls, instance_parent: Union[MutableSequence, MutableMapping],
               parent_attr: Union[int, str], start_path: Optional[List[str]] = None,
               schema_path: Optional[str] = None,
               context: Optional[Context] = None) -> 'JsonEditor':

        return cls(instance_parent=instance_parent, parent_attr=parent_attr,
                   schema_file_path=schema_path, start_path=start_path, context=context)


def main():
    yaml = ruamel.yaml.YAML()

    with open(sys.argv[1]) as fp:
        instance_obj = yaml.load(fp)

    JsonEditor.from_file(instance_obj, sys.argv[2])


if __name__ == "__main__":
    main()
