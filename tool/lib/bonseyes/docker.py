import os
import subprocess
from subprocess import CalledProcessError
from typing import List, Dict, Optional, NamedTuple, Tuple

import sys

VolumeEntry = NamedTuple('VolumeEntry', [('mount_point', str), ('volume', 'Volume'), ('mode', str)])


class Volume:
    def __init__(self, path: str):
        self.path = path

    @property
    def abs_path(self):
        return os.path.abspath(self.path)

    @staticmethod
    def create(path: str):
        os.makedirs(path, exist_ok=True)
        return Volume(path)

    @staticmethod
    def load_from_path(path: str):
        if not os.path.isdir(path):
            raise Exception("Invalid path " + path)
        return Volume(path)


class Container:
    def __init__(self, cid: str):
        self.cid = cid

    @staticmethod
    def run(image_name: str, command: List[str],
            environment: Optional[Dict[str, str]] = None,
            volumes: Optional[List[VolumeEntry]] = None,
            expose_ports: Optional[List[int]] = None,
            network: str = None,
            detach: bool = False,
            auto_remove: bool = True) -> Optional[str]:

        extra_params = ['--privileged']

        if auto_remove:
            extra_params += ['--rm']

        # setup special networking if requested
        if network is not None:
            extra_params += ['--network', network]

        # start in background the container
        if detach:
            extra_params += ['-d']

        # check if we are running in a pseudoterminal and if that is the case attach it to docker
        if sys.stdout.isatty():
            extra_params += ['-ti']

            # set uid and gid in the docker to match the ones used by the current user
            extra_params.extend(['-u', str(os.getuid()) + ':' + str(os.getgid())])

        # add all the environment variables
        if environment is not None:
            for env_name, env_val in environment.items():
                extra_params += ['-e', env_name + '=' + env_val]

        # add all the volumes variables
        if volumes is not None:
            for entry in volumes:
                extra_params += ['-v', entry.volume.abs_path + ':' + entry.mount_point + ':' + entry.mode]

        # add all port mappings
        if expose_ports is not None:
            for port in expose_ports:
                extra_params += ['-p', str(port)]

        try:
            cid = subprocess.check_output(['docker', 'run'] + extra_params + [image_name] + command).decode().strip()
        except subprocess.CalledProcessError as e:
            print(e.output)

            raise e

        if detach:
            return cid

    def get_address(self, port: int) -> Tuple[str, int]:

        try:
            ext_port = subprocess.check_output(['docker', 'inspect',
                                            '--type=container',
                                            '-f', '{{(index (index .NetworkSettings.Ports "' +
                                            str(port) + '/tcp") 0).HostPort}}', self.cid])

            ip = subprocess.check_output(['docker', 'inspect',
                                          '--type=container',
                                          '-f', '{{(index (index .NetworkSettings.Ports "' +
                                          str(port) + '/tcp") 0).HostIp}}', self.cid])

            ip = ip.decode().strip()

            # fix the ip if we are talking to a remote docker host
            if ip == "0.0.0.0" and "DOCKER_HOST" in os.environ:
                ip = os.environ["DOCKER_HOST"].split("://")[1].split(":")[0]

            # override the docker IP if the variable is set
            if "BE_DOCKER_HOST_IP" in os.environ:
                ip = os.environ["BE_DOCKER_HOST_IP"]

            ext_port = int(ext_port.decode().strip())

            return ip, ext_port

        except CalledProcessError:
            raise Exception("Cannot find mapping of port " + str(port) + " of container " + self.cid)

    @staticmethod
    def create(image_name: str, command: List[str],
               environment: Optional[Dict[str, str]] = None,
               volumes: Optional[List[VolumeEntry]] = None,
               expose_ports: Optional[List[int]] = None,
               network: str = None):
        return Container(Container.run(image_name, command, environment, volumes, expose_ports, network,
                                       detach=True, auto_remove=False))

    @staticmethod
    def load_from_ref(file_name: str):
        with open(file_name) as fp:
            return Container(fp.read().strip())

    def save_to_ref(self, file_name: str):
        with open(file_name, 'w') as fp:
            fp.write(self.cid)

    def print_logs(self):
        subprocess.check_call(['docker', 'logs', self.cid])

    def stop(self):
        try:
            with open(os.devnull, 'w') as fp:
                subprocess.check_call(['docker', 'stop', self.cid], stdout=fp, stderr=fp)
        except CalledProcessError:
            pass

    def remove(self):
        try:
            with open(os.devnull, 'w') as fp:
                subprocess.check_call(['docker', 'remove', self.cid], stdout=fp, stderr=fp)
        except CalledProcessError:
            pass

    def is_running(self):
        with open(os.devnull, 'w') as fp:
            try:
                ret = subprocess.check_output(['docker', 'inspect', '--type=container',
                                               '-f', '{{.State.Running}}', self.cid],
                                              stderr=fp).decode().strip()
                return ret == 'true'
            except CalledProcessError:
                return False
