#!/bin/bash

set -e

set -x

BASE_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

docker build -t registry.gitlab.com/bonseyes/bonseyes-cli:${1:-latest} ${BASE_PATH}
