======================
Bonseyes Documentation
======================

BonsAPPs is a service layer which is continuation of the EU-funded Bonseyes project, and offers a series of modular services — such as experimentation, model compression, optimisation, benchmarking,
and deployment on hardware and security that will increase AI usage among enterprises and SMEs, which currently lack internal innovation capabilities.

This document gathers all the documentation associated to the workflows of Bonseyes/BonsApps.

.. toctree::
   :maxdepth: 2
   :caption: Setup

   pages/setup

.. toctree::
   :maxdepth: 2
   :caption: User Guides

   pages/user_guides

.. toctree::
   :maxdepth: 2
   :caption: Developer Guides

   pages/developer_guides
