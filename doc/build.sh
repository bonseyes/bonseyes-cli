#!/bin/bash

DOC_PATH=$(readlink -f "$( dirname "${BASH_SOURCE[0]}")")

rm -rf ${DOC_PATH}/_build

docker run -v ${DOC_PATH}/../:/data \
           -w /data -w /data/doc -u $(id -u) --group-add $(id -g) \
           registry.gitlab.com/bonseyes/util/doc-builder:latest \
            sphinx-build . _build/html -v --color
