AI App Artifacts
================

.. jsonschema:: ../schemas/core/ai_app.yml

Results of the evaluation of an AI App are stored in evaluation reports:

.. jsonschema:: ../schemas/core/evaluation_report.yml