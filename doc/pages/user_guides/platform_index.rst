***********
Description
***********

.. _DPE Description:

A Developer Platform is a digital package containing the full software stack (operating system, drivers, middleware components, etc.) and documentation required to procure, set up and control target hardware for the execution of AI apps. Moreover, the platform provides a cross-compilation environment and tooling that is capable of creating executables for the target hardware on the developer workstation.

A developer platform are composed of various components:

- **Support docker**: the container used to build the platform package and setup target hardware.
- **Builder docker**: the container used to cross-compile binaries for the target hardware.
- **Manager docker**: the container used to control the target hardware.
- **Clean metadata**: the full metadata that is used for the listing on the marketplace.

Currently, the following platforms are supported:

- RaspberryPi 4b Ubuntu20

  - ${PlatformName}: raspberry4b_64-ubuntu20
  - Size of platform's sources: 5.43 GB
  - Estimated workflow time (excluding VM installation): 1h15

- Nvidia Jetson Nano JP4.4.1

  - ${PlatformName}: jetson_nano-jetpack4.4
  - Size of platform's sources: 3.50 GB
  - Estimated workflow time (excluding VM installation): 3-4h

- Nvidia Jetson Nano JP4.6.1

  - ${PlatformName}: jetson_nano-jetpack4.6
  - Size of platform's sources: 2.47 GB
  - Estimated workflow time (excluding VM installation): 3-4h

- Nvidia Jetson Xavier JP4.6.1

  - ${PlatformName}: jetson_xavier-jetpack4.6
  - Size of platform's sources: 3.75 GB
  - Estimated workflow time (excluding VM installation): 3-4h

A Bonseyes Developer Environment (DPE) is the environment, e.g., tools and workflows, to help the user employ a developer platform. A DPE has the objective of walking the user through the steps of building a Developer Platform, setting up the embedded hardware and test a sample application on it to make sure of the correct operation of the system.

A DPE employs three elements:

- **Bonseyes CLI tool**: Bonseyes Command Line Interface to execute worflows.
- **Board Support Package (BSP)**: Package containing docker images, drivers, toolchains and other sources of the target embedded platform.
- **Sample application**: “Hello_world” application to test the correct operation of the of the DPE workflow. 

BSPs are available and can be obtained in the Bonseyes Marketplace under Developer Platforms.

The objective of this document is to guide you through the process of using a DPE to flash and set up a board.

***************
Workflows usage
***************

Two different DPE workflows may be used based on the type of device that is the BSP is linked to. The following sections present the most widespread workflow first: the *Linux-based workflow*. This workflow is the one used when the board runs a Linux operating system. At the end of this document, we present the *MCU (Microcontroller Unit) workflow*. This second workflow shares the same principles as the Linux-based workflow, but the DPE's internal behaviour greatly differs due to a lack of operating system. 

************
Buy platform
************

DPEs can be bought or obtained through the BMP:

.. image:: ../assets/BMP_platform.png
  :alt: Image of the BMP

Once the platform has been bought, you will see it in your dashboard.

Likewise, buy the hello-world example, that is available in the AI Apps' tab under the *Marketplace*'s vertical, to test the workflow. 

**Note**: The hello-world app can be used for any platform regardless of the target platform description on the BMP.

.. _setupPlatform:

**************
Setup platform
**************

This guide assumes you already have the done the following:

- Setup the local environment as explained in :doc:`/pages/setup`
- Bought a platform in the BMP
- Bought the hello-world app in the BMP

To start the workflow, first create a new directory. Then, change directory to the new folder:

.. code-block:: bash

    mkdir my-bonseyes-platform
    cd my-bonseyes-platform


Download DPE from BMP
~~~~~~~~~~~~~~~~~~~~~
Next, donwload the DPE from the BMP by executing the following command (replace ${PlatformName} by the platform name (e.g: raspberry4b_64-ubuntu20):

.. code-block:: bash

    bonseyes marketplace download-platform --output-directory ${PlatformName}_sources

A dialog will prompts asking you to choose the platform to download:

.. image:: ../assets/platform_download.png
  :alt: Download platform from BMP

The platform will be downloaded and the platform's docker images, that encasulate some of the target platform environment and tools, will be directly loaded into your system.


Next, download the hello-world example from the BMP:

.. code-block:: bash

    bonseyes marketplace download-ai-app --output-dir hello_world

The hello_world application will be donwloaded. 

**Note**: For this specific AI App, we will be only looking at the source code inside hello_world/hello_world.


Set up a Bonseyes DPE
~~~~~~~~~~~~~~~~~~~~~

Build platform
--------------

Next, build the platform sources by executing the following command:

.. code-block:: bash

    bonseyes platform build --platform-sources ${PlatformName}_sources ${PlatformName}_build

The command will build the platform and store it in the ${PlatformName}_build directory

For some platforms, a web based wizard goes through the necessary steps to build and setup the platform. If it appears in stdout, click on the url link (http://127.0.0.1:5000) and follow the steps. The web wizard is shown in the following image:

.. image:: ../assets/wizard.png
  :height: 300 px
  :alt: Image of the web wizard


Flash target platform
---------------------

To deploy on some platforms, you may need to flash a SD card. If you used a VirtualBox VM and have an integrated SD card reader in your host machine, you may need to perform the following steps to make it detected by the VM.
WARNING: if you specify the wrong disk for the VM, you may end up formatting your host system! If you have a USB SD card reader it is suggested to plug it in, as it should be recognized by Virtual Box as a USB device instead.

You can find more information about using an integrated SD card in `this stack exchange post <https://superuser.com/a/458085/>`__, the important parts of it are summarized down below.

In short, a virtual disk is created which points directly to the raw SD card file. This disk is then added to the VM.

    • Insert the SD card
    • In a terminal, type "mount" to identify the path of the SD card
    • Unmount the SD card if was mounted
    • Run the following command. Make sure to replace "/PATH/TO/SD_CARD" by the correct path to the SD card. "~/VirtualBox VMs/sdcard.vmdk" is the path where the virtual disk will be saved.

.. code-block:: bash

    VBoxManage internalcommands createrawvmdk -filename ~/VirtualBox VMs/sdcard.vmdk -rawdisk /PATH/TO/SD_CARD

    • Then, if the VM was running, turn it off
    • Open the VM settings > storage, select the controller and click "Add Hard Disk"
    • Select "Choose existing disk" and select to the path previously specified
    • Start the VM and check that the SD card can be accessed. Check that the disk representing the SD card has the correct memory size, or you may have picked the wrong device
    • If you picked the wrong device, turn off the VM and remove the disk from the storage. Using the wrong disk may cause data loss on your host device!
    • You may need to change the permissions of the SD card so that Virtual Box can access it
    • If you unplug the SD card, you will need to remove the virtual disk from Virtual Box to be able to boot the machine

Next, set up the target hardware and create a target config:

.. code-block:: bash

    bonseyes target setup --platform-build ${PlatformName}_build --target-config ${PlatformName}_config


A new folder with the platform configuration has been created on ${PlatformName}_config.

The target hardware is now configured and can be used.


*************
Test platform
*************

Build an application for target a platform
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To ease and unify the build and development process, Bonseyes platform implementation provides build managers with a cross compilation environment.

In order to build a particular application using the build manager, the following environment variables must be set:

    • PLATFORM_SOURCES_DIR - path to folder with platform sources downloaded using bonseyes tool (${PlatformName}_source from previous command)
    • BUILDED_PLATFORM_PATH - path to folder where platform is build using bonseyes tool (${PlatformName}_build from previous command),
    • APPLICATION_SOURCE_CODE_PATH - path to application folder with cmake configuration that should be built
    • BUILD_PATH - temporary build files path
    • INSTALLATION_PATH - path where installation binaries should be copied upon build


For example:

.. code-block:: bash

    export PLATFORM_SOURCES_DIR=<path_to_folder>/raspberry4b_64-ubuntu20_sources
    export BUILDED_PLATFORM_PATH=<path_to_folder>/raspberry4b_64-ubuntu20_build
    export APPLICATION_SOURCE_CODE_PATH=<path_to_folder>/hello_world/hello_world
    export INSTALLATION_PATH=<path_to_folder>/hello_world/hello_world/build
    export BUILD_PATH=/tmp/cache

Next, copy the toolchain of the platform into the hello_world folder to enable the cross-compilation:


.. code-block:: bash

    cp ${PlatformName}_sources/platform/toolchain.cmake hello_world/hello_world

Now, define a "run_in_builder" command:

.. code-block:: bash

    run_in_builder() { bonseyes --packages-dir ${PLATFORM_SOURCES_DIR} platform run-in-builder --platform-build ${BUILDED_PLATFORM_PATH} --sources-dir ${APPLICATION_SOURCE_CODE_PATH} --cache-dir ${BUILD_PATH} --output-dir ${INSTALLATION_PATH} $@; }


The hello_world application can be now cross-compiled by executing:

.. code-block:: bash

    run_in_builder -- bash -c ./cross-compile.sh

If you get an error that says "Package com_bonseyes/... not found" make sure you are in the "my-bonseyes-project" folder and that the environment variables are correct.

To run other custom commands in the builder use the same approach. For example, to list files in the root application folder where the build will be performed, run:

.. code-block:: bash

    run_in_builder -- bash -c ls -la

The hello_world example contains a simple c++ file and cmake file including the cross-compilation toolchain definition. The cross-compile.sh script is given to ease the execution process.
In general, to build some C++ project with cmake file for a target platform, copy the toolchain.cmake of the target platform to the root directory of the application and cross-compile as explained in this document with run_in_builder.


Execute the application on the target platform
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The target manager is a docker container that is able to communicate with the platform and performs various operations in the scope of Bonseyes platform functionalities.
When started, it creates a local http server and provides a web interface to communicate with the target platform.

.. code-block:: bash

    bonseyes target start-server --target-config ${PlatformName}_config

Click on the url link (http://0.0.0.0:XXXXX) to open the target manager container.
You can check out and play around with the API to control the board.

Close the browser tab and execute the following command to stop the server:

.. code-block:: bash

    bonseyes target stop-server --target-config ${PlatformName}_config

Command line interface also provides capabilities to interact with the platform. For instance, target platform's information can be found by executing the following command:

.. code-block:: bash

    bonseyes target manage --target-config ${PlatformName}_config system-info


To execute the hello_world application on the platform, execute the following commands on your developer station.

Copy the hello world binary on the target platform by executing:

.. code-block:: bash

    bonseyes target manage --target-config ${PlatformName}_config runtime-upload --runtime $ABSOLUT/PATH/TO/my-bonseyes-platform/hello_world/hello_world --name hello_world

``Note:`` a different command is used for copying real AI Apps.


Find target platform's IP address (if you forgot):

.. code-block:: bash

    bonseyes target manage --target-config ${PlatformName}_config system-address

Log into the target platform through SSH (replace username by the one given during setup and the ip address):

.. code-block:: bash

    ssh USERNAME@IP_ADDR

Change directory to the uploaded folder:

.. code-block:: bash

    cd platform/runtimes/hello_world/build

Finally, execute the hello_world binary:

.. code-block:: bash

    ./hello_world

************
MCU workflow
************

Differences
~~~~~~~~~~~

Here is a summary of the differences between the MCU and Linux workflow: 

- When using the MCU Workflow, the user needs to build and setup the platform for each new application to be executed on the target platform. This come from the fact that the MCU does not have an operating system that allows to simply build and execute an application on the platform, even after having set up the board.
- The previous point also entails that  the user needs to flash the platform after having built the application, since the application is included in the BSM that is transfered to the platform during the flash operation.
- The execution and monitoring of the application on the platform does not work in the same way as with the Linux MCU. Once the platform is flashed, the system is running in a loop and the user needs to use specific commands to fetch the benchmarks results from the MCU platform.

Usage
~~~~~

With those differences in mind, here is an example of a full execution of the MCU Workflow: 

- Setup Platform : The instructions are the same between MCU and Linux workflow for buying, creating the directory and building the platform: 

.. code-block:: bash

    bonseyes platform build --platform-sources ${PlatformName}_sources ${PlatformName}_build

In the MCU workflow, The platform build command does not only explain how to download all the tools needed, but also guide you through the code generation for the platform (the code that will be compiled later).

- Build Application : The builder is used to cross-compile the application. To do so, the user needs to copy the code that he/she wants to cross-compile into the "project" folder of the hello_world folder (If there is no "project" folder into the hello_world folder,do create it). The hello_world folder can be found on `This repository <https://gitlab.com/bonseyes/platforms/mcu-workflows/stm32h7a3/-/tree/dpe_uworkflow/platform/hello_world>`__. The code that the user wants to cross-compile needs to use a Makefile for compilation. Once done, follow the instructions on "Build an application for target a platform" to cross-compile the code. Finally, copy the result under your deps folder: 

.. code-block:: bash

    cp -r hello_world ${PlatformName}_build/deps/

- Flash Target: To flash the application on the board, use the same command that is used for the Linux Workflow and follow the wizard: 

.. code-block:: bash

    bonseyes target setup --platform-build ${PlatformName}_build --target-config ${PlatformName}_config

- Get results from the application: With the manager docker, the user can get benchmark the results from the application running on the platform. To do so, start the manager server first as follows: 

.. code-block:: bash

    bonseyes target start-server --target-config ${PlatformName}_config

Once the manager is launched, use the "get-mcu-benchmark" command to get the results: 

.. code-block:: bash

    bonseyes target manage --target-config ${PlatformName}_config get-mcu-benchmark

Finally, once you are done, stop the Manager Server as follows: 

.. code-block:: bash

    bonseyes target stop-server --target-config ${PlatformName}_config
