**************
Join challenge
**************


This guide assumes you already have the done the following:

- Setup the local environment as explained in :doc:`/pages/setup`

To obtain a challenge the following steps are required:

1. Join the challenge on the `marketplace <https://beta.bonseyes.com/marketplace/challenges>`_

2. Download the challenge::

    $ bonseyes marketplace download-challenge --output-dir challenge

The challenge will be downloaded in the directory ``challenge`` and the necessary docker images
will be loaded in the local docker engine.

