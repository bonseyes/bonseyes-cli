*******************
AI Artifact Package
*******************

.. toctree::
   :maxdepth: 4

   dev_guides/package_index


************
AI Challenge
************

.. toctree::
   :maxdepth: 4

   dev_guides/ai_challenge_index

********
AI Asset
********

.. toctree::
   :maxdepth: 4

   dev_guides/ai_asset_index

*********
Data Tool
*********

.. toctree::
   :maxdepth: 4

   dev_guides/data_tool_index

***************
Evaluation Tool
***************

.. toctree::
   :maxdepth: 4

   dev_guides/eval_tool_index

********
Platform
********

.. toctree::
   :maxdepth: 4

   dev_guides/platform_index


******
AI App
******

.. toctree::
   :maxdepth: 4

   dev_guides/ai_app_index


*****
LPDNN
*****

.. toctree::
   :maxdepth: 4

   dev_guides/lpdnn_index
