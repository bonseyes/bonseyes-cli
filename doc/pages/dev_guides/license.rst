**********
Definition
**********

This page describes the modifications made to add a minimal licensing system to AI application and protect the decryption code part, occurring at runtime, with KOP (Kudelski Obfuscation Process).

The KOP tool implements the main software hardening techniques to counter the main piracy threats on software: Reverse, Tampering, and Code lifting.
The documentation (as a zip archive containing pages in HTML format) can be found here: :download:`setup-runner.sh <../assets/KOP-UserManual-2.22.5.tar.gz>`.

The goal of the proof of concept is to encrypt the main IP assets using a simple cipher and decrypt them at runtime after verifying the associated license.

This has been done in the following steps:

* Add IP assets encryption mechanisms in the build process
* Add pre-license file generation in the build process
* Develop a standalone application to compute and add the signature to the license file
* Develop a lib to verify and decrypt the IP assets at runtime
* Obfuscate the license lib (validation/decryption) with the KOP tool

This has been tested using the lenet5 sample application from LPDNN SDK: this sample app performs image classification on the MNIST dataset using lenet5 architecture (using Lpdnn Network Engine (LNE)).

Supported platforms:

* x86_64_ubuntu20
* x86_64_ubuntu20_cuda
* raspberry4b_64-ubuntu20

Supported inference engines:

* LNE
* NCNN
* ONNXruntime
* TensorRT


***********************
AI-App license workflow
***********************

AI-App encryption
~~~~~~~~~~~~~~~~~

The AI-App encryption is done at build time on the host machine using a **chacha20 hardcoded key** (mitigated with the name of the AI-App).

The encryption process follows these steps:

* The hardcoded key is mitigated with the name of the AI-App
* IP assets associated with the current ai_app_generator are encrypted
* The IP asset is then installed on the target directory

For example, you can use the new ``--license`` parameter supported by the build-for-target.sh script: ::

  ./build-for-target.sh --platform x86_64-ubuntu20 --license

With this example, and in the case of lenet5 model (from image-classification/mnist), the final assets encrypted are:

* share/ai-apps/mnist-lenet5-default/ai_app_config.json
* share/ai-apps/mnist-lenet5-default/model.json
* share/ai-apps/mnist-lenet5-default/model.bin


AI-App pre-license file generation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The generation of the pre-license file is also done at build time on the host machine.

For example, with the new ``--license`` parameter supported by the build-for-target.sh script, that will produce the pre-license file: ::

  sdk/build/${PLATFORM}/install/share/ai-apps/${AI_APP_NAME}/ai_app_config.lic

with the following content: ::

  {
    "app_id" : "mnist-lenet5-default",
    "encrypted_files" : [
      "ai_app_config.json",
      "model.json",
      "model.bin",
    ]
  }

Supported platforms also provide HW context (*device_os* and *device_arch*) that is added to the license file: ::

  {
    "app_id" : "mnist-lenet5-default",
    "encrypted_files" : [
      "ai_app_config.json",
      "model.json",
      "model.bin",
    ],
    "device_os" : "Linux",
    "device_arch" : "x86_64 or aarch64"
  }

If the platform has not been updated to provide the HW context, the fields can be manually added in the ai_app_config.lic.

To update a HW platform to provide the necessary HW context for the KOP, please follow these examples for x86_64_ubuntu20_cuda:

* `Commit-1 <https://gitlab.com/bonseyes/platforms/bin/x86_64-ubuntu20_cuda/-/commit/e429a41f35f4bf66e2440d9969901c0772c970b7>`_.
* `Commit-2 <https://gitlab.com/bonseyes/platforms/bin/x86_64-ubuntu20_cuda/-/commit/487db806a12265383cd7bfb82342a472078c9916>`_.


The *device_os* and *device_arch* fields will be checked at runtime when the license validation is done by using the `unix uname function <https://linux.die.net/man/3/uname>`_. The check corresponds to performing the following commands in the target platforms: ::

  # Get the device OS
  uname -s
  # Get the device arch
  uname -p


License signing tool
~~~~~~~~~~~~~~~~~~~~

Then, the file is passed to the license generator tool: ::

  ./gencert -i not-signed-license.json -o license.json

the output file will contain:  ::

  {
    "app_id": "mnist-lenet5-default",
    "encrypted_files": [
      "ai_app_config.json",
      "model.bin",
      "model.json"
    ],
    "device_arch": "aarch64",
    "device_os": "Linux",
    "license_signature": "304602210...6ac8ae8f9229"
  }

All these fields are parsed by this tool and will be used to produce the license signature using the **hardcoded ECDSA private key**.


License check at runtime
~~~~~~~~~~~~~~~~~~~~~~~~

To test the lenet5 sample application with a license, execute the following commands:  ::

  cd ./build/x86_64-ubuntu20/install/bin/
  source set-lib-path.sh
  ./aiapp-cli --license license.json --cfg ../share/ai-apps/mnist-lenet5-default/ai_app_config.json -f /PATH/TO/catalog/image-classification/mnist/challenge/samples/image_100.png

with license.json being the license generator's file containing the signature.

The AI-App license check is made at runtime and follow these steps:

* The application instantiates a LicenciedFile_context for the current AI-App.
* In the constructor of this class, the license file is read, and its validity is checked with the **hardcoded ECDSA public key**.


AI-App decryption
~~~~~~~~~~~~~~~~~

The AI-App decryption is made at runtime when a resource is read by the LNE engine using its Context (of type LicenciedFile_context).
The **chacha20 hardcoded key** (mitigated with the name of the AI-App) is used to decrypt the content.


************
KOP in LPDNN
************

Support in LPDNN platforms
~~~~~~~~~~~~~~~~~~~~~~~~~~

Certain libraries need to be added to LPDNN platforms to support the compilation and encryption process (python). The following dependencies have been added to the builder base image (Ubuntu Focal) as in this `example <https://gitlab.com/bonseyes/platforms/builders/ubuntu_focal/-/commit/695f9c4b43a54473182f0a71043510a00b5d7010>`_:

- cffi==1.14.6
- cryptography==2.8
- six==1.15.0
- pycparser==2.20

In addition, KOP library (KOP-2.21.14-full_on_ubu64_1804.tar.gz), the license (bonseyes.lic) and specific python2 packages also need to be added on the target LPDNN platforms:

- `x86_64_ubuntu20 <https://gitlab.com/bonseyes/platforms/builders/x86_64-ubuntu20/-/commit/c5f105b43bbf01fa39cf0330065bc2d655ae43f3>`_ with small `update <https://gitlab.com/bonseyes/platforms/builders/x86_64-ubuntu20/-/commit/4b00768fbf807b142c7ce0a477fb4b235219588c>`_.
- `raspberry4b_64-ubuntu20 <https://gitlab.com/bonseyes/platforms/builders/raspberry4b_64-ubuntu20/-/commit/13e147033819e85d0bb2ea4aac23e7d815ef1811>`_.

The following DockerFile snippet shows how the KOP tool and its dependencies are unpackaged and installed on a container: ::

  ## KOP tool
  # install kop utility & dependencies
  # -> python2.7 packages
  ADD data/pip2/functools3-23.2.3.post2.tar.gz /opt/install/kop/lib/python2.7/site-packages/
  ADD data/pip2/jsonschema-2.6.0.tar.gz /opt/install/kop/lib/python2.7/site-packages/
  # -> kop utility
  ADD data/other/KOP-2.21.14-full_on_ubu64_1804.tar.gz /opt/install/kop/kop4c/
  RUN mv /opt/install/kop/kop4c/KOP-2.21.14-full_on_ubu64_1804 /opt/install/kop/kop4c/2.21.14
  # Update lib to current target
  RUN ln -s /opt/install/kop/kop4c/2.21.14/lib/linux_x64 /opt/install/kop/kop4c/2.21.14/lib/target
  # -> kop utility license
  COPY --chown=1000:1000 data/other/bonseyes.lic /opt/install/kop/kop4c/
  ENV LM_LICENSE_FILE=/opt/install/kop/kop4c/bonseyes.lic
  # -> kop utility wrappers
  RUN echo "#!/bin/sh\n" \
           "PYTHONPATH=/opt/install/kop/lib/python2.7/site-packages /opt/install/kop/kop4c/2.21.14/kop \$@\n" > /usr/local/bin/kop
  RUN chmod +x /usr/local/bin/kop
  RUN echo "#!/bin/sh\n" \
           "PYTHONPATH=/opt/install/kop/lib/python2.7/site-packages /opt/install/kop/kop4c/2.21.14/pcop \$@\n" > /usr/local/bin/pcop
  RUN chmod +x /usr/local/bin/pcop 

Cipher used to encrypt/decrypt IP assets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The IP assets will be encrypted with the chacha20 cipher.
ChaCha20 is a stream cipher whose secret key is 256 bits long (32 bytes).
This cipher requires a nonce, 128 bits long (16 bytes), which must not be reused across encryptions performed with the same key.

The nonce is generated randomly and stored at the beginning of the encrypted content, as described in the following pseudo code: ::

  nonce = random(16)
  ciphertext = nonce + chacha20(key, nonce,   cleartext)


Cipher used to verify the license signature
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A hardcoded ECDSA Key pair is used to generate and then verify the license signature :

* Private key in license generator tool to generate the signature.
* Public key in lpdnn-security library to verify the signature.


LPDNN security library
~~~~~~~~~~~~~~~~~~~~~~

A new library named lpdnn-security has been added to the SDK as a dependency of the lpdnn-utils library.
This library, obfuscated by the KOP tool, will be used when a lpdnn::Context with a licensing feature needs to be used.


*****************
AI-App generation
*****************

As described in :doc:`/pages/dev_guides/ai_app_index`, it is possible to generate an AI-App using **AI-App generator script**, with the following command:

    .. code-block:: bash

       deployment_packages/${platformName}/tools/ai_app_generator/bin/ai-app-generator \
           --algorithm-file catalog/${modelName-version-size-precision}/algorithm.yml  \
           --challenge-file challenge/${challengeName}/challenge.yml \
           --deployment-file deployment/${modelName-version-size-precision}/deployment.yml  \
           --output-dir build/${new-aiapp-name}

To generate an AI-App encrypted with its pre-licensed file, you have to declare the **lisense** property in the deployment.yml file, as follows: ::

  ...
  subcomponents:
    security:
      license: "ai_app_config.lic"
      hw_arch: “x86 or aarm64”
      hw_os: “Linux”
  ...

Then, the generated AI-App will be encrypted, and the pre-license file ai_app_config.lic will be generated in the output directory.

Engines
~~~~~~~

The AI-App generator will call the target inference engine to generate the AI-App files according to the specifications of the engine.

For instance, LNE's AI-Apps contain two files for the architecture (model.json) and weights (model.bin). These two files need to be encrypted during the generation of the AI-App, as given in `this snippet 
<https://gitlab.com/bonseyes/lpdnn/ai-app/-/blob/master/engines/lne/components/network_processor/generator/generator.py?ref_type=heads#L179>`_: ::

  ...
  if license:
        component_data.encrypted_files = ['model.json', 'model.bin']
        ai_app_name = deployment_config.data.get('ai_app_name', '')
        encrypt_ai_app_data(ai_app_name, os.path.join(output_directory, "model.json"))
        encrypt_ai_app_data(ai_app_name, os.path.join(output_directory, "model.bin"))
  ...

Other engines may only require encrypting a single file, e.g., model.onnx.


*****************
Detailled Section
*****************


Encryption
~~~~~~~~~~

The encryption is done at build time in the host machine.

All asset encryption changes have been done in the ai-app submodule (https://gitlab.com/bonseyes/lpdnn/ai-app). This submodule contains all the engines that LPDNN supports. The *network_engine.yml* needs to be adapted for each engine to include KOP security.

For instance, a new field named key has been added to the deployment file of the LNE network engine (file engines/lne/components/network_processor/schemas/network_engine.yml): ::

  definitions:
    security:
      type: object
      title: Security linked to deployment of models for the LPDDN inference engine
      description: <
        All security parameters necessary to configure the LPDNN engine to
        execute a given model.
      properties:
        license:
          type: string
          description: Path to generated pre-license file
        hw_arch:
          type: string
          description: Target HW architecture context
        hw_os:
          type: string
          description: Target OS architecture context


This path is passed to the engine's ai_app_generator python script.

To encrypt the assets in python, we use the cryptography package:
https://pypi.org/project/cryptography/

A new encryption function, encrypt_file_chacha20, which encrypts all the file content, has been added to ai-app/ai_app_generator/lib/ai_app_generator/utils.py. This function will be used to encrypt assets.

The first file to encrypt describes the component data: **ai_app_config.json**.
This file is generated by a call to save_component_data in the ai_app_generator/lib/ai_app_generator/cmake.py script.
If a pre-license file has been declared, then the file's content is encrypted using the encrypt_ai_app_config function.

Other files to protect depend on the neural network used and contain model parameters and weight. For instance, in the lenet5 sample application, these files are generated by the LNE generator, located in engines/lne/components/network_processor/generator/generator.py.
Thus, if a key has been declared, the contents of files **model.json** and **model.bin** are encrypted using the encrypt_ai_app_data function.


Check & decryption
~~~~~~~~~~~~~~~~~~

The check of the license and the decryption of assets need to be done at runtime. 

A new library **lpdnn-security** has been added to the lpdnn-utils module to factorize the security code.
For this POC, this module only contains a few functions (declared in utils/security/inc/crypto.h) : 
* **sec_license_alloc** that checks the license signature and compares the runtime device architecture and operating system to values contained in the license file.
* **sec_license_content** that decrypts the content of a buffer.

These functions are used in the class LicenciedFile_context (declared in utils/src/file_context.cpp) that extends the Context interface.
When the end-user has to load licensed resources, it must use this context by calling the following factory function: ::

  namespace lpdnn {
  ...
  std::unique_ptr<lpdnn::Context> make_license_file_context(const std::string& basePath, const std::string& licenseFilePath);
  }

This context is associated to current AI-App instance to be used by the inference engine. ::

  Context *context = get_global_context();
  _weights_resource = context->get_resource(weights_file);

When the inference engine calls Context::get_resource method, the resource's content is read and decrypted.

A ``--license`` parameter has been added to **aiapp_cli** and **http_worker** applications.
This option can be used to pass the license file and creates a LicenciedFile_context with the path of the license.

Example for http-worker : ::

  cd ./build/x86_64-ubuntu20/install/bin/
  . set-lib-path.sh
  ./http-worker --license license.json --cfg ../share/ai-apps/mnist-lenet5-default/ai_app_config.json

Example for aiapp_cli : ::

  cd ./build/x86_64-ubuntu20/install/bin/
  . set-lib-path.sh
  ./aiapp-cli --license license.json --cfg ../share/ai-apps/mnist-lenet5-default/ai_app_config.json -f ../../../../catalog/image-classification/mnist/challenge/samples/image_100.png


Obfuscation
~~~~~~~~~~~

The *Kudelski Obfuscation Process* tool is used to obfuscate the lpdnn-security library.

A new option has been added to the main CMakeFile.txt of the SDK to activate or not the use of the tool ::

  option(KOP_OBFUSCATE            "Activate KOP Obfuscation."           ON)

lpdnn-security obfuscation
--------------------------

To integrate KOP in the build process of the lpdnn-security module, we need to prefix the compiler and linker with KOP.
To do this, we need to change CMake RULE_LAUNCH_COMPILE and RULE_LAUNCH_LINK properties.

The following code snippet shows how we prefix the compiler and linker for lpdnn-security : ::

  set(LIB "lpdnn-security")
  find_program(KOP_PROGRAM /usr/local/bin/kop)

  # Property to compile
  set_property(TARGET ${LIB} PROPERTY RULE_LAUNCH_COMPILE "${KOP_PROGRAM} --kop-config=${CMAKE_CURRENT_SOURCE_DIR}/kop-config.json")
  # Property to link
  set_property(TARGET ${LIB} PROPERTY RULE_LAUNCH_LINK "${KOP_PROGRAM} --kop-config=${CMAKE_CURRENT_SOURCE_DIR}/kop-config.json")

Also, the project needs to link with the kop checksum library (libcx_checksum.a), which needs pthread and dl libraries.
This can be done with CMake with the following code snippet : ::

  # kop dependencies
  add_library(kop-deps INTERFACE)
  target_link_libraries(kop-deps INTERFACE pthread dl)

  # Import kop libs
  set(KOP_IMPORTED_LIB_PATH /opt/install/kop/kop4c/2.22.5/lib/linux_x64/${KOP_MODE})
  add_library(kop_lib_checksum STATIC IMPORTED)
  set_property(TARGET kop_lib_checksum PROPERTY IMPORTED_LOCATION ${KOP_IMPORTED_LIB_PATH}/libcx_checksum.a)
  add_library(kop-libs INTERFACE)
  target_link_libraries(kop-libs INTERFACE kop_lib_checksum)
  add_dependencies(kop-libs kop-deps)

  # Add kop libs & deps
  target_link_libraries (${LIB}
    PRIVATE kop-libs
    PRIVATE kop-deps
  )

To finish, the ``KOP config file``, set using ``--kop-config`` parameter, identifies the options to apply through the KOP process.
It represents the portions of the source code to be protected and describes the protection methods.
Please refer to the KOP documentation for a complete description of all protections available and how to configure them.


***********
Limitations
***********

**Harcoded ECDSA Key pair in license generator**

The private and public ECDSA keys are hardcoded in the license generator tool.

Special care should be taken to distribute this tool to a privileged audience.

**Hardcoded key in license validation library**

The license library contains a hard-coded ECDSA public key to verify the device signature and the key to decrypt the AI data.

As minimal protection, the license verification process is only available if the KOP tool has been installed for the build platform: obfuscation of code is mandatory.

**No key management**

This POC provides no key management.

This issue should be addressed in production: the license generator needs the key pair and should be securely transferred (or created).
