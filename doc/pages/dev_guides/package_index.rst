**********
Definition
**********

An AI artifact package is a directory containing AI artifacts.

An AI artifact package always contain a metadata file called ``package.yml``.

This schema of the file is described below.

.. jsonschema:: ../../../schemas/core/package.yml

*********
Meta Data
*********

All AI artifacts contain a property ``metadata`` that describes them.

The schema of this section is the following:

.. jsonschema:: ../../../schemas/core/metadata.yml

**************
Create Package
**************

This guide assumes you already have the done the following:

- Setup the local environment as explained in :doc:`/pages/setup`

All AI Artifacts managed by the Bonseyes tooling process are stored in packages. Packages are directories in the local
file system. Thanks to their globally unique name they can be replicated on multiple hosts, re-organized
according in a file structure most appropriate for each project and shared between multiple organizations participating
in the AI development process.

Each package has a unique name. To ensure that it is globally unique the package name must always start with the domain
name of the organization in reverse underscore notation (i.e. for the organization "Example Inc" with domain name
``example.com`` the package name must start with ``com_example``). The rest of the package name is an identifier that
can be choosen freely by the organization.

The following are examples of valid package names:

- ``com_example/mychallenge``
- ``com_example/challenges/mychallenge``
- ``com_example/models/mymodel``

To create a package you can use the following command::

    $ bonseyes package create ${PACKAGE_DIR} --name ${PACKAGE_NAME}

where ``${PACKAGE_DIR}`` should be substituted with the directory where the package should be stored and
``${PACKAGE_NAME}`` should be substituted with the name of the package being created.
