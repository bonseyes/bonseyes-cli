$schema: http://json-schema.org/draft-04/schema#
$ref: "#/definitions/model_deployment"
definitions:
  model_deployment:
    type: object
    title: Deployment models for the LPDDN inference engine
    description: <
      All information used necessary to configure the LPDNN engine to
      execute a given model.
    properties:
      data_type:
        type: string
        description: Data type used for computations
        enum:
          - ANY
          - F32
          - F16
          - INT64
          - S16
          - S8
          - U8
        default: F32
      optimizations:
        type: array
        description: Optimizations to enable
      qfile:
        type: string
        description: File containing qdata
      log_severity:
        type: string
        description: Define the logging level
        enum:
          - verbose
          - info
          - warning
          - error
          - fatal
      verbosity_level:
        type: integer
        description: Defines verbosity level when logging severity is verbose; A higher value is more verbose.
      execution_mode:
        type: string
        description: Choose between sequential or parallel execution
        enum:
          - sequential
          - parallel
      enable_profiling:
        type: string
        description: Enable the ORT engine profiling and provide the profile file path prefix
      graph_optimization_level:
        type: string
        description: ONNX graph optimization level
        enum:
          - none
          - basic
          - extended
          - all
      optimized_model_path:
        type: string
        description: The path to save the optimized model
      inter_op_threads:
        type: integer
        description: Sets the max number of inter-op threads used to parallelize the execution
      intra_op_threads:
        type: integer
        description: Sets the max number of intra-op threads used to parallelize the execution (no OpenMP builds)
      execution_providers:
        type: object
        default: {}
        description: Defines available execution providers
        properties:
          cuda:
            type: object
            description: CUDA execution provider configuration.
            properties:
              device_id:
                type: integer
                description: CUDA device id
                default: 0
              gpu_mem_limit:
                type: integer
                description: The size limit of the device memory arena in bytes
              cudnn_conv_algo_search:
                type: string
                description: The type of search done for cuDNN convolution algorithms
                enum:
                  - exhaustive
                  - heuristic
                  - default
              do_copy_in_default_stream:
                type: boolean
                description: Whether to do copies in the default stream or use separate streams
                default: true
              cudnn_conv_use_max_workspace:
                type: integer
                description: Check tuning performance for convolution heavy models
                default: 0
          tensorrt:
            type: object
            description: TensorRT execution provider configuration.
            properties:
              device_id:
                type: integer
                description: CUDA device id
                default: 0
              max_workspace_size:
                type: integer
                description: Max workspace memory size for engine build
              max_partition_iterations:
                type: integer
                description: Max number of partition iterations
              min_subgraph_size:
                type: integer
                description: Min subgraph size
              fp16_enable:
                type: boolean
                description: Enable FP16
                default: true
              int8_enable:
                type: boolean
                description: Enable Int8
                default: false
              int8_calibration_table_name:
                type: string
                description: Path to the Int8 calibration table
              dla_enable:
                type: boolean
                description: Enable DLA
                default: false
              dla_core:
                type: integer
                description: Select DLA core
              engine_cache_enable:
                type: boolean
                description: Enable engine cache
              engine_cache_path:
                type: string
                description: Path to the engine cache
              dump_subgraphs:
                type: boolean
                description: Enable subgraphs dump
    additionalProperties: false
