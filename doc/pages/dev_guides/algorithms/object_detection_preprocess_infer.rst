Object Detection Preprocess Infer Algorithm
===========================================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/object_detection_preprocess_infer/algorithm.yml
   :language: yaml
