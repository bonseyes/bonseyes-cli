Image Preprocessor Algorithm
============================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/image_preprocessor/algorithm.yml
   :language: yaml

The algorithm supports the following configuration parameters:

.. jsonschema:: ../../../../algorithms/image_preprocessor/parameters.yml#/definitions/image_preprocessing