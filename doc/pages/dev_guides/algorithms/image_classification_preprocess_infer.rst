Image Classification Preprocess Infer Algorithm
===============================================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/image_classification_preprocess_infer/algorithm.yml
   :language: yaml
