Algorithm Configurations
========================

An algorithm config specifies the parameters for an algorithm and its subcomponents.

An algorithm config artifact is typically defined in a file ``algorithm.yml``.

The schema of the file is described below:

.. jsonschema:: ../../../../schemas/core/algorithm_config.yml

The algorithm config includes parameters for each algorithm and its subcomponents. The schemas of the supported
configuration are described in the corresponding algorithm definition (see :doc:`/pages/dev_guides/ai_app/algorithms`).
