Image Segmentation Preprocess Infer Algorithm
=============================================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/image_segmentation_preprocess_infer/algorithm.yml
   :language: yaml
