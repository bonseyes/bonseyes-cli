Network Processor Algorithm
===========================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/inference_processor/algorithm.yml
   :language: yaml

The algorithm supports the following configuration parameters:

.. jsonschema:: ../../../../algorithms/inference_processor/parameters.yml