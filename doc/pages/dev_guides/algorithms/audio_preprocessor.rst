Audio Preprocessor Algorithm
============================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/audio_preprocessor/algorithm.yml
   :language: yaml

The algorithm supports the following configuration parameters:

.. jsonschema:: ../../../../algorithms/audio_preprocessor/parameters.yml#/definitions/audio_preprocessing