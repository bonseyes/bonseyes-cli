Signal Preprocessor Algorithm
============================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/signal_preprocessor/algorithm.yml
   :language: yaml

The algorithm supports the following configuration parameters:

.. jsonschema:: ../../../../algorithms/signal_preprocessor/parameters.yml#/definitions/signal_preprocessing
