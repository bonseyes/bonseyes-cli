Face Recognition Preprocess Infer Algorithm
===========================================

The algorithm has the following description:

.. literalinclude:: ../../../../algorithms/face_recognition_preprocess_infer/algorithm.yml
   :language: yaml

The algorithm supports the following configuration parameters:

.. jsonschema:: ../../../../algorithms/face_recognition_preprocess_infer/parameters.yml#/definitions/preprocess_infer