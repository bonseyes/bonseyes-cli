Object Detection
================

The parameters are the following:

.. jsonschema:: ../../../../interfaces/object_detection/parameters.yml

The OpenAPI definition of the HTTP interface is the following:

.. literalinclude:: ../../../../interfaces/object_detection/http_api.yml
   :language: yaml

The C++ interface definition is the following:

.. literalinclude:: ../../../../inc/object_detection.hpp
   :language: cpp

The default JSON ground truth schema is the following:

.. jsonschema:: ../../../../interfaces/object_detection/ground_truth.yml

The default JSON result serialization is the following:

.. jsonschema:: ../../../../interfaces/object_detection/results.yml

