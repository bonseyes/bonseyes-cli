Utility Schemas
===============

This schema is used for parameters of interfaces accepting images:

.. jsonschema:: ../../../../../interfaces/common/image_input.yml

This schema is used for parameters of interfaces accepting audio:

.. jsonschema:: ../../../../../interfaces/common/audio_input.yml