Face Recognition
================

The parameters are the following:

.. jsonschema:: ../../../../interfaces/face_recognition/parameters.yml

The OpenAPI definition of the HTTP interface is the following:

.. literalinclude:: ../../../../interfaces/face_recognition/http_api.yml
   :language: yaml

The C++ interface definition is the following:

.. literalinclude:: ../../../../inc/face_recognition.hpp
   :language: cpp

The default JSON ground truth schema is the following:

.. jsonschema:: ../../../../interfaces/face_recognition/ground_truth.yml

The default JSON result serialization is the following:

.. jsonschema:: ../../../../interfaces/face_recognition/results.yml

