Image Segmentation
==================

The parameters are the following:

.. jsonschema:: ../../../../interfaces/image_segmentation/parameters.yml

The OpenAPI definition of the HTTP interface is the following:

.. literalinclude:: ../../../../interfaces/image_segmentation/http_api.yml
   :language: yaml

The C++ interface definition is the following:

.. literalinclude:: ../../../../inc/image_segmentation.hpp
   :language: cpp

The default JSON ground truth schema is the following:

.. jsonschema:: ../../../../interfaces/image_segmentation/ground_truth.yml

The default JSON result serialization is the following:

.. jsonschema:: ../../../../interfaces/image_segmentation/results.yml

