Signal Classification
=====================

The parameters are the following:

.. jsonschema:: ../../../../interfaces/signal_classification/parameters.yml

The OpenAPI definition of the HTTP interface is the following:

.. literalinclude:: ../../../../interfaces/signal_classification/http_api.yml
   :language: yaml

The C++ interface definition is the following:

.. literalinclude:: ../../../../inc/signal_classification.hpp
   :language: cpp

The default JSON ground truth schema is the following:

.. jsonschema:: ../../../../interfaces/signal_classification/ground_truth.yml

The default JSON result serialization is the following:

.. jsonschema:: ../../../../interfaces/signal_classification/results.yml

