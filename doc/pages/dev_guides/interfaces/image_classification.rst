Image Classification
====================

The parameters are the following:

.. jsonschema:: ../../../../interfaces/image_classification/parameters.yml

The OpenAPI definition of the HTTP interface is the following:

.. literalinclude:: ../../../../interfaces/image_classification/http_api.yml
   :language: yaml

The C++ interface definition is the following:

.. literalinclude:: ../../../../inc/image_classification.hpp
   :language: cpp

The default JSON ground truth schema is the following:

.. jsonschema:: ../../../../interfaces/image_classification/ground_truth.yml

The default JSON result serialization is the following:

.. jsonschema:: ../../../../interfaces/image_classification/results.yml

