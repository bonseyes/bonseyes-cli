*************
Prerequisites
*************

To use the Bonseyes CLI tool we recommend to use a developer workstation with
`Ubuntu 18.04 LTS <http://releases.ubuntu.com/18.04/>`_, other OS version may also work but have
not been fully tested. Virtual Machines, e.g., VirtualBox, can also be used for usage/development in CPU only. VM images can be found in `here <https://www.linuxvmimages.com/images/ubuntu-1804/>`__ to create a new environment.

If you don't have VirtualBox installed, you can find the binary for VirtualBox in `here <https://www.virtualbox.org/wiki/Downloads>`__.
To use this VM, you can follow the instructions in `here <https://www.linuxvmimages.com/how-to-use/how-to-import-vm-images-in-virtualbox/>`__. By following this tutorial, you will be able to import the VM image and modify the resources used by the VM. If you want to be able to use copy-paste or shared folder functionalities, please install the VirtualBox Guest Additions. For that, you can follow the instruction in `here <https://linuxize.com/post/how-to-install-virtualbox-guest-additions-in-ubuntu/>`__.

If you encounter problems with network access after the setup of the VM, you can modify the network interface to NAT in the configuration of the VM :

.. image:: assets/NAT_conf.png
  :alt: Image of the NAT network interface configuration

The overall process of installing the VM, donwloading the image and configuring the VM is estimated to last around 1h.

Bonseyes workflows assume that the workstation has an internet connection.
Additional devices may be required depending on the platform that will be used (e.g. SD card reader,
Ethernet card), more details are available in the platform package. 

The Bonseyes CLI tool uses some assets hosted on Gitlab. To make sure they are available, first, register
in GitLab and create an account. Then, make sure you user has access to Gitlab with SSH. To do so follow the
`instructions <https://docs.gitlab.com/ee/ssh/README.html>`_.

*******************************
Setup the developer workstation
*******************************

The Bonseyes CLI tool requires a few software packages to be installed in the developer workstation (VM or native machine):
To start with, update the system by executing:

.. code-block:: bash

    sudo apt-get update

Docker
~~~~~~
To install docker, follow the instructions in `here <https://docs.docker.com/engine/install/ubuntu/>`__.

By default, docker is not accessible to normal users. To allow the current user to access docker, run the following command:

.. code-block:: bash

    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker

Verify that you can run docker commands without sudo:

.. code-block:: bash

     docker run hello-world


Git and Git LFS
~~~~~~~~~~~~~~~
Install git and git LFS by executing:

.. code-block:: bash

     sudo apt-get install git git-lfs

Git LFS is not active by default. To make sure git lfs is active, run the following command:

.. code-block:: bash

     git lfs install

The command will print some errors that can be safely ignored.

Due to a bug in Ubuntu 18.04 LTS, the binaries installed by pip are not available by default. To make sure that they are available, run the following command:

.. code-block:: bash

     echo 'export PATH="$HOME/.local/bin:$PATH"' >> .bashrc

Unfortunately, due to some limitations in Ubuntu 18.04 LTS, it is not possible to ensure that the new user groups are taken into account after a simple logout/login.
To complete the setup, it is necessary to restart the machine so that the changes to PATH and groups take effect.


Packages
~~~~~~~~
The remaining packages can be installed by executing the following command:

.. code-block:: bash

    sudo apt-get install python3 python3-pip python3-wheel python3-setuptools


.. _bonseyesTool:

**********************
Install Bonseyes tool
**********************

Once all required software packages are installed it is possible to install the bonseyes tool with
the following command::

    $ pip3 install git+ssh://git@gitlab.com/bonseyes/bonseyes-cli.git

The command will fail with an error "Access Denied" if the ssh keys have not been setup correctly in
Gitlab. If you are not using the original version of pip distributed with Ubuntu you may need to add
the ``--user`` flag to the command to force installation of the tool in your current user.

In order to successfully install the tool on ARM-based platforms it is required to execute following commands::

    $ sudo apt-get install libffi-dev libssl-dev
    $ export PATH="$HOME/.local/bin:$PATH"

Once the tool is installed it is possible to check that all the required software is correctly
installed with the following command. Please note that you mave have to restart your terminal after installing bonseyes-cli for this command to work::

    $ bonseyes system check

If the setup was successful all the results should be green.

*************************
Log in to the marketplace
*************************
Users need to create an acount in the Bonseyes Marketplace to access and use the Bonseyes artifacts and workflows. Please, register `here <https://beta.bonseyes.com/register>`_. 

To log in to the marketplace from the command line (terminal), use the following command:

    $ bonseyes marketplace login

Once logged in the credentials are stored in a file in the home directory and will be re-used by
other commands that interact with the marketplace.
