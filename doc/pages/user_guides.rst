*********
Challenge
*********

.. toctree::
   :maxdepth: 4

   user_guides/challenge_index

********
Datatool
********

.. toctree::
   :maxdepth: 4

   user_guides/datatool_index

********
Platform
********

.. toctree::
   :maxdepth: 4

   user_guides/platform_index

********
AI Asset
********

.. toctree::
   :maxdepth: 4

   user_guides/ai_asset_index

******
AI App
******

.. toctree::
   :maxdepth: 4

   user_guides/ai_app_index

