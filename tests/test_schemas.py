#!/usr/bin/env python3

import unittest

import jsonschema
import yaml
import os

from setuptools import glob


def check_yaml(file_name):
    if os.path.isdir(file_name):
        check_yaml(file_name)
    else:
        with open(file_name) as fp:
            yaml.safe_load(fp)


def check_schema(file_name):
    if os.path.isdir(file_name):
        check_yaml(file_name)
    else:
        with open(file_name) as fp:
            schema = yaml.safe_load(fp)
            jsonschema.Draft4Validator.check_schema(schema)


BASE_PATH = os.path.join(os.path.dirname(__file__), '..')


class TestSchemas(unittest.TestCase):

    def test_syntax(self):
        for match in glob.glob(os.path.join(BASE_PATH, 'examples', '**', '*.yml')):
            check_yaml(match)

        for match in glob.glob(os.path.join(BASE_PATH, 'ai_algorithms', '**', '*.yml')):
            check_yaml(match)

        for match in glob.glob(os.path.join(BASE_PATH, 'ai_algorithm_components', '**', '*.yml')):
            check_yaml(match)

        for match in glob.glob(os.path.join(BASE_PATH, 'ai_classes', '**', '*.yml')):
            check_yaml(match)

    def test_schemas(self):
        for match in glob.glob(os.path.join(BASE_PATH, 'schemas', '**', '*.yml')):
            check_schema(match)
