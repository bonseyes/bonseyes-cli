import setuptools
import os

with open("tool/README.rst", "r") as fh:
    long_description = fh.read()

with open("tool/requirements.txt", "r") as fh:
    requirements = fh.read().split('\n')

data_files = []

for sub_data_dir in ['interfaces', 'algorithms', 'schemas', 'network_formats']:
    data_files.extend([(os.path.join('share', 'bonseyes', d),
                        [os.path.join(d, f) for f in files])
                       for d, folders, files in os.walk(sub_data_dir)])

packages = [dirpath.replace('/', '.')[len('tool/lib/'):] for dirpath, _, _ in os.walk('tool/lib')
            if not dirpath.endswith('/__pycache__') and dirpath != 'tool/lib']


setuptools.setup(
    name="bonseyes",
    version=os.environ.get('BONSEYES_PACKAGE_VERSION', '0.0.0dev0'),
    author="Bonseyes",
    author_email="info@bonseyes.com",
    description="Bonseyes Tools",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.com/bonseyes/bonseyes-cli",
    package_dir={'': 'tool/lib'},
    data_files=data_files,
    scripts=['tool/bin/bonseyes'],
    packages=packages,
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.5',
)
