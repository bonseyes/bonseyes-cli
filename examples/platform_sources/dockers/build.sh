#!/bin/bash

set -e
set -x

BASE_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))/../../../

cd ${BASE_PATH}

docker build -t com_example/platform/builder_base -f examples/platform_sources/dockers/builder_base/Dockerfile .
docker build -t com_example/platform/manager_base -f examples/platform_sources/dockers/manager_base/Dockerfile .
docker build -t com_example/platform/support -f examples/platform_sources/dockers/support/Dockerfile .