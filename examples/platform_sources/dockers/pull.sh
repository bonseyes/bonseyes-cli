#!/bin/bash

set -e
set -x

docker pull registry.gitlab.com/bonseyes/benchmarks/docker-base-images/python:v1
docker pull registry.gitlab.com/bonseyes/platforms/manager-server:latest
