#!/bin/bash

# this is the script used configure the target board

echo "Platform Sources Input Directory: ${PLATFORM_DIR}"
echo "Built OS Input Directory: ${OS_DIR}"
echo
echo "Target Configuration Output Directory: ${TARGET_CONFIG_DIR}"
echo

echo "Creating fake target configuration"
echo "configured" > ${TARGET_CONFIG_DIR}/fake_config
