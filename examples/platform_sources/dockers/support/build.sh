#!/bin/bash

# this is the script used to assemble builder and manager docker sources

echo "Platform Sources Input Directory: ${PLATFORM_DIR}"
echo "Third Party Dependencies Input Directory: ${DEPS_DIR}"
echo
echo "Built OS Output Directory: ${OS_DIR}"
echo "Builder Docker Sources Output Directory: ${BUILDER_SOURCES_DIR}"
echo "Manager Docker Sources Output Directory: ${MANAGER_SOURCES_DIR}"
echo

echo "Preparing builder sources directory"
cp -r ${PLATFORM_DIR}/platform/assets/builder_sources/* ${BUILDER_SOURCES_DIR}

echo "Copying fake third party dependency to builder sources"
cp ${DEPS_DIR}/fake_dependency.txt ${BUILDER_SOURCES_DIR}


echo "Preparing manager sources directory"
cp -r ${PLATFORM_DIR}/platform/assets/manager_sources/* ${MANAGER_SOURCES_DIR}

echo "Copying fake third party dependency to manager sources"
cp ${DEPS_DIR}/fake_dependency.txt ${MANAGER_SOURCES_DIR}
