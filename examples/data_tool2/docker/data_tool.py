#!/usr/bin/python3
import argparse
import json
import os

import jsonschema
import shutil
import yaml


def prepare_data(credentials, output_dir):

    # load credentials
    with open(credentials) as fp:
       password = fp.read()

    if password != 'secret':
        raise Exception('Invalid credentials')

    # create an dataset file with the metadata for one sample
    with open(os.path.join(output_dir, 'example_file'), 'w') as fp:
        fp.write("test")

    with open(os.path.join(output_dir, 'dataset.json'), 'w') as fp:
        json.dump({'samples': [{'id': 'sample1',
                                'image': 'example_file',
                                'bounding_box': [1, 2, 3],
                                'image_class': 'class1'}]}, fp)


def main():

    parser = argparse.ArgumentParser(description="Example Data Tool")

    parser.add_argument("--credentials", help="File with the credentials")
    parser.add_argument("--output-dir", help="Directory where to store the dataset", required=True)

    args = parser.parse_args()

    prepare_data(args.credentials, args.output_dir)


if __name__ == '__main__':
    main()
