#!/bin/bash

set -e
set -x

BASE_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))/../../

cd ${BASE_PATH}

docker pull registry.gitlab.com/bonseyes/benchmarks/docker-base-images/python:v1

docker build -t com_example/evaluation_tool -f examples/evaluation_tool/docker/Dockerfile .