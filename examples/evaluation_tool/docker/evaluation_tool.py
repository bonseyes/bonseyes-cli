#!/usr/bin/python3
import argparse
import json
import os

import jsonschema
import requests
import yaml


def benchmark(parameters, target_url, dataset_dir):

    # read the metadata for the samples of the dataset
    with open(os.path.join(dataset_dir, 'dataset.json')) as fp:
        dataset = json.load(fp)

    results = {}

    # iterate over each sample in the dataset and process it with the ai-app
    for sample in dataset['samples']:

        print("Processing sample " + sample['id'])

        # read the data for the sample
        with open(os.path.join(dataset_dir, sample['image']), 'rb') as fp:
            data = fp.read()

        with requests.post(target_url, data=data) as ret:
            if ret.status_code != 200:
                raise Exception('Invalid response received from server')

            results[sample['id']] = ret.json()

    return results


def load_parameters(parameters_path):
    # load the parameters
    if parameters_path is not None:
        with open(os.path.join(parameters_path)) as fp:
            parameters = yaml.safe_load(fp)
    else:
        parameters = {}

    base_dir = os.path.dirname(__file__)

    # check that the parameters passed are correct
    with open(os.path.join(base_dir, 'parameters.yml')) as fp:
        schema = yaml.safe_load(fp)

    jsonschema.validate(parameters, schema)

    return parameters


def main():

    parser = argparse.ArgumentParser(description="Example Evaluation Tool")

    parser.add_argument("--parameters", help="Parameters file")
    parser.add_argument("--target-url", required=True, help="URL of the target platform")
    parser.add_argument("--dataset-dir", required=True, help="Directory with the dataset")
    parser.add_argument("--output-dir", required=True, help="Output directory where the benchmark will be stored")

    args = parser.parse_args()

    results = benchmark(load_parameters(args.parameters), args.target_url, args.dataset_dir)

    os.makedirs(args.output_dir, exist_ok=True)

    with open(os.path.join(args.output_dir, 'output.json'), 'w') as fp:
        json.dump(results, fp, indent=True)


if __name__ == '__main__':
    main()
