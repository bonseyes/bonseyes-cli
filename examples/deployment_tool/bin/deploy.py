#!/usr/bin/python3 -u

import argparse
import json
import os
from uuid import uuid4

from bonseyes.algorithm import AlgorithmConfig
from bonseyes.challenge import Challenge
from bonseyes.context import Context
from bonseyes.platform import PlatformSources
from bonseyes.utils import get_data_dir


def main():

    parser = argparse.ArgumentParser(description="Deployment tool")

    parser.add_argument('--platform-path', required=True)
    parser.add_argument('--model-file', required=True)
    parser.add_argument('--deployment-config-file', required=True)
    parser.add_argument('--challenge-file')
    parser.add_argument('--output-dir', required=True)
    parser.add_argument('--debug', action='store_true')

    args = parser.parse_args()

    # create a context used to load assets from packages with the default packages
    package_paths = [os.path.join(get_data_dir(), 'interfaces'), os.path.join(get_data_dir(), 'algorithms')]
    context = Context(package_paths)

    # register the packages containing the various artifacts that where passed as arguments
    context.register_package_for_artifact(args.platform_path + '/platform.yml')
    context.register_package_for_artifact(args.model_file)
    context.register_package_for_artifact(args.deployment_config_file)
    context.register_package_for_artifact(args.challenge_file)

    # load the platform sources
    platform = PlatformSources.load(context.find_artifact_name(args.platform_path), context)

    # load the ai algorithm config
    algorithm_config = AlgorithmConfig.load(context.find_artifact_name(args.model_file), context)

    # write out the ai_app artifact description
    with open(os.path.join(args.output_dir, 'ai_app.yml'), 'w') as fp:
        json.dump({
            'platform': platform.platform_build_name,
            'challenge': algorithm_config.challenge_name,
            'runtime': 'com_example/runtime#runtime.yml'
        }, fp)

    challenge = Challenge.load(algorithm_config.challenge_name, context)   # type: Challenge

    # write out the "code" for the ai app, this will be used by the runtime
    # to execute the ai app
    with open(os.path.join(args.output_dir, 'example_ai_app.json'), 'w') as fp:
        json.dump({'interface': challenge.interface_name}, fp)

    # create a package file
    with open(os.path.join(args.output_dir, 'package.yml'), 'w') as fp:
        json.dump({'name': 'uuid_' + str(uuid4())}, fp)


if __name__ == "__main__":
    main()
