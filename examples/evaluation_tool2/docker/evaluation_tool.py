#!/usr/bin/python3
import argparse
import json
import os

import requests


def benchmark(target_url, dataset_dirs):

    images_dir = [x[1] for x in dataset_dirs if x[0] == 'images'][0]
    bounding_boxes_dir = [x[1] for x in dataset_dirs if x[0] == 'bounding_boxes'][0]

    # read the data for the image samples
    with open(os.path.join(images_dir, 'dataset.json')) as fp:
        dataset = json.load(fp)

    # read the data for the bounding boxes
    with open(os.path.join(bounding_boxes_dir, 'dataset.json')) as fp:
        bounding_boxes_dataset = json.load(fp)

    # create bounding boxes index
    bounding_boxes = {x['id']: x['bounding_box'] for x in bounding_boxes_dataset['samples']}

    results = {}

    # iterate over each sample in the dataset and process it with the ai-app
    for sample in dataset['samples']:

        print("Processing sample " + sample['id'])

        # read the data for the sample
        with open(os.path.join(images_dir, sample['image']), 'rb') as fp:
            data = fp.read()

        # find the bounding box corresponding to the sample
        bounding_box = bounding_boxes[sample['id']]

        with requests.post(target_url, data=data) as ret:
            if ret.status_code != 200:
                raise Exception('Invalid response received from server')

            results[sample['id']] = ret.json()

    return results


def main():

    parser = argparse.ArgumentParser(description="Example Evaluation Tool")

    parser.add_argument("--target-url", required=True, help="URL of the target platform")
    parser.add_argument("--dataset-dir", nargs=2, action="append", required=True, help="Directory with the dataset")
    parser.add_argument("--output-dir", required=True, help="Output directory where the benchmark will be stored")

    args = parser.parse_args()

    results = benchmark(args.target_url, args.dataset_dir)

    os.makedirs(args.output_dir, exist_ok=True)

    with open(os.path.join(args.output_dir, 'output.json'), 'w') as fp:
        json.dump(results, fp, indent=True)


if __name__ == '__main__':
    main()
