#!/usr/bin/python3
import argparse
import json
import os

import jsonschema
import shutil
import yaml


def prepare_data(parameters, input_dir, output_dir):

    # copy the data that we received
    shutil.copy(os.path.join(input_dir, 'example_file'), os.path.join(output_dir, 'example_file'))

    # create an dataset file with the metadata for one sample
    with open(os.path.join(output_dir, 'dataset.json'), 'w') as fp:
        json.dump({'samples': [{'id': 'sample1',
                                'image': 'example_file',
                                'bounding_box': [1, 2, 3],
                                'image_class': 'class1'}]}, fp)


def load_parameters(parameters_path):

    # load the parameters
    if parameters_path is not None:
        with open(os.path.join(parameters_path)) as fp:
            parameters = yaml.safe_load(fp)
    else:
        parameters = {}

    base_dir = os.path.dirname(__file__)

    # check that the parameters passed are correct
    with open(os.path.join(base_dir, 'parameters.yml')) as fp:
        schema = yaml.safe_load(fp)

    jsonschema.validate(parameters, schema)

    return parameters


def main():

    parser = argparse.ArgumentParser(description="Example Data Tool")

    parser.add_argument("--parameters", help="File with the import configuration parameters")
    parser.add_argument("--input-dir", help="Directory with the input data", required=True)
    parser.add_argument("--output-dir", help="Directory where to store the dataset", required=True)

    args = parser.parse_args()

    prepare_data(load_parameters(args.parameters), args.input_dir, args.output_dir)


if __name__ == '__main__':
    main()
