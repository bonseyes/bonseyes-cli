#!/bin/bash

set -e
set -x

BASE_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))/../../

cd ${BASE_PATH}

docker build -t com_example/data_tool -f examples/data_tool/docker/Dockerfile .