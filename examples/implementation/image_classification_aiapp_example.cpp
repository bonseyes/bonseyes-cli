///
/// Sample implementation of an image-classification Ai-app
/// This is not meant to be used in real applications but just as an example
/// of how to write a "standalone" aiapp which is not based on the
/// LPDNN aiapp framework. We will use LNE to perform inference but the code
/// can be easily adapted for any other framework.
///
/// \copyright 2020 NVISO SA. All rights reserved.
///


#include <memory>

//#include "aiapp_factory.hpp"
#include "image_classification_example.hpp"
#include <nlohmann/json.hpp>
#include <iostream>
#include "opencv2/core/mat.hpp"

using namespace std;
using namespace nlohmann;

namespace lpdnn {
namespace ai_app {


bool Image_classification_example::init(const std::string& cfg) {
  json jcfg = json::parse(cfg);
  if (jcfg.is_null()) {
    std::cerr << "Error parsing configuration string\n";
    return false;
  }

  json aiapp_parameters_config = jcfg["parameters"];
  auto aiapp_classes = aiapp_parameters_config["classes"];
  json subcomponents_config = jcfg.at("subcomponents");

  // Read preprocessing parameters
  json pre_processing_config = subcomponents_config.at("preprocessing");
  json pre_processing_parameters = pre_processing_config.at("parameters");
  json normalization_parameters = pre_processing_parameters.at("normalization");
  normalization_parameters.at("mean_data").get_to(_mean);
  normalization_parameters.at("std_data").get_to(_std);

  // Get network parameters
  json inference_config = subcomponents_config.at("inference");
  json model_config = inference_config.at("parameters").at("model");
  string model_architecture_file = model_config.at("architecture");
  string model_weights_file = model_config.at("weights");

  std::cout << "mean " << _mean[0] << endl;
  std::cout << "std " << _std[0] << endl;
  std::cout << "model_architecture_file " << model_architecture_file << endl;

  return true;
}



Image_classification::Output Image_classification_example::execute(
    const ai_app::Image& input) {
  Image_classification::Result result;

  //
  // Preprocessing
  //

  // Convert aiapp image from jpeg to opencv Mat
  // (see aiapp Image_preprocessor for a full implementation)
  assert(img.format == ai_app::Image::Format::encoded);
  cv::Mat mat = cv::imdecode(img.data, img.data_size, cv::IMREAD_COLOR_RGB, cv::IMREAD_8BIT);

  // Perform cropping
  mat = do_crop(mat);

  // Perform alignment
  mat = do_align(mat);

  // Perform normalization
  mat = do_normalization(mat);

  // Reorganize image data as expected by the model (RGB BGR etc)
  mat = do_convert_layout(mat);

  //
  // Inference
  //

  _network.set_input("", mat.data()));
  result.success = _network.infer();

  //
  // Postoprocessing
  //

  // Convert the network output to result

  result.confidence[] = my_postprocess(_network.output());
  
  return result;
}


std::vector<std::string> Image_classification_example::classes() {
  return {};
}


std::vector<ai_app::Image::Format> Image_classification_example::image_formats() const {
  return {};
}



}
}


