///
/// Sample implementation of an image-classification Ai-app
/// This is not meant to be used in real applications but just as an example
/// of how to write a "standalone" aiapp which is not based on the
/// LPDNN aiapp framework. We will use LNE to perform inference but the code
/// can be easily adapted for any other framework.
///
/// \copyright 2020 NVISO SA. All rights reserved.
///

#pragma once

#include "aiapp_basic.hpp"
#include "image_classification.hpp"

// Use LNE network
#include "Network.hpp"

namespace lpdnn {
namespace ai_app {


/// Sample direct implementation of image classification aiapp
class Image_classification_example : public Aiapp_basic, public ai_app::Image_classification {
public:
  bool init(const std::string& cfg) override;
  Result execute(const ai_app::Image& input) override;
  std::vector<std::string> classes() override;
  std::vector<ai_app::Image::Format> image_formats() const override;
private:
  std::vector<float> _mean;
  std::vector<float> _std;
  Network _net{"Net"};
};


}  // namespace ai_app
}  // namespace lpdnn
