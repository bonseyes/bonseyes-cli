# Bonseyes Tool and default Artifacts

This repository contains the sources of the Bonseyes CLI Tool and
the default artifacts:

- ``tool``: sources of the Bonseyes CLI tool
- ``interfaces``: default AI app interfaces
- ``algorithms``: default AI algorithms
- ``network_formats``: default neutral network formats
- ``schemas``: schemas for the artifact YAML files
- ``examples``: sample artifacts

