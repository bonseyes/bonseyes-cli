///
/// Ai-app interface and types for audio-based ai-app
///
/// \copyright 2019 NVISO SA. All rights reserved.
/// \license This project is released under the XXXXXX License.
///

#pragma once

#include "aiapp.hpp"

namespace lpdnn {
namespace ai_app {

/// Audio representation.
/// An audio sample can be constructed from a vector<uint8_t>, or a string
/// or raw data pointer and size. When passing rvalues vector or strings, the
/// audio will take ownership of the data, otherwise will just keep reference.
class Audio_chunk {
 protected:
  /// Contains audio data if we have ownership of it
  std::vector<uint8_t> _content;

 public:
  /// Audio format
  enum class Format {
    raw = 1,        /// TBD
    encoded = 256,  /// Standard encoded in TBD format
    tile = 512    /// Tile format. Use attributes field for more details.
  };

  /// Don't take data ownership.
  Audio_chunk(Format fmt, const std::vector<uint8_t>& data)
      : Audio_chunk(fmt, data.data(), data.size()) {}
  /// Take data ownership
  Audio_chunk(Format fmt, std::vector<uint8_t>&& data)
      : _content(std::move(data)),
        format{fmt},
        data{_content.data()},
        data_size{_content.size()} {}

  /// Don't take data ownership.
  Audio_chunk(Format fmt, const std::string& data)
      : Audio_chunk(fmt, (uint8_t*)data.c_str(), data.size()) {}
  /// Take data ownership
  Audio_chunk(Format fmt, std::string&& data)
      : Audio_chunk(
            fmt, std::vector<uint8_t>((uint8_t*)data.c_str(),
                                      (uint8_t*)data.c_str() + data.size())) {
    data.clear();
  }

  /// Don't take data ownership
  Audio_chunk(Format fmt, const uint8_t* audio_data, size_t audio_data_size)
      : format{fmt}, data{audio_data}, data_size{audio_data_size} {}

  /// Utility factory methods
  static Audio_chunk encoded(const std::vector<uint8_t>& data) {
    return Audio_chunk(Format::encoded, data);
  }

  /// Audio format
  Format format;

  /// Region of interest inside the audio (all if empty)
  // Range roi{};

  /// Tile attributes.
  /// This is ai-app specific and allows to specify tile data formats.
  std::string attributes;

  /// Pointer to data (no ownership of the data).
  const uint8_t* data;

  /// Size of data.
  size_t data_size;
};

/// Abstract audio-based AiApp
class Audio_based : virtual public Aiapp {
 public:
  /// @return supported audio formats (ordered by preference)
  virtual std::vector<Audio_chunk::Format> audio_formats() const = 0;
};

}  // namespace ai_app
}  // namespace lpdnn
