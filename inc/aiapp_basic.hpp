///
/// Ai-app basic implementation of all non-mandatory methods
///
/// \copyright 2020 NVISO SA. All rights reserved.
///

#pragma once

#include "aiapp.hpp"

namespace lpdnn {
namespace ai_app {

/// Basic AI-App implementation defining all optional methods as no-op
class Aiapp_basic  : virtual public ai_app::Aiapp {
public:
  std::string aiapp_name() const override { return {}; }
  Version version() const { return Version(0, 0, 0, 0); }
  const char* component_name() const override { return {}; }
  bool init(const std::string& cfg) override { return true; }
  bool set_options(const std::string& opt,
                           const std::string& name) override { return {}; }
  std::vector<std::string> components(
      const std::string& name) const override { return {}; }
  std::vector<ai_app::Blob> output(const std::string& name) const override { return {}; }
  std::string metrics(const std::string& name) const override { return {}; }
  bool set_exit_after(const std::string& name) override { return {}; }
  ai_app::Error error() const override { return {}; }
  void clear_error() override {}
};

}  // namespace ai_app
}  // namespace lpdnn
