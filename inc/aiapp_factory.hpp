///
/// Ai-app factory
///
/// \copyright 2019 NVISO SA. All rights reserved.
/// \license This project is released under the XXXXXX License.
///

#pragma once

#include "aiapp.hpp"

#include <cassert>
#include <map>
#include <memory>
#include <string>
#include <type_traits>
#include <iostream>

namespace lpdnn {
namespace aiapp_impl {

class Aiapp_factory {
 public:
  using Create_function = std::unique_ptr<ai_app::Aiapp> (*)();

  /// Register all aiapp implementations
  static void init();

  /// Create and initialize aiapp
  static std::unique_ptr<ai_app::Aiapp> make_aiapp(
      const std::string& cfg_filename,
      const std::string& license_filename = {}) {
    init();
    return do_make_aiapp(cfg_filename, license_filename);
  }

  /// Create and initialize aiapp of the specified abstract class by reading the
  /// implementation class to be created from the configuration file
  template <typename AI_CLASS>
  static std::unique_ptr<typename std::enable_if<std::is_abstract<AI_CLASS>::value, AI_CLASS>::type>
  make(const std::string& cfg_filename, const std::string& license_filename = {}) {
    // We could check AI_CLASS::ai_interface_name in config file here.
    auto aiapp = make_aiapp(cfg_filename, license_filename);
    auto aiapp_ptr = dynamic_cast<typename AI_CLASS::Ai_interface_class*>(aiapp.get());
    if (aiapp_ptr) aiapp.release();
    return std::unique_ptr<AI_CLASS>(static_cast<AI_CLASS*>(aiapp_ptr));
  }

  /// Create and initialize aiapp of the specified concrete class by
  /// calling new directly
  template <typename AI_CLASS>
  static std::unique_ptr<typename std::enable_if<!std::is_abstract<AI_CLASS>::value, AI_CLASS>::type>
  make(const std::string& cfg_filename, const std::string& license_filename = {}) {
    auto aiapp = std::unique_ptr<AI_CLASS>(new AI_CLASS);
    do_init_aiapp(aiapp.get(), cfg_filename, license_filename);
    return aiapp;
  }


  /// Get component_name from ai-app configuration
  static std::string component_name(const std::string& aiapp_cfg);

  /// Create an aiapp for the specified component name
  static std::unique_ptr<ai_app::Aiapp> create(const std::string&
                                               component_name);

  /// Register an aiapp implementation
  static bool register_implementation(const std::string& component_name,
                                      Create_function create);

  /// Register an aiapp implementation
  template <typename T>
  static bool register_implementation() {
    return register_implementation(T::ai_component_name, []() {
      return std::unique_ptr<ai_app::Aiapp>(new T());
    });
  }

 private:
  /// Must be implemented in the application to register all available aiapps
  static void register_aiapps();

  /// Create and initialize aiapp
  static std::unique_ptr<ai_app::Aiapp> do_make_aiapp(
      const std::string& cfg_filename,
      const std::string& license_filename);

  /// Initialize aiapp
  static bool do_init_aiapp(ai_app::Aiapp* aiapp,
      const std::string& cfg_filename,
      const std::string& license_filename);

  static bool s_initialized;
  static std::map<std::string, Create_function> s_map;
};

// Define method inline to avoid introducing circular dependency in the lib
// as register_aiapps() is normally defined in the application itself
inline void Aiapp_factory::init() {
  if (!s_initialized) {
    register_aiapps();
    s_initialized = true;
  }
}
}
}
