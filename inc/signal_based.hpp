///
/// Ai-app interface and types for signal-based ai-apps
///
/// \copyright 2018 NVISO SA. All rights reserved.
/// \license This project is released under the XXXXXX License.
///

#pragma once

#include "aiapp.hpp"
#include <iostream>
#include <fstream>
#include <iterator>

namespace lpdnn {
namespace ai_app {

/// Signal representation.
/// An signal can be constructed from a std::vector<uint8_t>, or a std::string
/// or raw data pointer and size. When passing rvalues vector or strings, the
/// signal will take ownership of the data, otherwise will just keep reference.
class Signal {
 protected:
  /// Contains signal data if we have ownership of it
  std::vector<uint8_t> _signal_content;

 public:
  /// Signal format
  enum class Format {
    invalid = 0,        /// invalid signal

    encoded = 256,  

    tile = 512  ///  Preprocessed tile format. Use attributes field for more details.
  };

  /// Don't take data ownership.
  /// sgnl_dim parameter can be omitted in case of encoded signals since
  /// this information will be extracted from the signal content itself.
  Signal(Format sgnl_format, const std::vector<uint8_t>& data, Dim2d sgnl_dim)
      : Signal(sgnl_format, data.data(), data.size(), sgnl_dim) {}

  /// Take data ownership
  Signal(Format sgnl_format, std::vector<uint8_t>&& data, Dim2d sgnl_dim = {})
      : _signal_content(std::move(data)),
        format{sgnl_format},
        dim(sgnl_dim),
        data{_signal_content.data()},
        data_size{_signal_content.size()} {}

  /// Don't take data ownership.
  Signal(Format sgnl_format, const std::string& data, Dim2d sgnl_dim)
      : Signal(sgnl_format, (uint8_t*)data.c_str(), data.size(), sgnl_dim) {}

  /// Take data ownership
  Signal(Format sgnl_format, std::string&& data, Dim2d sgnl_dim)
      : Signal(sgnl_format,
              std::vector<uint8_t>((uint8_t*)data.c_str(),
                                   (uint8_t*)data.c_str() + data.size()),
              sgnl_dim) {
    data.clear();
  }

  /// Don't take data ownership
  /// sgnl_data_size is mandatory in case of encoded signals.
  Signal(Format sgnl_format, const uint8_t* sgnl_data, size_t sgnl_data_size,
        Dim2d sgnl_dim)
      : format{sgnl_format},
        dim(sgnl_dim),
        data{sgnl_data},
        data_size{sgnl_data_size} {}

  /// Utility factory methods
  static Signal encoded(const std::vector<uint8_t>& data) {
    return Signal(Format::encoded, data, {});
  }
  static Signal encoded(std::vector<uint8_t>&& data) {
    return Signal(Format::encoded, std::move(data), {});
  }

  /// Signal format
  Format format;

  /// Signal dimensions (for raw signals)
  Dim2d dim;


  /// Tile attributes.
  /// This is ai-app specific and allows to specify tile data formats.
  std::string attributes;

  /// Pointer to signal data (no ownership of the data).
  const uint8_t* data;

  /// Size of signal data. Mandatory for encoded signals.
  size_t data_size;

};

/// Abstract signal-based AiApp
class Signal_based : virtual public Aiapp {
 public:
  /// @return supported signal formats (ordered by preference)
  virtual std::vector<Signal::Format> signal_formats() const = 0;

  /// Load signal from file
  /// @return loaded signal
  virtual ai_app::Signal load_signal(const std::string& file_name) const = 0;
};


/// Default implementation for load_signal method.
/// Can be called by derived classes if desired
inline ai_app::Signal Signal_based::load_signal(const std::string& file_name) const {
  std::ifstream f(file_name, std::ios::binary);
  if (!f.good()) {
    std::cout << "Error: can't read input file: " << file_name << std::endl;
    return Signal(ai_app::Signal::Format::encoded, {});
  }
  std::vector<uint8_t> file_content(std::istreambuf_iterator<char>{f}, {});
  return Signal(ai_app::Signal::Format::encoded, std::move(file_content));
};

}  // namespace ai_app
}  // namespace lpdnn
