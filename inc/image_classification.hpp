///
/// Ai-app interface for image classification
///
/// \copyright 2018 NVISO SA. All rights reserved.
/// \license This project is released under the XXXXXX License.
///

#pragma once

#include "image_based.hpp"

namespace lpdnn {
namespace ai_app {

/// Image classification AiApp
class Image_classification : virtual public Image_based {
 public:
  struct Result {
    bool success{};
    std::vector<float> confidence;
  };

  /// Perform inference.
  virtual Result execute(const Image& input, const std::vector<lpdnn::ai_app::Blob>& additional_inputs) = 0;
  virtual Result execute(const Image& input) = 0;

  /// @return Names of classes
  virtual std::vector<std::string> classes() = 0;

  /// @return our aiapp class id
  const char* interface_name() const override { return ai_interface_name; }
  static constexpr char const* ai_interface_name = "com_bonseyes/interfaces#image_classification";
  using Ai_interface_class = Image_classification;
};

}  // namespace ai_app
}  // namespace lpdnn
