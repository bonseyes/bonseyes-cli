///
/// Ai-app base interface and types
///
/// \copyright 2018 NVISO SA. All rights reserved.
/// \license This project is released under the XXXXXX License.
///

#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <vector>
#include "lpdnn/json.hpp"

namespace lpdnn {
namespace ai_app {

/// 2-dimensional size
struct Dim2d {
  int x;
  int y;

  Dim2d operator+(const Dim2d& rhs) {
    Dim2d result;
    result.x = this->x + rhs.x;
    result.y = this->y + rhs.y;

    return result;
  }
};

/// Aiapp Blob
/// This could be improved to allow referring to existing data
/// thus avoding unneeded data-copy, for example by using shared_ptr.
struct Blob {
  /// Data dimensions. Mandatory if the blob represents a tensor.
  std::vector<int> dim;

  /// Data. Mandatory if the blob represents a tensor.
  std::vector<float> data;

  /// Optional raw representation.
  std::vector<uint8_t> raw;

  /// Optional CBOR representation when data is structured.
  std::vector<uint8_t> cbor;

  /// Optional additional information
  /// (eg, description of internal representation: "NCHW,8bits,dp3").
  std::string info;
};

/// Aiapp error information
struct Error {
  /// Aiapp error codes
  enum class Code {
    success = 0,
    generic = 1,
    unimplemented = 2,
    uninitialized = 3,
    invalid_argument = 4,
    invalid_data = 5,
    io_error = 6,
    internal_error = 7
  };

  Code code{};  // Error code
  std::string msg;  // Error description
};


/// AI-App interface
class Aiapp {
 public:
  /// Aiapp version information
  struct Version {
    Version(){}
    Version(int maj, int min, int maint, int b) :
      major_number{maj},
      minor_number(min),
      maintenance_number(maint),
      build_number(b) {}
    int major_number{};
    int minor_number{};
    int maintenance_number{};
    int build_number{};
  };

  /// @return the aiapp name
  virtual std::string aiapp_name() const = 0;

  /// \return config that was specified to create the component
  virtual json get_config(const std::string& name = "") const = 0;

  /// @return the aiapp version
  virtual Version version() const = 0;

  /// Initialization options
  /// \param cfg: configuration string, typically in JSON format.
  /// \return: true if success
  virtual bool init(const std::string& cfg) = 0;

  /// Set runtime options for the specified component
  /// \param opt: runtime options, typically in JSON format.
  /// \param name: subcomponent name
  /// \return: true if success
  virtual bool set_options(const std::string& opt,
                           const std::string& name = "") = 0;

  /// \return: information on last error occoured
  virtual Error error() const = 0;

  /// \return: clear information on last error occoured
  virtual void clear_error() = 0;
  
  /// Introspection methods
  /// \{

  /// \return: names of all direct subcomponents of the specified component
  virtual std::vector<std::string> components(
      const std::string& name = "") const = 0;

  /// \return output(s) of the specified component
  virtual std::vector<Blob> output(const std::string& name = "") const = 0;

  /// \return metrics of the specified component and all its subcomponents
  virtual std::string metrics(const std::string& name = "") const = 0;

  /// set end-of-execution at the end of the specified component
  /// if name is empty any exit-point previously set is removed
  virtual bool set_exit_after(const std::string& name = "") = 0;

  /// \}

  /// @return the interface name for this aiapp
  virtual const char* interface_name() const = 0;

  /// @return the component name for this aiapp
  virtual const char* component_name() const = 0;

  virtual ~Aiapp() {}
};

/// AiApp standard processing components
/// Each ai-app can contain other sub-components.
/// Each subcomponent can be identified by a pathname, for example:
///   "preprocessing.normalize"
///   "inference.net1.conv23"
struct Component {
  /// Standard component names. Their use is not mandatory but
  /// allows an ai-app to be supported by existing tools.
  static constexpr char const* preprocessing = "preprocessing";
  static constexpr char const* inference = "inference";
  static constexpr char const* postprocessing = "postprocessing";

  /// Ai-app interface parameters
  static constexpr char const* interface = "interface";

  /// Name separator in a component pathname string.
  /// Component names can't contain the separator except possibly for the leafs
  static constexpr char separator = '.';

  /// Concatenate component names in a component pathname
  static std::string join(const std::string& path, const std::string& comp) {
    return path + separator + comp;
  }
};

/// AiApp Metrics
struct Metrics {
  /// Standard metrics. All timings are in microseconds.
  static constexpr char const* init_time = "init_time";
  static constexpr char const* inference_time = "inference_time";
  static constexpr char const* inference_cpu_time = "inference_cpu_time";
};

}  // namespace ai_app
}  // namespace lpdnn
