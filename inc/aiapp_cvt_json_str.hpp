///
/// Utility functions to convert ai_apps in/out types from/to json strings
///
/// \copyright 2020 NVISO SA. All rights reserved.
///

#pragma once

#include "audio_classification.hpp"
#include "face_recognition.hpp"
#include "image_classification.hpp"
#include "object_detection.hpp"
#include "signal_classification.hpp"
#include "image_segmentation.hpp"
#include <string>


namespace lpdnn {

namespace ai_app {

// Version
std::string to_json_str(const Aiapp::Version& p);
bool from_json_str(const std::string& js, Aiapp::Version& p);

// Blob
std::string to_json_str(const Blob& p);
bool from_json_str(const std::string& js, Blob& p);
bool from_json_str(const std::string& js, std::vector<Blob>& p);

// Image based ai-apps
std::string to_json_str(const Dim2d& p);
bool from_json_str(const std::string& js, Dim2d& p);

std::string to_json_str(const Dim3d& p);
bool from_json_str(const std::string& js, Dim3d& p);

std::string to_json_str(const Rect& p);
bool from_json_str(const std::string& js, Rect& p);

std::string to_json_str(const Landmark& p);
bool from_json_str(const std::string& js, Landmark& p);

std::string to_json_str(const Landmark3d& p);
bool from_json_str(const std::string& js, Landmark3d& p);

std::string to_json_str(const Landmarks& p);
bool from_json_str(const std::string& js, Landmarks& p);

std::string to_json_str(const Landmarks3d& p);
bool from_json_str(const std::string& js, Landmarks3d& p);

std::string to_json_str(const Segmentation& p);
bool from_json_str(const std::string& js, Segmentation& p);

std::string to_json_str(const Pixel& p);
bool from_json_str(const std::string& js, Pixel& p);

// ImageClassification
std::string to_json_str(const Image_classification::Result& p);
bool from_json_str(const std::string& js, Image_classification::Result& p);

// ObjectDetection
std::string to_json_str(const Object_detection::Result::Item& p);
bool from_json_str(const std::string& js, Object_detection::Result::Item& p);

std::string to_json_str(const Object_detection::Result& p);
bool from_json_str(const std::string& js, Object_detection::Result& p);

// Face_recognition
std::string to_json_str(const Face_recognition::Result& p);
bool from_json_str(const std::string& js, Face_recognition::Result& p);

// Audio_classification
std::string to_json_str(const Audio_classification::Result& p);
bool from_json_str(const std::string& js, Audio_classification::Result& p);

// SignalClassification
std::string to_json_str(const Signal_classification::Result& p);
bool from_json_str(const std::string& js, Signal_classification::Result& p);

// ImageSegmentation
std::string to_json_str(const Image_segmentation::Result& p);
bool from_json_str(const std::string& js, Image_segmentation::Result& p);

}  // namespace app
}  // namespace lpdnn
