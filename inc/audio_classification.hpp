///
/// Ai-app interface and types for audio-classification ai-app
///
/// \copyright 2019 NVISO SA. All rights reserved.
/// \license This project is released under the XXXXXX License.
///

#pragma once

#include "audio_based.hpp"

namespace lpdnn {
namespace ai_app {

/// Audio Classification AiApp
class Audio_classification : virtual public Audio_based {
 public:
  struct Result {
    bool success{};
    std::vector<float> confidence;
  };

  /// Perform inference.
  virtual Result execute(const Audio_chunk& input, const std::vector<ai_app::Blob>& additional_inputs) = 0;
  virtual Result execute(const Audio_chunk& input) = 0;

  /// @return Names of audio classes
  virtual std::vector<std::string> classes() = 0;

  /// @return our aiapp class id
  const char* interface_name() const override { return ai_interface_name; }
  static constexpr char const* ai_interface_name = "com_bonseyes/interfaces#audio_classification";
  using Ai_interface_class = Audio_classification;
};

}  // namespace ai_app
}  // namespace lpdnn
