FROM docker:18.09.1-git as builder

RUN apk add --no-cache python3 bash gcc libffi-dev openssl-dev musl-dev python3-dev

COPY tool/requirements.txt /requirements.txt

RUN pip3 install --upgrade pip && pip3 install --prefix=/install -r /requirements.txt

FROM python:3.8-slim as docbuilder

RUN apt-get update -y && apt-get install -y build-essential && \
    python3 -m pip install sphinx sphinx-argparse sphinx_rtd_theme sphinx_togglebutton yaspin pyyaml sphinx-jsonschema recommonmark

COPY inc /src/inc
COPY algorithms /src/algorithms
COPY interfaces /src/interfaces
COPY schemas /src/schemas
COPY doc /src/doc

WORKDIR /src/doc

RUN sphinx-build . _build/html -v --color

FROM docker:18.09.1-git

RUN apk add --no-cache python3 bash libffi openssl

COPY --from=builder /install /usr/
COPY --from=docbuilder /src/doc/_build/html /opt/doc

ADD tool/lib /app/tool/lib
ADD tool/bin /app/tool/bin
ADD schemas /app/schemas
ADD interfaces /app/interfaces
ADD algorithms /app/algorithms
ADD network_formats /app/network_formats

ENV PATH ${PATH}:/app/tool/bin

ENV PYTHONPATH /app/tool/lib

